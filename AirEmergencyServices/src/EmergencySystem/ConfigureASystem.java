/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmergencySystem;

import Employee.EmergencySystemAdmin;
import Employee.EnterpriseAdmin;
import Employee.HospitalEnterpriseAdmin;
import Employee.Drone;
import Drone.DroneStation;
import EmergencySystem.EmergencyAddressLocation.EmergencyAddressLocation;
import EmergencySystem.Enterprise.Emergency911Enterprise;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Enterprise.Enterprise.EnterpriseType;
import EmergencySystem.Network.Network;
import Employee.Ambulance;
import Employee.Doctor;
import Hospital.Hospital;
import Hospital.Organisation.AmbulanceOrganisation;
import Hospital.Organisation.DoctorOrganization;
import Hospital.Organisation.DroneOrganisation;
import Hospital.Organisation.Organisation;
import Hospital.Organisation.Organisation.Type;
import Hospital.Role.AmbulanceRole;
import Hospital.Role.DoctorRole;
import Hospital.Role.DroneRole;
import Hospital.Role.EmergencySystemAdminRole;
import Hospital.Role.Emergency911EnterpriseAdminRole;
import Hospital.Role.DroneHeadquarterUser;
import Hospital.Role.HospitalEnterpriseAdminRole;
import Hospital.Role.PoliceAdminRole;
import Hospital.Role.SWATAdminRole;
import Hospital.Role.EcomAdminRole;
import Hospital.UserAccount.UserAccount;
import LicensePlate.LicensePlate;
import Person.Person;

/**
 *
 * @author abhisheksatbhai
 */
public class ConfigureASystem {
    public static EmergencySystem configure(){
        
        EmergencySystem system = EmergencySystem.getInstance();
        
        EmergencySystemAdmin emerAdmin=new EmergencySystemAdmin();
      
        system.getEmployeeDirectory().createEmployee(emerAdmin);
        UserAccount esadmin_ua = system.getUserAccountDirectory().createUserAccount("admin", "admin@123", emerAdmin, new EmergencySystemAdminRole());
        UserAccount esuser_ua = system.getUserAccountDirectory().createUserAccount("user", "user@123", emerAdmin, new DroneHeadquarterUser());
         //end of emergency system admin creation
        
        
        //Creating boston network
        Network n1=system.addNetwork();
        n1.setNetworkName("Boston");
        
        //Creating person directory
        Person p1=system.getPersonDirectory().addPerson();
        p1.setName("Abhishek Satbhai");
        p1.setAge(23);
        p1.setAddress("75 saint, Alphonsus Street");
        p1.setCarOwned("Volkswagan GT");
        p1.setDriversLicenseNumber(901106335);
        p1.setLicensePlateNumber("FP1744");
        p1.setPhoneNumber("6179925210");
        p1.setEmergencyContactNumber("6179925210");
        p1.setPhoto("Images/person1.jpg");
        
        
        Person p2=system.getPersonDirectory().addPerson();
        p2.setName("Chinmayi Shaligram");
        p2.setAge(23);
        p2.setAddress("75 st alphonsous street");
        p2.setCarOwned("Mercedes Benz");
        p2.setDriversLicenseNumber(838009103);
        p2.setLicensePlateNumber("CI0000");
        p2.setPhoneNumber("6179925064");
        p2.setEmergencyContactNumber("6179925064");
        p2.setPhoto("Images/person2.jpg");
        
        Person p3=system.getPersonDirectory().addPerson();
        p3.setName("Aditya Kulkarni");
        p3.setAge(24);
        p3.setAddress("94 saint german street");
        p3.setCarOwned("Audi");
        p3.setDriversLicenseNumber(354876495);
        p3.setLicensePlateNumber("RT72LY");
        p3.setPhoneNumber("6179925110");
        p3.setEmergencyContactNumber("6179925110");
        p3.setPhoto("Images/person3.jpg");
        
        Person p4=system.getPersonDirectory().addPerson();
        p4.setName("Harshal Sathai");
        p4.setAge(18);
        p4.setAddress("25 Bolyston Street");
        p4.setCarOwned("Jaguar");
        p4.setDriversLicenseNumber(992211135);
        p4.setLicensePlateNumber("40559F");
        p4.setPhoneNumber("6179925111");
        p4.setEmergencyContactNumber("6179925211");
        p4.setPhoto("Images/person4.jpg");
        
        Person p5=system.getPersonDirectory().addPerson();
        p5.setName("Rutuja Joshi");
        p5.setAge(27);
        p5.setAddress("1045 Tremont Street");
        p5.setCarOwned("BMW");
        p5.setDriversLicenseNumber(901105975);
        p5.setLicensePlateNumber("BR2515");
        p5.setPhoneNumber("6179925212");
        p5.setEmergencyContactNumber("6179925212");
        p5.setPhoto("Images/person5.jpg");
        
        Person p6=system.getPersonDirectory().addPerson();
        p6.setName("Pankaj Bankar");
        p6.setAge(25);
        p6.setAddress("102 Newbury Street");
        p6.setCarOwned("Toyota");
        p6.setDriversLicenseNumber(901105976);
        p6.setLicensePlateNumber("PL2020");
        p6.setPhoneNumber("6179925213");
        p6.setEmergencyContactNumber("6179925213");
        p6.setPhoto("Images/person6.png");
        
        
        //Creating licenseplate directory
        
        LicensePlate l1=system.getLicensePlateDir().addLicensePlate();
        l1.setLicensePlateNumber("Images//license_plate1.jpg");
        
         LicensePlate l2=system.getLicensePlateDir().addLicensePlate();
        l2.setLicensePlateNumber("Images//license_plate2.jpg");
        
         LicensePlate l3=system.getLicensePlateDir().addLicensePlate();
        l3.setLicensePlateNumber("Images//license_plate3.jpg");
        
         LicensePlate l4=system.getLicensePlateDir().addLicensePlate();
        l4.setLicensePlateNumber("Images//license_plate4.jpg");
        
         LicensePlate l5=system.getLicensePlateDir().addLicensePlate();
        l5.setLicensePlateNumber("Images//license_plate5.jpg");
        
         LicensePlate l6=system.getLicensePlateDir().addLicensePlate();
        l6.setLicensePlateNumber("Images//license_plate6.jpg");
        
        //Creation of enterprises in boston

        Enterprise.EnterpriseType type1_n2 = EnterpriseType.EMEREGENCY911ENTERPRISE;
        Enterprise enterprisen2_2 = n1.getEntDirObj().createAndAddEnterprise("BostonDC",type1_n2,"360 Huntington Ave, Boston, MA 02120" );
        
        Enterprise.EnterpriseType type2_n2 = EnterpriseType.POLICEENTERPRISE;
        Enterprise enterprisen2_3 = n1.getEntDirObj().createAndAddEnterprise("BostonPolice",type2_n2,"" );
        
        Enterprise.EnterpriseType type8_n2 = EnterpriseType.PRIVATEENTERPRISE;
        Enterprise enterprisen6 = n1.getEntDirObj().createAndAddEnterprise("BostonSWAT",type8_n2,"" );
        
        Enterprise.EnterpriseType type9_n2 = EnterpriseType.ECOMENTERPRISE;
        Enterprise enterprisen7 = n1.getEntDirObj().createAndAddEnterprise("BostonEcom",type9_n2,"" );
        
       //Creation of boston enterprise admin
        EnterpriseAdmin bostonpsapAdmin=new EnterpriseAdmin();
        enterprisen2_2.getEmployeeDirectory().createEmployee(bostonpsapAdmin);
        enterprisen2_2.getUserAccountDirectory().createUserAccount("bostondc", "bostondc", bostonpsapAdmin, new Emergency911EnterpriseAdminRole());
        
        
        EnterpriseAdmin bostonPoliceAdmin=new EnterpriseAdmin();
        enterprisen2_3.getEmployeeDirectory().createEmployee(bostonPoliceAdmin);
        enterprisen2_3.getUserAccountDirectory().createUserAccount("bostonpolice", "bostonpolice", bostonPoliceAdmin, new PoliceAdminRole());
        bostonPoliceAdmin.setAvailable(true);
        
        EnterpriseAdmin bostonSWATAdmin=new EnterpriseAdmin();
        enterprisen6.getEmployeeDirectory().createEmployee(bostonSWATAdmin);
        enterprisen6.getUserAccountDirectory().createUserAccount("bostonswat", "bostonswat", bostonSWATAdmin, new SWATAdminRole());
        bostonSWATAdmin.setAvailable(true);
        
        EnterpriseAdmin bostonECOMAdmin=new EnterpriseAdmin();
        enterprisen7.getEmployeeDirectory().createEmployee(bostonECOMAdmin);
        enterprisen7.getUserAccountDirectory().createUserAccount("bostonecom", "bostonecom", bostonECOMAdmin, new EcomAdminRole());
        bostonECOMAdmin.setAvailable(true);
        
        
        //creating hospitals in the network
        

        Hospital h1_2=n1.addHospital("Wokhardt Hospital");
        h1_2.setHospitalName("Wokhardt Hospital");
        h1_2.setHospitalLocation("75 Francis St, Boston, MA 02115");
        h1_2.setSpeciality("Orthopedic");
        h1_2.setNumberOfBeds(50);
        h1_2.setNumberOfEmptyBeds(5);
        
        
        
        Hospital h1_3=n1.addHospital("Massachusetts General Hospital");
        h1_3.setHospitalName("Massachusetts General Hospital");
        h1_3.setHospitalLocation("275 Cambridge Street, Boston, MA 02114");
        h1_3.setSpeciality("Neurology");
        h1_3.setNumberOfBeds(50);
        h1_3.setNumberOfEmptyBeds(0);
        
        Hospital h1_4=n1.addHospital("Shriners Hospitals for Children");
        h1_4.setHospitalName("Shriners Hospitals for Children");
        h1_4.setHospitalLocation("51 Blossom St, Boston, MA 02115");
        h1_4.setSpeciality("Cardiothoracic");
        h1_4.setNumberOfBeds(50);
        h1_4.setNumberOfEmptyBeds(5);
        
        Hospital h1_5=n1.addHospital("Carney Hospital");
        h1_5.setHospitalName("Carney Hospital");
        h1_5.setHospitalLocation("2100 Dorchester Ave, Boston, MA 02124");
        h1_5.setSpeciality("Allergist");
        h1_5.setNumberOfBeds(50);
        h1_5.setNumberOfEmptyBeds(10);
        
         //creating hospital enterprise admin
         
        HospitalEnterpriseAdmin h1n1=new HospitalEnterpriseAdmin();
        h1n1.setName("Wokhardt Admin");
        h1_2.getEmployeeDirectory().createEmployee(h1n1);
        h1_2.getUserAccountDirectory().createUserAccount("wokhardtadmin", "wokhardtadmin", h1n1, new HospitalEnterpriseAdminRole());

        HospitalEnterpriseAdmin h2n1=new HospitalEnterpriseAdmin();
        h2n1.setName("Massachusetts Admin");
        h1_3.getEmployeeDirectory().createEmployee(h2n1);
        h1_3.getUserAccountDirectory().createUserAccount("massadmin", "massadmin", h2n1, new HospitalEnterpriseAdminRole());
         
        HospitalEnterpriseAdmin h3n1=new HospitalEnterpriseAdmin();
        h3n1.setName("Shriners Admin");
      
        h1_4.getEmployeeDirectory().createEmployee(h3n1);
        h1_4.getUserAccountDirectory().createUserAccount("shrinersadmin", "shrinersadmin", h3n1, new HospitalEnterpriseAdminRole());
         
        HospitalEnterpriseAdmin h4n1=new HospitalEnterpriseAdmin();
        h4n1.setName("Carney Admin");
        h1_5.getEmployeeDirectory().createEmployee(h4n1);
        h1_5.getUserAccountDirectory().createUserAccount("carneyadmin", "carneyadmin", h4n1, new HospitalEnterpriseAdminRole());
         
        
        //creating doctor employees and ambulance employees
        
        h1_2.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h1_2.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        
        Doctor d1_n1=new Doctor();
        d1_n1.setName("Doctor Sane");
        d1_n1.setDoctorsAvailablityStatus(true);
        d1_n1.setDoctorsSpeciality("Neurologist");
        
        Doctor d2_n1=new Doctor();
        d2_n1.setName("Doctor Pingle");
        d2_n1.setDoctorsAvailablityStatus(true);
        d2_n1.setDoctorsSpeciality("Orthopedic");
        
        
        
       for(Organisation org: h1_2.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d1_n1);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d2_n1);
               
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drsane", "drsane", d1_n1, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drpingle", "drpingle", d2_n1, new DoctorRole());
               
              
           }
       }
        h1_3.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h1_3.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        
       Doctor d3_n1=new Doctor();
        d3_n1.setName("Doctor Shruti");
        d3_n1.setDoctorsAvailablityStatus(true);
        d3_n1.setDoctorsSpeciality("Dermeatologist");
        
        Doctor d4_n1=new Doctor();
        d4_n1.setName("Doctor Shreya");
        d4_n1.setDoctorsAvailablityStatus(true);
        d4_n1.setDoctorsSpeciality("Cardiologist");
        
         for(Organisation org: h1_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d3_n1);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d4_n1);
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drshruti", "drshruti", d3_n1, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drshreya", "drshreya", d4_n1, new DoctorRole());
           
           }
       }
        
        h1_4.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h1_4.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        
        Doctor d5_n1=new Doctor();
        d5_n1.setName("Doctor Shivam");
        d5_n1.setDoctorsAvailablityStatus(true);
        d5_n1.setDoctorsSpeciality("Cardiologist");
        
        Doctor d6_n1=new Doctor();
        d6_n1.setName("Doctor Tanvi");
        d6_n1.setDoctorsAvailablityStatus(true);
        d6_n1.setDoctorsSpeciality("Asthama attack");
        
         for(Organisation org: h1_4.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d5_n1);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d6_n1);
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drshivam", "drshivam", d5_n1, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drtanvi", "drtanvi", d6_n1, new DoctorRole());
               
        
           }
           
       }
         
         
        h1_5.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h1_5.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
         
        Doctor d7_n1=new Doctor();
        d7_n1.setName("Doctor Vishal");
        d7_n1.setDoctorsAvailablityStatus(true);
        d7_n1.setDoctorsSpeciality("Child Specialist");
        
        Doctor d8_n1=new Doctor();
        d8_n1.setName("Doctor Anuradha");
        d8_n1.setDoctorsAvailablityStatus(true);
        d8_n1.setDoctorsSpeciality("Cardiologist");
        
         for(Organisation org: h1_5.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
                
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d7_n1);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d8_n1);
               
                
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drvishal", "drvishal", d7_n1, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("dranuradha", "dranuradha", d8_n1, new DoctorRole());

           }
       }
       
        Ambulance a1_n1=new Ambulance();
        a1_n1.setName("WAmb1");
        a1_n1.setAvailability("Available");
        
        Ambulance a2_n1=new Ambulance();
        a2_n1.setName("WAmb2");
        a2_n1.setAvailability("Available");
        
        Ambulance a9_n1=new Ambulance();
        a9_n1.setName("WAmb3");
        a9_n1.setAvailability("Available");
        
        Ambulance a10_n1=new Ambulance();
        a10_n1.setName("WAmb4");
        a10_n1.setAvailability("Available");
        
        
    
       for(Organisation org: h1_2.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a1_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a2_n1);
               
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a9_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a10_n1);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("wamb1", "wamb1", a1_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("wamb2", "wamb2", a2_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("wamb3", "wamb3", a9_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("wamb4", "wamb4", a10_n1, new AmbulanceRole());
               

           }
       }
        Ambulance a3_n1=new Ambulance();
        a3_n1.setName("MAmb1");
        a3_n1.setAvailability("Available");
        
        
        Ambulance a4_n1=new Ambulance();
        a4_n1.setName("MAmb2");
        a4_n1.setAvailability("Available");
        
        Ambulance a11_n1=new Ambulance();
        a11_n1.setName("MAmb3");
        a11_n1.setAvailability("Available");
        
        
        Ambulance a12_n1=new Ambulance();
        a12_n1.setName("MAmb4");
        a12_n1.setAvailability("Available");
        
        for(Organisation org: h1_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a3_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a4_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a11_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a12_n1);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb1", "mamb1", a3_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb2", "mamb2", a4_n1, new AmbulanceRole());
                ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb3", "mamb3", a11_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb4", "mamb4", a12_n1, new AmbulanceRole());
               
           }
           
       }
        
        Ambulance a5_n1=new Ambulance();
        a5_n1.setName("SAmb1");
        a5_n1.setAvailability("Available");
        
        
        Ambulance a6_n1=new Ambulance();
        a6_n1.setName("SAmb2");
        a6_n1.setAvailability("Available");
        
         for(Organisation org: h1_4.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a5_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a6_n1);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("samb1", "samb1", a5_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("samb2", "samb2", a6_n1, new AmbulanceRole());
           }
           
       }
         
            
        Ambulance a7_n1=new Ambulance();
        a7_n1.setName("CAmb1");
        a7_n1.setAvailability("Available");
        
        
        Ambulance a8_n1=new Ambulance();
        a8_n1.setName("CAmb2");
        a8_n1.setAvailability("Available");
        
        
          for(Organisation org: h1_5.getOrganizationDirectory().getOrganisationList())
         {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a7_n1);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a8_n1);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("camb1", "camb1", a7_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("camb2", "camb2", a8_n1, new AmbulanceRole());

           }
           
        }
  
        //creating drones in the network
        Organisation droneOrg_n1=null;
         for (Type type : Organisation.Type.values())
         {
            if (type.getValue().equals(Type.DRONE.getValue()))
            {
          
                droneOrg_n1=((Emergency911Enterprise)(enterprisen2_2)).getOrganizationDirectory().createOrganisation(type);
            
            }
         }
         
         //creating drone stations in boston dispatch center enterprise
        DroneStation bostonDroneStation1=null;
        DroneStation bostonDroneStation2=null;
        DroneStation bostonDroneStation3=null;
        for(Organisation org:((Emergency911Enterprise)enterprisen2_2).getOrganizationDirectory().getOrganisationList())
        {
            if(org instanceof DroneOrganisation)
            {
                bostonDroneStation1=((DroneOrganisation)org).getDroneDirectoryObject().addDroneStation();
                bostonDroneStation1.setDroneStationName("Boston Drone Station 1");
                bostonDroneStation1.setDroneStationAddress("68 William Cardinal O'Connell Way, Boston, MA 02114");
       
                 bostonDroneStation2=((DroneOrganisation)org).getDroneDirectoryObject().addDroneStation();
                 bostonDroneStation2.setDroneStationName("Boston Drone Station 2");
                 bostonDroneStation2.setDroneStationAddress("4 Yawkey Way, Boston, MA 02215");
            
                 bostonDroneStation3=((DroneOrganisation)org).getDroneDirectoryObject().addDroneStation();
                 bostonDroneStation3.setDroneStationName("Boston Drone Station 3");
                 bostonDroneStation3.setDroneStationAddress("300 Fenway, Boston, MA 02115");
            }
        }
        
        //adding drones to drone station
        Drone d1n1=new Drone();
        d1n1.setDroneId("Boston_Drone_1");
        d1n1.setStatus("Active");
        bostonDroneStation1.addDrone(d1n1);
       
        droneOrg_n1.getEmployeeDirectory().createEmployee(d1n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone1", "bostondrone1", d1n1, new DroneRole() );
      

        Drone d2n1= new Drone();
        d2n1.setDroneId("Boston_Drone_2");
        d2n1.setStatus("Assigned");
        bostonDroneStation1.addDrone(d2n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d2n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone2", "bostondrone2", d2n1, new DroneRole() );
      
        
        Drone d3n1= new Drone();
        d3n1.setDroneId("Boston_Drone_3");
        d3n1.setStatus("Active");
        bostonDroneStation1.addDrone(d3n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d3n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone3", "bostondrone3", d3n1, new DroneRole() );
      
        
        Drone d4n1= new Drone();
        d4n1.setDroneId("Boston_Drone_4");
        d4n1.setStatus("Assigned");
        bostonDroneStation1.addDrone(d4n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d4n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone4", "bostondrone4", d4n1, new DroneRole() );
      
        
        Drone d5n1=new Drone();
        d5n1.setDroneId("Boston_Drone_5");
        d5n1.setStatus("Active");
        bostonDroneStation1.addDrone(d5n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d5n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone5", "bostondrone5", d5n1, new DroneRole() );

       
        Drone d6n1=new Drone();
              
        d6n1.setDroneId("Boston_Drone_6");
        d6n1.setStatus("Active");
        bostonDroneStation2.addDrone(d6n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d6n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone6", "bostondrone6", d6n1, new DroneRole() );
      
        Drone d7n1=new Drone();
        d7n1.setDroneId("Boston_Drone_7");
        d7n1.setStatus("Active");
        bostonDroneStation2.addDrone(d7n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d7n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone7", "bostondrone7", d7n1, new DroneRole() );
      
        
        
        Drone d8n1= new Drone();
        d8n1.setDroneId("Boston_Drone_8");
        d8n1.setStatus("Active");
        bostonDroneStation2.addDrone(d8n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d8n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone8", "bostondrone8", d8n1, new DroneRole() );
      
        
        
        Drone d9n1=new Drone();
        d9n1.setDroneId("Boston_Drone_9");
        d9n1.setStatus("Active");
        bostonDroneStation3.addDrone(d9n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d9n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone9", "bostondrone9", d9n1, new DroneRole() );
      
        
        Drone d10n1= new Drone();
        d10n1.setDroneId("Boston_Drone_10");
        d10n1.setStatus("Active");
        bostonDroneStation3.addDrone(d10n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d10n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone10", "bostondrone10", d10n1, new DroneRole() );
      
        
        Drone d11n1= new Drone();
        d11n1.setDroneId("Boston_Drone_11");
        d11n1.setStatus("Active");
        bostonDroneStation3.addDrone(d11n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d11n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone11", "bostondrone11", d11n1, new DroneRole() );
      
      
        Drone d12n1= new Drone();
        d12n1.setDroneId("Boston_Drone_12");
        d12n1.setStatus("Active");
        bostonDroneStation3.addDrone(d12n1);
        droneOrg_n1.getEmployeeDirectory().createEmployee(d12n1);
        droneOrg_n1.getUserAccountDirectory().createUserAccount("bostondrone12", "bostondrone12", d12n1, new DroneRole() );
     
        //End of boston network creation
        
        
        //Creating cambridge network
        Network n2=system.addNetwork();
        n2.setNetworkName("Cambridge");
        
        //Creating enterprises in cambridge network

        Enterprise.EnterpriseType type3_n2 = EnterpriseType.EMEREGENCY911ENTERPRISE;
        Enterprise enterprise_n2_2 = n2.getEntDirObj().createAndAddEnterprise("CambridgeDC",type3_n2,"1350 Massachusetts Avenue. Cambridge, MA 02138" );
      
        Enterprise.EnterpriseType type4_n2 = EnterpriseType.POLICEENTERPRISE;
        Enterprise enterprisen2_4 = n2.getEntDirObj().createAndAddEnterprise("CambridgePolice",type4_n2,"" );
        
        //creating cambridge enterprise admin 
        
        EnterpriseAdmin cambridgepsapAdmin=new EnterpriseAdmin();
        enterprise_n2_2.getEmployeeDirectory().createEmployee(cambridgepsapAdmin);
        enterprise_n2_2.getUserAccountDirectory().createUserAccount("cambridgedc", "cambridgedc", cambridgepsapAdmin, new Emergency911EnterpriseAdminRole());
        
        EnterpriseAdmin cambridgePoliceAdmin=new EnterpriseAdmin();
        enterprisen2_4.getEmployeeDirectory().createEmployee(cambridgePoliceAdmin);
        enterprisen2_4.getUserAccountDirectory().createUserAccount("cambridgepolice", "cambridgepolice", cambridgePoliceAdmin, new PoliceAdminRole());
        cambridgePoliceAdmin.setAvailable(true);

      //hospital creation  
      
        Hospital h2_1=n2.addHospital("CHA Cambridge Hospital campus");
        h2_1.setHospitalName("CHA Cambridge Hospital campus");
        h2_1.setHospitalLocation("1493 Cambridge St, Cambridge, MA 02139");
        h2_1.setSpeciality("Neurology");
        h2_1.setNumberOfBeds(50);
        h2_1.setNumberOfEmptyBeds(10);
        
        Hospital h2_2=n2.addHospital("Spaulding Hospital Cambridge");
        h2_2.setHospitalName("Spaulding Hospital Cambridge");
        h2_2.setHospitalLocation("1575 Cambridge St, Cambridge, MA 02138");
        h2_2.setSpeciality("Cardiothoracic");
        h2_2.setNumberOfBeds(50);
        h2_2.setNumberOfEmptyBeds(5);
        
        
        Hospital h2_3=n2.addHospital("Mount Auburn Hospital: Walk-In Center");
        h2_3.setHospitalName("Mount Auburn Hospital: Walk-In Center");
        h2_3.setHospitalLocation("330 Mt Auburn St, Cambridge, MA 02138");
        h2_3.setSpeciality("Plastics");
        h2_3.setNumberOfBeds(50);
        h2_3.setNumberOfEmptyBeds(10);
        
       //creating hospital admins
        
        HospitalEnterpriseAdmin h1n2=new HospitalEnterpriseAdmin();
        h1n2.setName("CHA Cambridge Admin");
        h2_1.getEmployeeDirectory().createEmployee(h1n2);
        h2_1.getUserAccountDirectory().createUserAccount("chacamadmin", "chacamadmin", h1n2, new HospitalEnterpriseAdminRole());
         
        HospitalEnterpriseAdmin h2n2=new HospitalEnterpriseAdmin();
        h2n2.setName("Spaulding Admin");
        h2_2.getEmployeeDirectory().createEmployee(h2n2);
        h2_2.getUserAccountDirectory().createUserAccount("spauldingadmin", "spauldingadmin", h2n2, new HospitalEnterpriseAdminRole());
         
        HospitalEnterpriseAdmin h3n2=new HospitalEnterpriseAdmin();
        h3n2.setName("Mount Admin");
        h2_3.getEmployeeDirectory().createEmployee(h3n2);
        h2_3.getUserAccountDirectory().createUserAccount("mountadmin", "mountadmin", h3n2, new HospitalEnterpriseAdminRole());
         
         
        //creating doctor employees and ambulance employees
       
        h2_1.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h2_1.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        Doctor d1_n2=new Doctor();
        d1_n2.setName("Doctor Amanda");
        d1_n2.setDoctorsAvailablityStatus(true);
        d1_n2.setDoctorsSpeciality("Car accident-Head Injury");
        
        Doctor d2_n2=new Doctor();
        d2_n2.setName("Doctor Maura");
        d2_n2.setDoctorsAvailablityStatus(true);
        d2_n2.setDoctorsSpeciality("Heart attack");
       
        
       for(Organisation org: h2_1.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d1_n2);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d2_n2);
              
              
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("dramanda", "dramanda", d1_n2, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drmaura", "drmaura", d2_n2, new DoctorRole());
               
              
               
           }
       }
       
       
        h2_2.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h2_2.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        Doctor d3_n2=new Doctor();
        d3_n2.setName("Doctor David");
        d3_n2.setDoctorsAvailablityStatus(true);
        d3_n2.setDoctorsSpeciality("Heart attack");
        
        Doctor d4_n2=new Doctor();
        d4_n2.setName("Doctor Kunal");
        d4_n2.setDoctorsAvailablityStatus(true);
        d4_n2.setDoctorsSpeciality("Fire");
       
       for(Organisation org: h2_2.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d3_n2);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d4_n2);
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drdavid", "drdavid", d3_n2, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drkunal", "drkunal", d4_n2, new DoctorRole());
           }
       }
           
        h2_3.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h2_3.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        
        Doctor d5_n2=new Doctor();
        d5_n2.setName("Doctor Stephen");
        d5_n2.setDoctorsAvailablityStatus(true);
        d5_n2.setDoctorsSpeciality("Fire");
      
        
        Doctor d6_n2=new Doctor();
        d6_n2.setName("Doctor Jeneifer");
        d6_n2.setDoctorsAvailablityStatus(true);
        d6_n2.setDoctorsSpeciality("Car accident-Head Injury");
        
        for(Organisation org: h2_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d5_n2);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d6_n2);
               
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drstepen", "drstepen", d5_n2, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drjeneifer", "drjeneifer", d6_n2, new DoctorRole());

           }
       }
      
        Ambulance a1_n2=new Ambulance();
        a1_n2.setName("CAmb1");
        a1_n2.setAvailability("Available");
        
        
        Ambulance a2_n2=new Ambulance();
        a2_n2.setName("CAmb2");
        a2_n2.setAvailability("Available");
        
 
       for(Organisation org: h2_1.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a1_n2);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a2_n2);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("chacamb1", "chacamb1", a1_n2, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("chacamb2", "chacamb2", a2_n2, new AmbulanceRole());

           }
       }
       
        Ambulance a3_n2=new Ambulance();
        a3_n2.setName("SAmb1");
        a3_n2.setAvailability("Available");
        
        
        Ambulance a4_n2=new Ambulance();
        a4_n2.setName("SAmb2");
        a4_n2.setAvailability("Available");
        
        
       for(Organisation org: h2_2.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a3_n2);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a4_n2);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("samb1", "samb1", a1_n1, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("samb2", "samb2", a2_n1, new AmbulanceRole());

           }
       }
        
         Ambulance a5_n2=new Ambulance();
        a5_n2.setName("MAmb1");
        a5_n2.setAvailability("Available");
        
        
        Ambulance a6_n2=new Ambulance();
        a6_n2.setName("MAmb2");
        a6_n2.setAvailability("Available");
        
        
       for(Organisation org: h2_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a5_n2);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a6_n2);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb1", "mamb1", a5_n2, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("mamb2", "mamb2", a6_n2, new AmbulanceRole());

           }
       }
        
        
        
        
        //drone creation
         Organisation droneOrg_n2=null;
         for (Type type : Organisation.Type.values())
         {
            if (type.getValue().equals(Type.DRONE.getValue()))
            {
            droneOrg_n2=((Emergency911Enterprise)(enterprise_n2_2)).getOrganizationDirectory().createOrganisation(type);
            }
        }
         
       //creating cambridge drone stations  
       DroneStation cambridgeDroneStation1=null;
        
        for(Organisation org:((Emergency911Enterprise)enterprise_n2_2).getOrganizationDirectory().getOrganisationList())
        {
            if(org instanceof DroneOrganisation)
            {
                cambridgeDroneStation1=((DroneOrganisation)org).getDroneDirectoryObject().addDroneStation();
                cambridgeDroneStation1.setDroneStationName("Cambridge Drone Station 1");
                cambridgeDroneStation1.setDroneStationAddress("1350 Massachusetts Avenue. Cambridge, MA 02138 ");
                 
            }
        }
        
      
      
        //adding drones to cambridge drone station
        Drone d1n2=new Drone();
        
        d1n2.setDroneId("Cambridge_Drone_1");
        d1n2.setStatus("Active");
        cambridgeDroneStation1.addDrone(d1n2);
        droneOrg_n2.getEmployeeDirectory().createEmployee(d1n2);
        droneOrg_n2.getUserAccountDirectory().createUserAccount("cambridgedrone1", "cambridgedrone1", d1n2, new DroneRole() );
     
        
        Drone d2n2= new Drone();
        d2n2.setDroneId("Cambridge_Drone_2");
        d2n2.setStatus("Active");
        cambridgeDroneStation1.addDrone(d2n2);
        droneOrg_n2.getEmployeeDirectory().createEmployee(d2n2);
        droneOrg_n2.getUserAccountDirectory().createUserAccount("cambridgedrone2", "cambridgedrone2", d2n2, new DroneRole() );
     
        
        Drone d3n2= new Drone();
        d3n2.setDroneId("Cambridge_Drone_3");
        d3n2.setStatus("Active");
         cambridgeDroneStation1.addDrone(d3n2);
        droneOrg_n2.getEmployeeDirectory().createEmployee(d3n2);
        droneOrg_n2.getUserAccountDirectory().createUserAccount("cambridgedrone3", "cambridgedrone3", d3n2, new DroneRole() );
     
        
        Drone d4n2=new Drone();
        d4n2.setDroneId("Cambridge_Drone_4");
        d4n2.setStatus("Active");
        cambridgeDroneStation1.addDrone(d4n2);
        droneOrg_n2.getEmployeeDirectory().createEmployee(d4n2);
        droneOrg_n2.getUserAccountDirectory().createUserAccount("cambridgedrone4", "cambridgedrone4", d4n2, new DroneRole() );
     
        
        Drone d5n2= new Drone();
        d5n2.setDroneId("Cambridge_Drone_5");
        d5n2.setStatus("Active");
        cambridgeDroneStation1.addDrone(d5n2);
        droneOrg_n2.getEmployeeDirectory().createEmployee(d5n2);
        droneOrg_n2.getUserAccountDirectory().createUserAccount("cambridgedrone5", "cambridgedrone5", d5n2, new DroneRole() );
     
        
       //end of cambridge network creation
        
        
        //Creating malden network
        Network n3=system.addNetwork();
        n3.setNetworkName("Malden");
        
        //creating enterprises in malden network

        Enterprise.EnterpriseType type3_n4 = EnterpriseType.EMEREGENCY911ENTERPRISE;
        Enterprise enterprise_n3_2 = n3.getEntDirObj().createAndAddEnterprise("MaldenDC",type3_n4,"77 Salem St, Malden, MA 02148" );
     
        Enterprise.EnterpriseType type2_n5 = EnterpriseType.POLICEENTERPRISE;
        Enterprise enterprisen2_5 = n3.getEntDirObj().createAndAddEnterprise("MaldenPolice",type2_n5,"" );
        
        //creating enterprise admin for malden
        
        EnterpriseAdmin maldenpsapAdmin=new EnterpriseAdmin();
        enterprise_n3_2.getEmployeeDirectory().createEmployee(maldenpsapAdmin);
        enterprise_n3_2.getUserAccountDirectory().createUserAccount("maldendc", "maldendc", maldenpsapAdmin, new Emergency911EnterpriseAdminRole());
       
        EnterpriseAdmin maldenPoliceAdmin=new EnterpriseAdmin();
        enterprisen2_5.getEmployeeDirectory().createEmployee(maldenPoliceAdmin);
        enterprisen2_5.getUserAccountDirectory().createUserAccount("maldenpolice", "maldenpolice", maldenPoliceAdmin, new PoliceAdminRole());
        maldenPoliceAdmin.setAvailable(true);
  
        //hospital creation
        
        Hospital h3_1=n3.addHospital("AFC Doctors Express Urgent Care Malden");
        h3_1.setHospitalName("AFC Doctors Express Urgent Care Malden");
        h3_1.setHospitalLocation("219 Centre St, Malden, MA 02148");
        h3_1.setSpeciality("Cardiothoracic");
        h3_1.setNumberOfBeds(50);
        h3_1.setNumberOfEmptyBeds(5);
      
        
        Hospital h3_3=n3.addHospital("CHA Malden Family Medicine Center");
        h3_3.setHospitalName("CHA Malden Family Medicine Center");
        h3_3.setHospitalLocation("195 Canal St, Malden, MA 02148");
        h3_3.setSpeciality("Orthopedic");
        h3_3.setNumberOfBeds(50);
        h3_3.setNumberOfEmptyBeds(5);
       
        //hospital admin creation
        
        HospitalEnterpriseAdmin h1n3=new HospitalEnterpriseAdmin();
        h1n3.setName("AFC Admin");
        h3_1.getEmployeeDirectory().createEmployee(h1n3);
        h3_1.getUserAccountDirectory().createUserAccount("afcadmin", "afcadmin", h1n3, new HospitalEnterpriseAdminRole());
         
        HospitalEnterpriseAdmin h2n3=new HospitalEnterpriseAdmin();
        h2n2.setName("CHA Malden Admin");
        h3_3.getEmployeeDirectory().createEmployee(h2n2);
        h3_3.getUserAccountDirectory().createUserAccount("chaadmin", "chaadmin", h2n2, new HospitalEnterpriseAdminRole());
         
        //creating doctors and ambulances in malden network
        
        h3_1.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h3_1.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
        Doctor d1_n3=new Doctor();
        d1_n3.setName("Doctor Josephs");
        d1_n3.setDoctorsAvailablityStatus(true);
        d1_n3.setDoctorsSpeciality("Heart attack");
        
        Doctor d2_n3=new Doctor();
        d2_n3.setName("Doctor Rahul");
        d2_n3.setDoctorsAvailablityStatus(true);
        d2_n3.setDoctorsSpeciality("Car accident-Body Injury");
       
        
       for(Organisation org: h3_1.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d1_n3);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d2_n3);

               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drjosephs", "drjosephs", d1_n3, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drrahul", "drrahul", d2_n3, new DoctorRole());
               
              
               
           }
       }
        h3_3.getOrganizationDirectory().createOrganisation(Type.DOCTOR);
        h3_3.getOrganizationDirectory().createOrganisation(Type.AMBULANCE);
       Doctor d3_n3=new Doctor();
        d3_n3.setName("Doctor Shobha");
        d3_n3.setDoctorsAvailablityStatus(true);
        d3_n3.setDoctorsSpeciality("Heart attack");
        
        Doctor d4_n3=new Doctor();
        d4_n3.setName("Doctor Paul");
        d4_n3.setDoctorsAvailablityStatus(true);
        d4_n3.setDoctorsSpeciality("Car accident-Body Injury");
       
        
       for(Organisation org: h3_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof DoctorOrganization)
           {
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d1_n3);
               ((DoctorOrganization)org).getEmployeeDirectory().createEmployee(d2_n3);
  
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drshobha", "drshobha", d1_n3, new DoctorRole());
               ((DoctorOrganization)org).getUserAccountDirectory().createUserAccount("drpaul", "drpaul", d2_n3, new DoctorRole());
               
              
               
           }
       }
       
        Ambulance a1_n3=new Ambulance();
        a1_n3.setName("AFC Amb1");
        a1_n3.setAvailability("Available");
        
        
        Ambulance a2_n3=new Ambulance();
        a2_n3.setName("AFC Amb2");
        a2_n3.setAvailability("Available");
        
       for(Organisation org: h3_1.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a1_n3);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a1_n3);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("afcamb1", "afcamb1", a1_n3, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("afcamb2", "afcamb2", a1_n3, new AmbulanceRole());

           }
       }
        
       
        Ambulance a3_n3=new Ambulance();
        a3_n2.setName("CHA MAl Amb1");
        a3_n2.setAvailability("Available");
        
        
        Ambulance a4_n3=new Ambulance();
        a4_n3.setName("CHA MAl Amb2");
        a4_n3.setAvailability("Available");
        
        
       for(Organisation org: h3_3.getOrganizationDirectory().getOrganisationList())
       {
           if(org instanceof AmbulanceOrganisation)
           {
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a5_n2);
               ((AmbulanceOrganisation)org).getEmployeeDirectory().createEmployee(a6_n2);
               
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("chaamb1", "chaamb1", a5_n2, new AmbulanceRole());
               ((AmbulanceOrganisation)org).getUserAccountDirectory().createUserAccount("chaamb2", "chaamb2", a6_n2, new AmbulanceRole());

           }
       }
        
    
       //drone creation
        Organisation droneOrg_n3=null;
         for (Type type : Organisation.Type.values())
         {
            if (type.getValue().equals(Type.DRONE.getValue()))
            {
            droneOrg_n3=((Emergency911Enterprise)(enterprise_n3_2)).getOrganizationDirectory().createOrganisation(type);
            }
        }
         
     
         //drone station creation in malden
         DroneStation maldenDroneStation1=null;
        
        for(Organisation org:((Emergency911Enterprise)enterprise_n3_2).getOrganizationDirectory().getOrganisationList())
        {
            if(org instanceof DroneOrganisation)
            {
                maldenDroneStation1=((DroneOrganisation)org).getDroneDirectoryObject().addDroneStation();
                maldenDroneStation1.setDroneStationName("Malden Drone Station 1");
                maldenDroneStation1.setDroneStationAddress("77 Salem St, Malden, MA 02148");
            }
        }
         
         
        //adding drones to malden drone station
        Drone d1n3=new Drone();     
        d1n3.setDroneId("Malden_Drone_1");
        d1n3.setStatus("Active");
        maldenDroneStation1.addDrone(d1n3);
        droneOrg_n3.getEmployeeDirectory().createEmployee(d1n3);
        droneOrg_n3.getUserAccountDirectory().createUserAccount("maldendrone1", "maldendrone1", d1n3, new DroneRole() );
     
        
        Drone d2n3= new Drone();
        d2n3.setDroneId("Malden_Drone_2");
        d2n3.setStatus("Active");
        maldenDroneStation1.addDrone(d2n3);
        droneOrg_n3.getEmployeeDirectory().createEmployee(d2n3);
        droneOrg_n3.getUserAccountDirectory().createUserAccount("maldendrone2", "maldendrone2", d2n3, new DroneRole() );
     
        
        Drone d3n3= new Drone();
        d3n3.setDroneId("Malden_Drone_3");
        d3n3.setStatus("Active");
        maldenDroneStation1.addDrone(d1n3);
        droneOrg_n3.getEmployeeDirectory().createEmployee(d3n3);
        droneOrg_n3.getUserAccountDirectory().createUserAccount("maldendrone3", "maldendrone3", d3n3, new DroneRole() );
     
        
        
        Drone d4n3= new Drone();
        d4n3.setDroneId("Malden_Drone_4");
        d4n3.setStatus("Active");
        maldenDroneStation1.addDrone(d4n3);
        droneOrg_n3.getEmployeeDirectory().createEmployee(d4n3);
        droneOrg_n3.getUserAccountDirectory().createUserAccount("maldendrone4", "maldendrone4", d4n3, new DroneRole() );
     
        
        Drone d5n3= new Drone();
        d5n3.setDroneId("Malden_Drone_5");
        d5n3.setStatus("Active");
        maldenDroneStation1.addDrone(d5n3);
        droneOrg_n3.getEmployeeDirectory().createEmployee(d5n3);
        droneOrg_n3.getUserAccountDirectory().createUserAccount("maldendrone5", "maldendrone5", d5n3, new DroneRole() );
     
      
        //end of malden network creation
  
        
        EmergencyAddressLocation e1=system.getDirectory().addEmeregncyLocation();
        e1.setAddress("44 Clearway Street, Boston, MA, 02115");
       
        
        EmergencyAddressLocation e2=system.getDirectory().addEmeregncyLocation();
        e2.setAddress("75 St Alphonsus St, Boston, MA 02120");
      
        
        EmergencyAddressLocation e3=system.getDirectory().addEmeregncyLocation();
        e3.setAddress("1 Arborway, Boston, MA 02130");
        
        
        EmergencyAddressLocation e4=system.getDirectory().addEmeregncyLocation();
        e4.setAddress("Franklin Park Rd, Dorchester, Boston, Massachusetts 02121");
       
        
        EmergencyAddressLocation e5=system.getDirectory().addEmeregncyLocation();
        e5.setAddress("25 Shattuck St, Boston, MA 02115");
       
        
        EmergencyAddressLocation e6=system.getDirectory().addEmeregncyLocation();
        e6.setAddress("55 Fruit St, Boston, MA 02114");
       
        
        return system;
 
    }

  
}
