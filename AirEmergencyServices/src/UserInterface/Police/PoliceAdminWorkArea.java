/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Police;

import EmergencySystem.Emergency.AccidentEmergency;
import EmergencySystem.Emergency.Description;
import EmergencySystem.Emergency.Emergency;
import EmergencySystem.Emergency.EmergencyDirectory;
import EmergencySystem.Emergency.FireEmergency;
import EmergencySystem.Emergency.MedicalEmergency;
import EmergencySystem.EmergencySystem;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Network.Network;
import Hospital.UserAccount.UserAccount;
import Hospital.WorkQueue.Emergency911DepartmentWorkRequest;
import Hospital.WorkQueue.WorkRequest;
import Person.Person;
import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_core;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import java.util.Date;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author abhisheksatbhai
 */
public class PoliceAdminWorkArea extends javax.swing.JPanel {

    /**
     * Creates new form PoliceAdminWorkArea
     */
    JPanel userProcessContainer;
    UserAccount account;
    EmergencySystem system;
    private EmergencyDirectory emergencyDirectory;
    Network network;
    Enterprise enterprise;
    private Emergency e;
    private String location;
    private boolean manageFields=false;
    public PoliceAdminWorkArea(JPanel userProcessContainer, UserAccount account, EmergencySystem system, Network network, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.account=account;
        this.system=system;
        this.network=network;
        this.enterprise=enterprise;
        this.emergencyDirectory = system.getEmergencyDirectory();
        emergencyTable.setFont(new Font("Serif", Font.BOLD, 16));
        populateEmergencyTable();
        manageFields(manageFields);
        alertFamBtn.setVisible(false);
       locateBtn.setVisible(false);
       populateEmergencyLocation();
       populateNatureOfEmergencyCombo();
       
       JTableHeader tableHeader = emergencyTable.getTableHeader();
       Font headerFont = new Font("Verdana", Font.PLAIN, 16);
       tableHeader.setFont(headerFont);
       
    }
    public void manageFields(boolean display)
    {
        nameLabel.setVisible(display);
        nameTF.setVisible(display);
        ageLabel.setVisible(display);
        ageTF.setVisible(display);
        addressLabel.setVisible(display);
        addressTF.setVisible(display);
        driverLicenseNumberLabel.setVisible(display);
        driversLicenseNumberTF.setVisible(display);
        carOwnedLabel.setVisible(display);
        carOwnedTF.setVisible(display);
        licensePlaeNumberLabel.setVisible(display);
        licensePlateTF.setVisible(display);
        phoneNumberLabel.setVisible(display);
        phoneNumberTF.setVisible(display);
        emergencyContactNumberLabel.setVisible(display);
        emergencyContactNumberTF.setVisible(display);
        photoLabel.setVisible(display);
        nameTF.setEditable(false);
        ageTF.setEditable(false);
        addressTF.setEditable(false);
        driversLicenseNumberTF.setEditable(false);
        carOwnedTF.setEditable(false);
        licensePlateTF.setEditable(false);
        phoneNumberTF.setEditable(false);
        emergencyContactNumberTF.setEditable(false);
            
    }
    
        public void populateNatureOfEmergencyCombo()
    {
         natureOfEmergencyCombo.removeAllItems();
        for (Emergency.EmergencyType type : Emergency.EmergencyType.values())
        {
                String value=type.getValue();
                natureOfEmergencyCombo.addItem(value);
        }
  
    }
        public void populateEmergencyLocation()
    {
       
        Random rand = new Random(); 
        int index=rand.nextInt(5);
         
         locationEmergencyTF.setText(system.getDirectory().getEmergencyAddressLocationList().get(index).getAddress());
    }
        public void populateDescriptionComboBox(String emrg)
    {
        descriptionCombo.removeAllItems();
        if(emrg.equalsIgnoreCase("Accident Emergency"))
        {
            AccidentEmergency accEmerg=new AccidentEmergency();
            for(Description d:accEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
        else if(emrg.equalsIgnoreCase("Medical Emergency"))
        {
            MedicalEmergency medEmerg=new MedicalEmergency();
          
            for(Description d:medEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Fire Emergency"))
        {
            FireEmergency fireEmerg=new FireEmergency();
            System.err.println("I am going in the fire emer");
            for(Description d:fireEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
    }
    
    public void populateEmergencyTable()
    {
        DefaultTableModel model = (DefaultTableModel) emergencyTable.getModel();
        
        model.setRowCount(0);
        
        for (WorkRequest workRequest:account.getWorkQueue().getWorkRequestList()){
            Object[] row = new Object[5];
            row[0]= ((Emergency911DepartmentWorkRequest) workRequest);
            row[1]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getCallersPhoneNumber();
            row[2]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency();
            row[3]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getDescription();
            row[4]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getEmergencyStatus();
            model.addRow(row);
        }
    }
    


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        emergencyTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        retrieveLicensePlateBtn = new javax.swing.JButton();
        licensePlateLabel = new javax.swing.JLabel();
        licensePlateNumberTF = new javax.swing.JTextField();
        retrieveDataBtn = new javax.swing.JButton();
        photoLabel = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        ageLabel = new javax.swing.JLabel();
        addressLabel = new javax.swing.JLabel();
        driverLicenseNumberLabel = new javax.swing.JLabel();
        carOwnedLabel = new javax.swing.JLabel();
        licensePlaeNumberLabel = new javax.swing.JLabel();
        phoneNumberLabel = new javax.swing.JLabel();
        nameTF = new javax.swing.JTextField();
        ageTF = new javax.swing.JTextField();
        addressTF = new javax.swing.JTextField();
        licensePlateTF = new javax.swing.JTextField();
        phoneNumberTF = new javax.swing.JTextField();
        driversLicenseNumberTF = new javax.swing.JTextField();
        carOwnedTF = new javax.swing.JTextField();
        emergencyContactNumberTF = new javax.swing.JTextField();
        emergencyContactNumberLabel = new javax.swing.JLabel();
        alertFamBtn = new javax.swing.JButton();
        pickTheCall = new javax.swing.JPanel();
        emergencyLocation = new javax.swing.JLabel();
        natureOfEmergency = new javax.swing.JLabel();
        description = new javax.swing.JLabel();
        locationEmergencyTF = new javax.swing.JTextField();
        locationErr = new javax.swing.JLabel();
        callerPhoneNumberErr = new javax.swing.JLabel();
        natureOfemergencyErr = new javax.swing.JLabel();
        descriptionErr = new javax.swing.JLabel();
        reportAnEmergencyBtn = new javax.swing.JButton();
        locateBtn = new javax.swing.JButton();
        priorityLabel = new javax.swing.JLabel();
        prioritySlider = new javax.swing.JSlider();
        natureOfEmergencyCombo = new javax.swing.JComboBox();
        descriptionCombo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 102, 153));

        jTabbedPane1.setBackground(new java.awt.Color(204, 255, 204));
        jTabbedPane1.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        emergencyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Phone number", "Location of emergency", "Description", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(emergencyTable);
        if (emergencyTable.getColumnModel().getColumnCount() > 0) {
            emergencyTable.getColumnModel().getColumn(2).setMinWidth(250);
            emergencyTable.getColumnModel().getColumn(2).setPreferredWidth(250);
            emergencyTable.getColumnModel().getColumn(2).setMaxWidth(250);
        }

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Accident List");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(223, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(675, 675, 675))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 754, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(jLabel2)
                .addGap(72, 72, 72)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(417, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Work Queue", jPanel1);

        jPanel2.setBackground(new java.awt.Color(0, 102, 153));

        retrieveLicensePlateBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        retrieveLicensePlateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449766076_finance-10.png"))); // NOI18N
        retrieveLicensePlateBtn.setText("Retrieve the license plate");
        retrieveLicensePlateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retrieveLicensePlateBtnActionPerformed(evt);
            }
        });

        licensePlateLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        licensePlateLabel.setForeground(new java.awt.Color(255, 255, 255));
        licensePlateLabel.setText("Retrieve person data from license plate:");

        retrieveDataBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        retrieveDataBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449765858_cloud-arrow-down.png"))); // NOI18N
        retrieveDataBtn.setText("Retrieve data");
        retrieveDataBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retrieveDataBtnActionPerformed(evt);
            }
        });

        photoLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        nameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel.setText("Name:");

        ageLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ageLabel.setForeground(new java.awt.Color(255, 255, 255));
        ageLabel.setText("Age:");

        addressLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addressLabel.setForeground(new java.awt.Color(255, 255, 255));
        addressLabel.setText("Address:");

        driverLicenseNumberLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        driverLicenseNumberLabel.setForeground(new java.awt.Color(255, 255, 255));
        driverLicenseNumberLabel.setText("Drivers license number:");

        carOwnedLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        carOwnedLabel.setForeground(new java.awt.Color(255, 255, 255));
        carOwnedLabel.setText("Car owned:");

        licensePlaeNumberLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        licensePlaeNumberLabel.setForeground(new java.awt.Color(255, 255, 255));
        licensePlaeNumberLabel.setText("License plate number:");

        phoneNumberLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        phoneNumberLabel.setForeground(new java.awt.Color(255, 255, 255));
        phoneNumberLabel.setText("Phone number:");

        addressTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addressTFActionPerformed(evt);
            }
        });

        emergencyContactNumberLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        emergencyContactNumberLabel.setForeground(new java.awt.Color(255, 255, 255));
        emergencyContactNumberLabel.setText("Emergency contact number:");

        alertFamBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        alertFamBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449766211_send.png"))); // NOI18N
        alertFamBtn.setText("Alert the family about the incidence");
        alertFamBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alertFamBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(photoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(addressLabel)
                                    .addGap(77, 77, 77)
                                    .addComponent(driversLicenseNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(ageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(77, 77, 77)
                                    .addComponent(ageTF, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(78, 78, 78)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(phoneNumberLabel)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(emergencyContactNumberLabel)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(emergencyContactNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(driverLicenseNumberLabel)
                                        .addComponent(nameLabel)
                                        .addComponent(licensePlaeNumberLabel)
                                        .addComponent(carOwnedLabel))
                                    .addGap(61, 61, 61)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(licensePlateTF, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(addressTF, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(carOwnedTF, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(phoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(licensePlateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(licensePlateNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84)
                        .addComponent(retrieveDataBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(retrieveLicensePlateBtn)
                        .addGap(237, 237, 237)))
                .addContainerGap(149, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(alertFamBtn)
                .addGap(380, 380, 380))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {addressTF, carOwnedTF, emergencyContactNumberTF, licensePlateTF, nameTF, phoneNumberTF});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(retrieveLicensePlateBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(licensePlateNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(licensePlateLabel)
                    .addComponent(retrieveDataBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addressTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(driverLicenseNumberLabel))
                        .addGap(72, 72, 72)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(licensePlaeNumberLabel)
                            .addComponent(licensePlateTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(67, 67, 67)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(carOwnedTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(carOwnedLabel))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(82, 82, 82)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(ageTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(phoneNumberLabel)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(64, 64, 64)
                                .addComponent(phoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(emergencyContactNumberLabel)
                            .addComponent(emergencyContactNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(driversLicenseNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(photoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 125, Short.MAX_VALUE)
                .addComponent(alertFamBtn)
                .addGap(85, 85, 85))
        );

        jTabbedPane1.addTab("Search person", jPanel2);

        pickTheCall.setBackground(new java.awt.Color(0, 102, 153));
        pickTheCall.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pickTheCall.setForeground(new java.awt.Color(255, 255, 255));

        emergencyLocation.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        emergencyLocation.setForeground(new java.awt.Color(255, 255, 255));
        emergencyLocation.setText("Location of the emergency:");

        natureOfEmergency.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        natureOfEmergency.setForeground(new java.awt.Color(255, 255, 255));
        natureOfEmergency.setText("Nature of the emergency: ");

        description.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        description.setForeground(new java.awt.Color(255, 255, 255));
        description.setText("Description:");

        locationEmergencyTF.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationEmergencyTF.setEnabled(false);
        locationEmergencyTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locationEmergencyTFActionPerformed(evt);
            }
        });

        callerPhoneNumberErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        callerPhoneNumberErr.setForeground(new java.awt.Color(102, 102, 102));

        reportAnEmergencyBtn.setBackground(new java.awt.Color(204, 204, 204));
        reportAnEmergencyBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        reportAnEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn.setText("Submit request");
        reportAnEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtnActionPerformed(evt);
            }
        });

        locateBtn.setBackground(new java.awt.Color(204, 204, 204));
        locateBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn.setText("Locate the emergency location");
        locateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtnActionPerformed(evt);
            }
        });

        priorityLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        priorityLabel.setForeground(new java.awt.Color(255, 255, 255));
        priorityLabel.setText("Priority:");

        prioritySlider.setMajorTickSpacing(1);
        prioritySlider.setMaximum(10);
        prioritySlider.setMinorTickSpacing(1);
        prioritySlider.setPaintLabels(true);
        prioritySlider.setPaintTicks(true);
        prioritySlider.setValue(0);

        natureOfEmergencyCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Accident ", "Theft\t" }));
        natureOfEmergencyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natureOfEmergencyComboActionPerformed(evt);
            }
        });

        descriptionCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout pickTheCallLayout = new javax.swing.GroupLayout(pickTheCall);
        pickTheCall.setLayout(pickTheCallLayout);
        pickTheCallLayout.setHorizontalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(192, 192, 192)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(emergencyLocation, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(natureOfEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(90, 90, 90)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(0, 12, Short.MAX_VALUE)
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addComponent(descriptionCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(25, 25, 25)))
                        .addGap(373, 373, 373))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(locateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(308, 308, 308))
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(346, 346, 346)
                .addComponent(reportAnEmergencyBtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pickTheCallLayout.setVerticalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(emergencyLocation, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(134, 134, 134))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(natureOfEmergency)
                                .addGap(26, 26, 26))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(priorityLabel))
                .addGap(58, 58, 58)
                .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(locateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(283, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Request Drone", pickTheCall);

        jLabel3.setBackground(new java.awt.Color(0, 102, 153));
        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Police Admin Work Area");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1053, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 743, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(10, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void retrieveLicensePlateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retrieveLicensePlateBtnActionPerformed
        // TODO add your handling code here:
        int row=emergencyTable.getSelectedRow();
        Emergency emergency=null;
        if(row>=0)
        {
        emergency=(Emergency) emergencyTable.getValueAt(row, 2);
        String imageUrl=emergency.getLicensePlateURL();
     //   imageUrl.replace("\\", "\\\\");
        StringBuffer sb = new StringBuffer();
        
       // imageUrl=sb.append("\"").append(imageUrl).append("\"");
           
            String finalPath=""; 
          java.net.URL imgURL = getClass().getResource(imageUrl);
    if (imgURL != null) {
     
         
       File file=new File(imgURL.toString());
        finalPath=file.toString().replaceAll("\\\\", "\\\\\\\\");
        finalPath=finalPath.substring(7,finalPath.length());
       //System.out.println(finalPath);
         
    } else {
        System.err.println("Couldn't find file: " + imageUrl);
       
    }
            
        opencv_core.IplImage image= cvLoadImage(finalPath);
        
        
        final CanvasFrame canvas =new CanvasFrame("License plate");
        canvas.showImage(image);
        canvas.setCanvasSize(500, 300);
        //canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        }
        
        else
        {
            JOptionPane.showMessageDialog(this,"Choose an emeregncy from the table");
        }
    }//GEN-LAST:event_retrieveLicensePlateBtnActionPerformed

    private void retrieveDataBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retrieveDataBtnActionPerformed
        // TODO add your handling code here:
       
       
        String licensePlateNumber=licensePlateNumberTF.getText();
        boolean status=false;
        for(Person p:system.getPersonDirectory().getPersonList())
        {
            if(p.getLicensePlateNumber().equalsIgnoreCase(licensePlateNumber))
            {
                status=true;
                manageFields(true);
                nameTF.setText(p.getName());
                ageTF.setText(Integer.toString(p.getAge()));
                addressTF.setText(p.getAddress());
                driversLicenseNumberTF.setText(Integer.toString(p.getDriversLicenseNumber()));
                carOwnedTF.setText(p.getCarOwned());
                licensePlateTF.setText(p.getLicensePlateNumber());
                phoneNumberTF.setText(p.getPhoneNumber());
                emergencyContactNumberTF.setText(p.getEmergencyContactNumber());
               alertFamBtn.setVisible(true);
                 ImageIcon imgThisImg =null;
                java.net.URL imgURL = getClass().getResource(p.getPhoto());
                if (imgURL != null) 
                {
                imgThisImg=new ImageIcon(imgURL);
                } 
              
                photoLabel.setIcon(imgThisImg);  
            }    
           
        }
        
        if(status==false)
        {
            JOptionPane.showMessageDialog(this,"This car has not been registered, cannot locate in database!");
        }
    }//GEN-LAST:event_retrieveDataBtnActionPerformed

    private void addressTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addressTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addressTFActionPerformed

    private void alertFamBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alertFamBtnActionPerformed
        // TODO add your handling code here:
        int row=emergencyTable.getSelectedRow();
        if(row>=0)
        {
            Emergency eme=(Emergency) emergencyTable.getValueAt(row,2);
            eme.setEmergencyStatus("Completed");
            Date d=new Date();
            eme.setAlertedThePatientEmergencyContact(d);
            eme.setTotatTimeTakenByPoliceToAlertEmergencyContact((eme.getAlertedThePatientEmergencyContact().getTime()-eme.getPoliceAlerted().getTime())/1000%60);
            System.err.println("the timw when police is alerted and family is alerted "+eme.getTotatTimeTakenByPoliceToAlertEmergencyContact());
            JOptionPane.showMessageDialog(this,"The patients family has been informed about the accident");
        }
        
        else{
        JOptionPane.showMessageDialog(this,"Choose a emergency from the table");
        }
    }//GEN-LAST:event_alertFamBtnActionPerformed

    private void locationEmergencyTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locationEmergencyTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_locationEmergencyTFActionPerformed

    private void reportAnEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtnActionPerformed
        // TODO add your handling code here:

        //        if(callersPhoneNumberTF.getText().matches("[0-9]+")&& (callersPhoneNumberTF.getText().length()==10))

        //        {
            callerPhoneNumberErr.setText("");
            //            findTheDistBtn.setEnabled(true);
            //            routeCallBtn.setEnabled(true);
            e=emergencyDirectory.createEmergency();
            e.setLocationOfEmergency(locationEmergencyTF.getText());
            //            e.setCallersPhoneNumber(callersPhoneNumberTF.getText());
            e.setName(account.getUsername()); 

            e.setNatureOfEmergency((String) natureOfEmergencyCombo.getSelectedItem());
            e.setDescription((String) descriptionCombo.getSelectedItem());
            e.setPriority(prioritySlider.getValue());
            e.setEmergencyStatus("Reported");
            Date d = new Date();
            e.setReportedTime(d);
            JOptionPane.showMessageDialog(this,"Drone request has been submitted!");
            locateBtn.setEnabled(true);
            reportAnEmergencyBtn.setEnabled(false);
            //            callersPhoneNumberTF.setEditable(false);
            location=e.getLocationOfEmergency().replaceAll("\\s","+");
            System.out.println(emergencyDirectory+"test"+e.getEmergencyStatus()+"TEST");
            //        }

        ////        else
        ////        {
            //
            //            if(!callersPhoneNumberTF.getText().matches("[0-9]+"))
            //            {
                //                callerPhoneNumberErr.setText("Enter only numeric values");
                //            }
            //
            //            else
            //            {
                //                callerPhoneNumberErr.setText("Enter a 10 digit numeric value");
                //            }
            //        }
    }//GEN-LAST:event_reportAnEmergencyBtnActionPerformed

    private void locateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            //
            //        Browser browser = BrowserFactory.create();
            //        JFrame frame = new JFrame("JxBrowser Google Maps");
            //        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
            //        frame.setSize(700, 500);
            //        frame.setLocationRelativeTo(null);
            //        frame.setVisible(true);
            //        browser.loadURL("http://maps.google.com");
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtnActionPerformed

    private void natureOfEmergencyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natureOfEmergencyComboActionPerformed
        // TODO add your handling code here:
        String emrg = (String) natureOfEmergencyCombo.getSelectedItem();
        if (emrg != null){
                        populateDescriptionComboBox(emrg);
        }
    }//GEN-LAST:event_natureOfEmergencyComboActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
        populateEmergencyTable();
    }//GEN-LAST:event_jTabbedPane1MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addressLabel;
    private javax.swing.JTextField addressTF;
    private javax.swing.JLabel ageLabel;
    private javax.swing.JTextField ageTF;
    private javax.swing.JButton alertFamBtn;
    private javax.swing.JLabel callerPhoneNumberErr;
    private javax.swing.JLabel carOwnedLabel;
    private javax.swing.JTextField carOwnedTF;
    private javax.swing.JLabel description;
    private javax.swing.JComboBox descriptionCombo;
    private javax.swing.JLabel descriptionErr;
    private javax.swing.JLabel driverLicenseNumberLabel;
    private javax.swing.JTextField driversLicenseNumberTF;
    private javax.swing.JLabel emergencyContactNumberLabel;
    private javax.swing.JTextField emergencyContactNumberTF;
    private javax.swing.JLabel emergencyLocation;
    private javax.swing.JTable emergencyTable;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel licensePlaeNumberLabel;
    private javax.swing.JLabel licensePlateLabel;
    private javax.swing.JTextField licensePlateNumberTF;
    private javax.swing.JTextField licensePlateTF;
    private javax.swing.JButton locateBtn;
    private javax.swing.JTextField locationEmergencyTF;
    private javax.swing.JLabel locationErr;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField nameTF;
    private javax.swing.JLabel natureOfEmergency;
    private javax.swing.JComboBox natureOfEmergencyCombo;
    private javax.swing.JLabel natureOfemergencyErr;
    private javax.swing.JLabel phoneNumberLabel;
    private javax.swing.JTextField phoneNumberTF;
    private javax.swing.JLabel photoLabel;
    private javax.swing.JPanel pickTheCall;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JSlider prioritySlider;
    private javax.swing.JButton reportAnEmergencyBtn;
    private javax.swing.JButton retrieveDataBtn;
    private javax.swing.JButton retrieveLicensePlateBtn;
    // End of variables declaration//GEN-END:variables
}
