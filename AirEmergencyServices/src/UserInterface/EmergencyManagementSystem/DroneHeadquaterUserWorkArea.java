/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.EmergencyManagementSystem;

import Employee.EnterpriseAdmin;
import Employee.HospitalEnterpriseAdmin;
import EmergencySystem.Emergency.AccidentEmergency;
import EmergencySystem.Emergency.Description;
import EmergencySystem.Emergency.Emergency;
import EmergencySystem.Emergency.Emergency.EmergencyType;
import EmergencySystem.Emergency.EmergencyDirectory;
import EmergencySystem.Emergency.FireEmergency;
import EmergencySystem.Emergency.MedicalEmergency;
import EmergencySystem.Emergency.TacticalEmergency;
import EmergencySystem.EmergencySystem;
import EmergencySystem.Enterprise.Emergency911Enterprise;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Enterprise.Enterprise.EnterpriseType;
import EmergencySystem.Network.Network;
import Hospital.Hospital;
import Hospital.Role.Emergency911EnterpriseAdminRole;
import Hospital.Role.HospitalEnterpriseAdminRole;
import Hospital.UserAccount.UserAccount;
import Hospital.WorkQueue.Emergency911DepartmentWorkRequest;
import Hospital.WorkQueue.WorkRequest;
import com.google.gson.Gson;
import com.sl.DistancePojo;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author abhisheksatbhai
 */
public class DroneHeadquaterUserWorkArea extends javax.swing.JPanel  {

    /**
     * Creates new form EmergencyManagementSystemWorkArea
     */
    private JPanel userProcessContainer;
    private EmergencySystem system;
    private EmergencyDirectory emergencyDirectory;
    private String location;
    private Emergency e;
    private boolean networkAlreadyPresent;
    private boolean enterpriseAlreadyPresent;
    private boolean hospitalAlreadyPresent;
    private boolean userNameIsUnique;
    private boolean entUsernameIsUnique;
   
    private UserAccount userAccount;
    public DroneHeadquaterUserWorkArea(JPanel userProcessContainer, EmergencySystem system, UserAccount userAccount)
    {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.system=system;
        this.userAccount=userAccount;
       
        emergencyDirectory=system.getEmergencyDirectory();
        locateBtn.setEnabled(false);
  
        populateEmergencyLocation();

        populateNatureOfEmergencyCombo();
  
        populateTable2();
        locateBtn.setVisible(false);
        locateBtn1.setVisible(false);
        reportAnEmergencyBtn1.setVisible(false);
        
    }
    

    
    public void populateEmergencyLocation()
    {
       
        Random rand = new Random(); 
        int index=rand.nextInt(5);
         
         locationEmergencyTF.setText(system.getDirectory().getEmergencyAddressLocationList().get(index).getAddress());
    }



    public void populateNatureOfEmergencyCombo()
    {
         natureOfEmergencyCombo.removeAllItems();
        for (EmergencyType type : Emergency.EmergencyType.values())
        {
                String value=type.getValue();
                natureOfEmergencyCombo.addItem(value);
        }
  
    }
    
    public void populateDescriptionComboBox(String emrg)
    {
        descriptionCombo.removeAllItems();
        if(emrg.equalsIgnoreCase("Accident Emergency"))
        {
            AccidentEmergency accEmerg=new AccidentEmergency();
            for(Description d:accEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
        else if(emrg.equalsIgnoreCase("Medical Emergency"))
        {
            MedicalEmergency medEmerg=new MedicalEmergency();
          
            for(Description d:medEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Fire Emergency"))
        {
            FireEmergency fireEmerg=new FireEmergency();
            System.err.println("I am going in the fire emer");
            for(Description d:fireEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Tactical Emergency"))
        {
            TacticalEmergency tactEmerg=new TacticalEmergency();
     
            for(Description d:tactEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
    }

    

  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        pickTheCall = new javax.swing.JPanel();
        emergencyLocation = new javax.swing.JLabel();
        phoneNumberOfCaller = new javax.swing.JLabel();
        natureOfEmergency = new javax.swing.JLabel();
        description = new javax.swing.JLabel();
        locationEmergencyTF = new javax.swing.JTextField();
        callersPhoneNumberTF = new javax.swing.JTextField();
        locationErr = new javax.swing.JLabel();
        callerPhoneNumberErr = new javax.swing.JLabel();
        natureOfemergencyErr = new javax.swing.JLabel();
        descriptionErr = new javax.swing.JLabel();
        reportAnEmergencyBtn = new javax.swing.JButton();
        locateBtn = new javax.swing.JButton();
        priorityLabel = new javax.swing.JLabel();
        prioritySlider = new javax.swing.JSlider();
        natureOfEmergencyCombo = new javax.swing.JComboBox();
        descriptionCombo = new javax.swing.JComboBox();
        pickTheCall1 = new javax.swing.JPanel();
        locationErr1 = new javax.swing.JLabel();
        callerPhoneNumberErr1 = new javax.swing.JLabel();
        natureOfemergencyErr1 = new javax.swing.JLabel();
        descriptionErr1 = new javax.swing.JLabel();
        reportAnEmergencyBtn1 = new javax.swing.JButton();
        locateBtn1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        emergencyTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        header = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 102, 153));
        setPreferredSize(new java.awt.Dimension(809, 853));

        jTabbedPane1.setBackground(new java.awt.Color(255, 153, 51));
        jTabbedPane1.setForeground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        pickTheCall.setBackground(new java.awt.Color(0, 102, 153));
        pickTheCall.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pickTheCall.setForeground(new java.awt.Color(255, 255, 255));
        pickTheCall.setPreferredSize(new java.awt.Dimension(809, 853));

        emergencyLocation.setBackground(new java.awt.Color(0, 51, 153));
        emergencyLocation.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        emergencyLocation.setForeground(new java.awt.Color(255, 255, 255));
        emergencyLocation.setText("Location of request:");

        phoneNumberOfCaller.setBackground(new java.awt.Color(0, 51, 153));
        phoneNumberOfCaller.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        phoneNumberOfCaller.setForeground(new java.awt.Color(255, 255, 255));
        phoneNumberOfCaller.setText("Phone number:");

        natureOfEmergency.setBackground(new java.awt.Color(0, 51, 153));
        natureOfEmergency.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        natureOfEmergency.setForeground(new java.awt.Color(255, 255, 255));
        natureOfEmergency.setText("Nature of the request: ");

        description.setBackground(new java.awt.Color(0, 51, 153));
        description.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        description.setForeground(new java.awt.Color(255, 255, 255));
        description.setText("Description:");

        locationEmergencyTF.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationEmergencyTF.setEnabled(false);
        locationEmergencyTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locationEmergencyTFActionPerformed(evt);
            }
        });

        callerPhoneNumberErr.setFont(new java.awt.Font("Tahoma", 2, 13)); // NOI18N
        callerPhoneNumberErr.setForeground(new java.awt.Color(255, 255, 255));

        reportAnEmergencyBtn.setBackground(new java.awt.Color(255, 255, 255));
        reportAnEmergencyBtn.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        reportAnEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn.setText("Request Drone");
        reportAnEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtnActionPerformed(evt);
            }
        });

        locateBtn.setBackground(new java.awt.Color(255, 153, 51));
        locateBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn.setForeground(new java.awt.Color(255, 255, 255));
        locateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn.setText("Locate the emergency location");
        locateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtnActionPerformed(evt);
            }
        });

        priorityLabel.setBackground(new java.awt.Color(0, 51, 153));
        priorityLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        priorityLabel.setForeground(new java.awt.Color(255, 255, 255));
        priorityLabel.setText("Priority:");

        prioritySlider.setBackground(new java.awt.Color(204, 204, 204));
        prioritySlider.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        prioritySlider.setForeground(new java.awt.Color(255, 255, 255));
        prioritySlider.setMajorTickSpacing(1);
        prioritySlider.setMaximum(10);
        prioritySlider.setMinorTickSpacing(1);
        prioritySlider.setPaintLabels(true);
        prioritySlider.setPaintTicks(true);
        prioritySlider.setValue(0);

        natureOfEmergencyCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        natureOfEmergencyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natureOfEmergencyComboActionPerformed(evt);
            }
        });

        descriptionCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout pickTheCallLayout = new javax.swing.GroupLayout(pickTheCall);
        pickTheCall.setLayout(pickTheCallLayout);
        pickTheCallLayout.setHorizontalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(phoneNumberOfCaller, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emergencyLocation)
                            .addComponent(natureOfEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reportAnEmergencyBtn)
                            .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(descriptionErr)))
                        .addGap(146, 146, 146))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(112, 112, 112)
                                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(92, 92, 92)
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(prioritySlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(descriptionCombo, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(locateBtn)
                                            .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(callersPhoneNumberTF)
                                                .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(natureOfEmergencyCombo, 0, 239, Short.MAX_VALUE)
                                                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pickTheCallLayout.setVerticalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emergencyLocation)
                    .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneNumberOfCaller))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(natureOfEmergency)
                    .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(72, 72, 72)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(priorityLabel)
                            .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(84, 84, 84)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(locateBtn))
                        .addContainerGap(48, Short.MAX_VALUE))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Request Drone On Call ", pickTheCall);

        pickTheCall1.setBackground(new java.awt.Color(0, 102, 153));
        pickTheCall1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pickTheCall1.setPreferredSize(new java.awt.Dimension(809, 853));

        callerPhoneNumberErr1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        callerPhoneNumberErr1.setForeground(new java.awt.Color(102, 102, 102));

        reportAnEmergencyBtn1.setBackground(new java.awt.Color(255, 255, 255));
        reportAnEmergencyBtn1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        reportAnEmergencyBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn1.setText("Process request");
        reportAnEmergencyBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtn1ActionPerformed(evt);
            }
        });

        locateBtn1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn1.setText("Locate the emergency location");
        locateBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtn1ActionPerformed(evt);
            }
        });

        emergencyTable2.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        emergencyTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Phone number", "Location of emergency", "Nature of emergency", "Priority", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(emergencyTable2);

        javax.swing.GroupLayout pickTheCall1Layout = new javax.swing.GroupLayout(pickTheCall1);
        pickTheCall1.setLayout(pickTheCall1Layout);
        pickTheCall1Layout.setHorizontalGroup(
            pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCall1Layout.createSequentialGroup()
                .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCall1Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 932, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(locationErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(callerPhoneNumberErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCall1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descriptionErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(741, 741, 741)
                        .addComponent(natureOfemergencyErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pickTheCall1Layout.createSequentialGroup()
                                .addGap(344, 344, 344)
                                .addComponent(reportAnEmergencyBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pickTheCall1Layout.createSequentialGroup()
                                .addGap(318, 318, 318)
                                .addComponent(locateBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pickTheCall1Layout.setVerticalGroup(
            pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCall1Layout.createSequentialGroup()
                .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(locationErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(callerPhoneNumberErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(natureOfemergencyErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(reportAnEmergencyBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(locateBtn1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)))
                .addComponent(descriptionErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(131, Short.MAX_VALUE))
        );

        pickTheCall1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {locateBtn1, reportAnEmergencyBtn1});

        jTabbedPane1.addTab("Work Queue", pickTheCall1);

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));

        header.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        header.setForeground(new java.awt.Color(255, 255, 255));
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Drone Headquarter's User");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(header, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 34, Short.MAX_VALUE)
                .addComponent(header))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
        populateTable2();
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void locateBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtn1ActionPerformed
        // TODO add your handling code here:
        try
        {
            //
            //        Browser browser = BrowserFactory.create();
            //        JFrame frame = new JFrame("JxBrowser Google Maps");
            //        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
            //        frame.setSize(700, 500);
            //        frame.setLocationRelativeTo(null);
            //        frame.setVisible(true);
            //        browser.loadURL("http://maps.google.com");
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtn1ActionPerformed

    private void reportAnEmergencyBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtn1ActionPerformed
        // TODO add your handling code here:
        int rowSelected = emergencyTable2.getSelectedRow();
        //            System.out.print(rowSelected);
        if (rowSelected>=0){
            e = emergencyDirectory.getEmergencyList().get(rowSelected);

            if(e.getEmergencyStatus().equalsIgnoreCase("Completed")){
                JOptionPane.showMessageDialog(this,"Request is already Completed!");
            }
            else{
                JOptionPane.showMessageDialog(this,"Request has been processed!");
        
            }
        }
        else
        {
            JOptionPane.showMessageDialog(this,"Please select a request");
        }
    }//GEN-LAST:event_reportAnEmergencyBtn1ActionPerformed

    private void natureOfEmergencyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natureOfEmergencyComboActionPerformed
        // TODO add your handling code here:
        String emrg = (String) natureOfEmergencyCombo.getSelectedItem();
        if (emrg != null){
            populateDescriptionComboBox(emrg);
        }
    }//GEN-LAST:event_natureOfEmergencyComboActionPerformed

    private void locateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtnActionPerformed

    private void reportAnEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtnActionPerformed
        // TODO add your handling code here:

        if(callersPhoneNumberTF.getText().matches("[0-9]+")&& (callersPhoneNumberTF.getText().length()==10))

        {
            callerPhoneNumberErr.setText("");
            e=emergencyDirectory.createEmergency();
            e.setLocationOfEmergency(locationEmergencyTF.getText());
            e.setCallersPhoneNumber(callersPhoneNumberTF.getText());
            e.setNatureOfEmergency((String) natureOfEmergencyCombo.getSelectedItem());
            e.setDescription((String) descriptionCombo.getSelectedItem());
            e.setPriority(prioritySlider.getValue());
            e.setEmergencyStatus("Reported");
            Date d = new Date();
            e.setReportedTime(d);
            JOptionPane.showMessageDialog(this,"Drone request has been submitted!!");
            locateBtn.setEnabled(true);
            reportAnEmergencyBtn.setEnabled(false);
            callersPhoneNumberTF.setEditable(false);
            location=e.getLocationOfEmergency().replaceAll("\\s","+");
        }

        else
        {

            if(!callersPhoneNumberTF.getText().matches("[0-9]+"))
            {
                callerPhoneNumberErr.setText("Enter only numeric values");
            }

            else
            {
                callerPhoneNumberErr.setText("Enter a 10 digit numeric value");
            }
        }

        callersPhoneNumberTF.setText("");
        callersPhoneNumberTF.setEditable(true);
        reportAnEmergencyBtn.setEnabled(true);
        populateEmergencyLocation();
    }//GEN-LAST:event_reportAnEmergencyBtnActionPerformed

    private void locationEmergencyTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locationEmergencyTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_locationEmergencyTFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel callerPhoneNumberErr;
    private javax.swing.JLabel callerPhoneNumberErr1;
    private javax.swing.JTextField callersPhoneNumberTF;
    private javax.swing.JLabel description;
    private javax.swing.JComboBox descriptionCombo;
    private javax.swing.JLabel descriptionErr;
    private javax.swing.JLabel descriptionErr1;
    private javax.swing.JLabel emergencyLocation;
    private javax.swing.JTable emergencyTable2;
    private javax.swing.JLabel header;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton locateBtn;
    private javax.swing.JButton locateBtn1;
    private javax.swing.JTextField locationEmergencyTF;
    private javax.swing.JLabel locationErr;
    private javax.swing.JLabel locationErr1;
    private javax.swing.JLabel natureOfEmergency;
    private javax.swing.JComboBox natureOfEmergencyCombo;
    private javax.swing.JLabel natureOfemergencyErr;
    private javax.swing.JLabel natureOfemergencyErr1;
    private javax.swing.JLabel phoneNumberOfCaller;
    private javax.swing.JPanel pickTheCall;
    private javax.swing.JPanel pickTheCall1;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JSlider prioritySlider;
    private javax.swing.JButton reportAnEmergencyBtn;
    private javax.swing.JButton reportAnEmergencyBtn1;
    // End of variables declaration//GEN-END:variables


    private void populateTable2() {
        DefaultTableModel model = (DefaultTableModel) emergencyTable2.getModel();
        
        model.setRowCount(0);
        for(Emergency e :emergencyDirectory.getEmergencyList())
        {
//            System.out.println(e.getCallersPhoneNumber());
            Object[] row = new Object[6];
            row[0]=  e.getName();
            row[1]= e.getCallersPhoneNumber();
            row[3]= e.getNatureOfEmergency();
            row[2]= e.getLocationOfEmergency();
            row[4]= e.getPriority();
            row[5]= e.getEmergencyStatus();
            
            model.addRow(row);
        }
        
    }
}
