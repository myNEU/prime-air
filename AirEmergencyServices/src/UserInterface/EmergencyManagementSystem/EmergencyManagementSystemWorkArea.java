/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.EmergencyManagementSystem;

import Employee.EnterpriseAdmin;
import Employee.HospitalEnterpriseAdmin;
import EmergencySystem.Emergency.AccidentEmergency;
import EmergencySystem.Emergency.Description;
import EmergencySystem.Emergency.Emergency;
import EmergencySystem.Emergency.Emergency.EmergencyType;
import EmergencySystem.Emergency.EmergencyDirectory;
import EmergencySystem.Emergency.FireEmergency;
import EmergencySystem.Emergency.MedicalEmergency;
import EmergencySystem.Emergency.TacticalEmergency;
import EmergencySystem.EmergencySystem;
import EmergencySystem.Enterprise.Emergency911Enterprise;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Enterprise.Enterprise.EnterpriseType;
import EmergencySystem.Network.Network;
import Hospital.Hospital;
import Hospital.Role.Emergency911EnterpriseAdminRole;
import Hospital.Role.HospitalEnterpriseAdminRole;
import Hospital.UserAccount.UserAccount;
import Hospital.WorkQueue.Emergency911DepartmentWorkRequest;
import Hospital.WorkQueue.WorkRequest;
import com.google.gson.Gson;
import com.sl.DistancePojo;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author abhisheksatbhai
 */
public class EmergencyManagementSystemWorkArea extends javax.swing.JPanel  {

    /**
     * Creates new form EmergencyManagementSystemWorkArea
     */
    private JPanel userProcessContainer;
    private EmergencySystem system;
    private EmergencyDirectory emergencyDirectory;
    private String location;
    private Emergency e;
    private boolean networkAlreadyPresent;
    private boolean enterpriseAlreadyPresent;
    private boolean hospitalAlreadyPresent;
    private boolean userNameIsUnique;
    private boolean entUsernameIsUnique;
   
    private UserAccount userAccount;
    public EmergencyManagementSystemWorkArea(JPanel userProcessContainer, EmergencySystem system, UserAccount userAccount)
    {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.system=system;
        this.userAccount=userAccount;
       
        emergencyDirectory=system.getEmergencyDirectory();
        locateBtn.setEnabled(false);
        distTF1.setEditable(false);
        dist2TF.setEditable(false);
        dist3TF.setEditable(false);
        findTheDistBtn.setEnabled(false);
        routeCallBtn.setEnabled(false);
        populateEmergencyLocation();
        ent911AddressTF.setVisible(false);
        ent911AddressLabel.setVisible(false);
        populateNetworkTable();
        populateCombo();
        populateAdminNetworkCombo();
        populateEntTable();
        populateNatureOfEmergencyCombo();
        populateTable();
        populateAdminDetailsTable();
        populateNetCombo();
        populateNetworkJCombo();
        populateTimeReportTable();
        populateTable2();
        locateBtn.setVisible(false);
        locateBtn1.setVisible(false);
        
        JTableHeader tableHeader = enterpriseTable.getTableHeader();
        Font headerFont = new Font("Verdana", Font.PLAIN, 16);
        tableHeader.setFont(headerFont);
        
        
        JTableHeader tableHeader1 = entAdminTable.getTableHeader();
        tableHeader1.setFont(headerFont);
        
        JTableHeader tableHeader2 = addNetworkTable.getTableHeader();
        tableHeader2.setFont(headerFont);
        
        JTableHeader tableHeader3 = timeFrameTable.getTableHeader();
        tableHeader3.setFont(headerFont);
        
        
        JTableHeader tableHeader4 = networkTable.getTableHeader();
        tableHeader4.setFont(headerFont);
        
        
        JTableHeader tableHeader5 = emergencyTable2.getTableHeader();
        tableHeader5.setFont(headerFont);
                
//        JLabel lab = new JLabel();
//        lab.setPreferredSize(new Dimension(100, 30));
//        jTabbedPane1.setTabComponentAt(0, lab);  // tab index, jLabel

//         UIManager.getLookAndFeelDefaults().put("TabbedPane:TabbedPaneTab.contentMargins", new Insets(1500, 15000, 0, 0));

    }
    
    public void  populateTimeReportTable()
    {
    DefaultTableModel model = (DefaultTableModel) timeFrameTable.getModel();

        model.setRowCount(0);

        for(WorkRequest work:userAccount.getWorkQueue().getWorkRequestList())
        {
            Object[] row = new Object[5];
            row[0] = ((Emergency911DepartmentWorkRequest)work).getEmergency();
            row[1] = ((Emergency911DepartmentWorkRequest)work).getEmergency().getTotalTimeToReachDrone();
            row[2] = ((Emergency911DepartmentWorkRequest)work).getEmergency().getTotalTimeForDoctorToGetComnnected();
            row[3] = ((Emergency911DepartmentWorkRequest)work).getEmergency().getTotalTimeToDispatchAmbulance();
            row[4] = ((Emergency911DepartmentWorkRequest)work).getEmergency().getTotatTimeTakenByPoliceToAlertEmergencyContact();
            

             model.addRow(row);
        }
        
       
    }
    
    public void populateEmergencyLocation()
    {
       
        Random rand = new Random(); 
        int index=rand.nextInt(5);
         
         locationEmergencyTF.setText(system.getDirectory().getEmergencyAddressLocationList().get(index).getAddress());
    }
    
    public void populateNetCombo()
    {
         netCombo.removeAllItems();
        
        for (Network network : system.getNetworkList()){
            netCombo.addItem(network);
        }
    }
    
    public void populateNetworkJCombo()
    {
    networkJCombo.removeAllItems();
        
        for (Network network : system.getNetworkList()){
            networkJCombo.addItem(network);
        }
    }
    
 
    
    public void populateAdminDetailsTable()
    {
        DefaultTableModel model = (DefaultTableModel) entAdminTable.getModel();

        model.setRowCount(0);
        for (Network network : system.getNetworkList()) 
        {
            for (Enterprise enterprise : network.getEntDirObj().getEnterpriseList()) 
            {
                for (UserAccount userAccount : enterprise.getUserAccountDirectory().getUserAccountList())
                {
                    Object[] row = new Object[3];
                    row[0] = enterprise.getName();
                    row[1] = network.getNetworkName();
                    row[2] = userAccount.getUsername();

                    model.addRow(row);
                }
            }
        }
    }
    
    public void populateHospitalTable(Network network)
    {
     DefaultTableModel model = (DefaultTableModel) hospitalTable.getModel();
     model.setRowCount(0);
     
     for(Hospital h:network.getHospitalList())
     {
         Object[] row = new Object[2];
         row[0] = h;
         row[1]=network;
         model.addRow(row);
         
     }
    }
    
    public void populateAdminNetworkCombo()
    {
        networkAdminCombo.removeAllItems();
        
        for (Network network : system.getNetworkList()){
            networkAdminCombo.addItem(network);
        }
    }
    
    public void populateEnterpriseAdminComboBox(Network network)
    {
        entAdminCombo.removeAllItems();
        
        for (Enterprise enterprise : network.getEntDirObj().getEnterpriseList()){
            entAdminCombo.addItem(enterprise);
        }
    }
    public void populateNetworkTable()
    {
        DefaultTableModel model = (DefaultTableModel) addNetworkTable.getModel();
        
        model.setRowCount(0);
        
        for (Network network : system.getNetworkList()){
            Object[] row = new Object[1];
            row[0] = network;
            model.addRow(row);
        }
    }
    
    public void populateEntTable()
    {
        DefaultTableModel model = (DefaultTableModel) enterpriseTable.getModel();

        model.setRowCount(0);
        for (Network network : system.getNetworkList()) 
        {
            for (Enterprise enterprise : network.getEntDirObj().getEnterpriseList()) 
            {
                Object[] row = new Object[3];
                row[0] = enterprise.getName();
                row[1] = network.getNetworkName();
                row[2] = enterprise.getEnterpriseType().getValue();

                model.addRow(row);
            }
        }
    }
    
    public void  populateCombo()
    {
         networkCombo.removeAllItems();
        for (Network n:system.getNetworkList())
        {  
                networkCombo.addItem(n);
        }
        entTypeCombo.removeAllItems();
        for (Enterprise.EnterpriseType type : Enterprise.EnterpriseType.values()) {
            if(!type.getValue().equalsIgnoreCase("Hospital Enterprise"))
            entTypeCombo.addItem(type);
        }
  
    }
    public void populateNatureOfEmergencyCombo()
    {
         natureOfEmergencyCombo.removeAllItems();
        for (EmergencyType type : Emergency.EmergencyType.values())
        {
                String value=type.getValue();
                natureOfEmergencyCombo.addItem(value);
        }
  
    }
    
    public void populateDescriptionComboBox(String emrg)
    {
        descriptionCombo.removeAllItems();
        if(emrg.equalsIgnoreCase("Accident Emergency"))
        {
            AccidentEmergency accEmerg=new AccidentEmergency();
            for(Description d:accEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
        else if(emrg.equalsIgnoreCase("Medical Emergency"))
        {
            MedicalEmergency medEmerg=new MedicalEmergency();
          
            for(Description d:medEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Fire Emergency"))
        {
            FireEmergency fireEmerg=new FireEmergency();
            System.err.println("I am going in the fire emer");
            for(Description d:fireEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Tactical Emergency"))
        {
            TacticalEmergency tactEmerg=new TacticalEmergency();
     
            for(Description d:tactEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
    }
    public void populateTable()
    {
        DefaultTableModel model = (DefaultTableModel) networkTable.getModel();
        
        model.setRowCount(0);
        
        for (Network network : system.getNetworkList()){
            Object[] row = new Object[2];
            row[0] = network;
            for(Enterprise e:network.getEntDirObj().getEnterpriseList())
            {
                if(e instanceof Emergency911Enterprise)
                {
                     row[1] = ((Emergency911Enterprise)e).getEmergency911DepartmentName();
                }
            }
           
            model.addRow(row);
        }
    }
    

  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        pickTheCall = new javax.swing.JPanel();
        emergencyLocation = new javax.swing.JLabel();
        phoneNumberOfCaller = new javax.swing.JLabel();
        natureOfEmergency = new javax.swing.JLabel();
        description = new javax.swing.JLabel();
        locationEmergencyTF = new javax.swing.JTextField();
        callersPhoneNumberTF = new javax.swing.JTextField();
        locationErr = new javax.swing.JLabel();
        callerPhoneNumberErr = new javax.swing.JLabel();
        natureOfemergencyErr = new javax.swing.JLabel();
        descriptionErr = new javax.swing.JLabel();
        reportAnEmergencyBtn = new javax.swing.JButton();
        locateBtn = new javax.swing.JButton();
        priorityLabel = new javax.swing.JLabel();
        prioritySlider = new javax.swing.JSlider();
        natureOfEmergencyCombo = new javax.swing.JComboBox();
        descriptionCombo = new javax.swing.JComboBox();
        pickTheCall1 = new javax.swing.JPanel();
        locationErr1 = new javax.swing.JLabel();
        callerPhoneNumberErr1 = new javax.swing.JLabel();
        natureOfemergencyErr1 = new javax.swing.JLabel();
        descriptionErr1 = new javax.swing.JLabel();
        reportAnEmergencyBtn1 = new javax.swing.JButton();
        locateBtn1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        emergencyTable2 = new javax.swing.JTable();
        search911Dept = new javax.swing.JPanel();
        table = new javax.swing.JScrollPane();
        networkTable = new javax.swing.JTable();
        findTheDistBtn = new javax.swing.JButton();
        bostonDitsLabel = new javax.swing.JLabel();
        camDistLabel = new javax.swing.JLabel();
        maldenDistLabel = new javax.swing.JLabel();
        distTF1 = new javax.swing.JTextField();
        dist2TF = new javax.swing.JTextField();
        dist3TF = new javax.swing.JTextField();
        closestPSAPLabel = new javax.swing.JLabel();
        routeCallBtn = new javax.swing.JButton();
        manageEmergencySystem = new javax.swing.JPanel();
        managedEmeregencyPanel = new javax.swing.JTabbedPane();
        createNetworkPanel = new javax.swing.JPanel();
        networkTab = new javax.swing.JScrollPane();
        addNetworkTable = new javax.swing.JTable();
        networkNameLabel = new javax.swing.JLabel();
        networkNameTF = new javax.swing.JTextField();
        addNetworkBtn = new javax.swing.JButton();
        networkNameErr = new javax.swing.JLabel();
        createHospitalHeader1 = new javax.swing.JLabel();
        createHospitalPanel = new javax.swing.JPanel();
        hospitalNameTF = new javax.swing.JTextField();
        hospitalAddressTF = new javax.swing.JTextField();
        specialityTF = new javax.swing.JTextField();
        noOfBedsTF = new javax.swing.JTextField();
        noOfEmptyBedsTF = new javax.swing.JTextField();
        createHospitalHeader = new javax.swing.JLabel();
        addHospitalBtn = new javax.swing.JButton();
        hospitalNameLabel = new javax.swing.JLabel();
        hospitalAddressLabel = new javax.swing.JLabel();
        specialityLabel = new javax.swing.JLabel();
        noOfBedsLabel = new javax.swing.JLabel();
        numberOfEmptyBeds = new javax.swing.JLabel();
        networkLabel = new javax.swing.JLabel();
        netCombo = new javax.swing.JComboBox();
        hospNameErr = new javax.swing.JLabel();
        hospAddressErr = new javax.swing.JLabel();
        specilaityErr = new javax.swing.JLabel();
        noOfBedsErr = new javax.swing.JLabel();
        noOfEmptyBedsErr = new javax.swing.JLabel();
        createEntAdminPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        entAdminTable = new javax.swing.JTable();
        netwrkL = new javax.swing.JLabel();
        enterpriseLabel = new javax.swing.JLabel();
        entAdminUserL = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        username_Admin_TF = new javax.swing.JTextField();
        networkAdminCombo = new javax.swing.JComboBox();
        entAdminCombo = new javax.swing.JComboBox();
        createAdminButton = new javax.swing.JButton();
        passwordAdminTF = new javax.swing.JPasswordField();
        nameAdminErr = new javax.swing.JLabel();
        passwordAdminErr = new javax.swing.JLabel();
        usernameAdminErr = new javax.swing.JLabel();
        name_AdminTF = new javax.swing.JTextField();
        entAdminUserL1 = new javax.swing.JLabel();
        createEnterprisePanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        enterpriseTable = new javax.swing.JTable();
        netLabel = new javax.swing.JLabel();
        entTypeLabel = new javax.swing.JLabel();
        entNameLabel = new javax.swing.JLabel();
        entNameTf = new javax.swing.JTextField();
        createEntBtn = new javax.swing.JButton();
        networkCombo = new javax.swing.JComboBox();
        entTypeCombo = new javax.swing.JComboBox();
        ent911AddressTF = new javax.swing.JTextField();
        ent911AddressLabel = new javax.swing.JLabel();
        entNameErr = new javax.swing.JLabel();
        entAddressErr = new javax.swing.JLabel();
        createHospitalAdminPanel = new javax.swing.JPanel();
        networkJLabel = new javax.swing.JLabel();
        networkJCombo = new javax.swing.JComboBox();
        jScrollPane4 = new javax.swing.JScrollPane();
        hospitalTable = new javax.swing.JTable();
        nameLabel = new javax.swing.JLabel();
        userNameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        hospitalAdminName = new javax.swing.JTextField();
        hospitalAdminUsernameTF = new javax.swing.JTextField();
        hospitalAdminPassword = new javax.swing.JPasswordField();
        createAdminBtn = new javax.swing.JButton();
        nameErr = new javax.swing.JLabel();
        passErr = new javax.swing.JLabel();
        userNaemErr = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        timeFrameTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        header = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 102, 153));
        setPreferredSize(new java.awt.Dimension(809, 853));

        jTabbedPane1.setBackground(new java.awt.Color(255, 153, 51));
        jTabbedPane1.setForeground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        pickTheCall.setBackground(new java.awt.Color(0, 102, 153));
        pickTheCall.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pickTheCall.setForeground(new java.awt.Color(255, 255, 255));
        pickTheCall.setPreferredSize(new java.awt.Dimension(809, 853));

        emergencyLocation.setBackground(new java.awt.Color(0, 51, 153));
        emergencyLocation.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        emergencyLocation.setForeground(new java.awt.Color(255, 255, 255));
        emergencyLocation.setText("Location of request:");

        phoneNumberOfCaller.setBackground(new java.awt.Color(0, 51, 153));
        phoneNumberOfCaller.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        phoneNumberOfCaller.setForeground(new java.awt.Color(255, 255, 255));
        phoneNumberOfCaller.setText("Phone number:");

        natureOfEmergency.setBackground(new java.awt.Color(0, 51, 153));
        natureOfEmergency.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        natureOfEmergency.setForeground(new java.awt.Color(255, 255, 255));
        natureOfEmergency.setText("Nature of the request: ");

        description.setBackground(new java.awt.Color(0, 51, 153));
        description.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        description.setForeground(new java.awt.Color(255, 255, 255));
        description.setText("Description:");

        locationEmergencyTF.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationEmergencyTF.setEnabled(false);
        locationEmergencyTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locationEmergencyTFActionPerformed(evt);
            }
        });

        callerPhoneNumberErr.setFont(new java.awt.Font("Tahoma", 2, 13)); // NOI18N
        callerPhoneNumberErr.setForeground(new java.awt.Color(255, 255, 255));

        reportAnEmergencyBtn.setBackground(new java.awt.Color(255, 255, 255));
        reportAnEmergencyBtn.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        reportAnEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn.setText("Request Drone");
        reportAnEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtnActionPerformed(evt);
            }
        });

        locateBtn.setBackground(new java.awt.Color(255, 153, 51));
        locateBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn.setForeground(new java.awt.Color(255, 255, 255));
        locateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn.setText("Locate the emergency location");
        locateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtnActionPerformed(evt);
            }
        });

        priorityLabel.setBackground(new java.awt.Color(0, 51, 153));
        priorityLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        priorityLabel.setForeground(new java.awt.Color(255, 255, 255));
        priorityLabel.setText("Priority:");

        prioritySlider.setBackground(new java.awt.Color(204, 204, 204));
        prioritySlider.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        prioritySlider.setForeground(new java.awt.Color(255, 255, 255));
        prioritySlider.setMajorTickSpacing(1);
        prioritySlider.setMaximum(10);
        prioritySlider.setMinorTickSpacing(1);
        prioritySlider.setPaintLabels(true);
        prioritySlider.setPaintTicks(true);
        prioritySlider.setValue(0);

        natureOfEmergencyCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        natureOfEmergencyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natureOfEmergencyComboActionPerformed(evt);
            }
        });

        descriptionCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout pickTheCallLayout = new javax.swing.GroupLayout(pickTheCall);
        pickTheCall.setLayout(pickTheCallLayout);
        pickTheCallLayout.setHorizontalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(phoneNumberOfCaller, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emergencyLocation)
                            .addComponent(natureOfEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reportAnEmergencyBtn)
                            .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(descriptionErr)))
                        .addGap(146, 146, 146))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(112, 112, 112)
                                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(92, 92, 92)
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(prioritySlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(descriptionCombo, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(locateBtn)
                                            .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(callersPhoneNumberTF)
                                                .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(natureOfEmergencyCombo, 0, 239, Short.MAX_VALUE)
                                                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pickTheCallLayout.setVerticalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emergencyLocation)
                    .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneNumberOfCaller))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(natureOfEmergency)
                    .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(72, 72, 72)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(priorityLabel)
                            .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(84, 84, 84)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(locateBtn))
                        .addContainerGap(48, Short.MAX_VALUE))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Request Drone On Call ", pickTheCall);

        pickTheCall1.setBackground(new java.awt.Color(0, 102, 153));
        pickTheCall1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pickTheCall1.setPreferredSize(new java.awt.Dimension(809, 853));

        callerPhoneNumberErr1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        callerPhoneNumberErr1.setForeground(new java.awt.Color(102, 102, 102));

        reportAnEmergencyBtn1.setBackground(new java.awt.Color(255, 255, 255));
        reportAnEmergencyBtn1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        reportAnEmergencyBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn1.setText("Process request");
        reportAnEmergencyBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtn1ActionPerformed(evt);
            }
        });

        locateBtn1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn1.setText("Locate the emergency location");
        locateBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtn1ActionPerformed(evt);
            }
        });

        emergencyTable2.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        emergencyTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Phone number", "Location of emergency", "Nature of emergency", "Priority", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(emergencyTable2);

        javax.swing.GroupLayout pickTheCall1Layout = new javax.swing.GroupLayout(pickTheCall1);
        pickTheCall1.setLayout(pickTheCall1Layout);
        pickTheCall1Layout.setHorizontalGroup(
            pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCall1Layout.createSequentialGroup()
                .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 932, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(locationErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(callerPhoneNumberErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(descriptionErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pickTheCall1Layout.createSequentialGroup()
                        .addGap(428, 428, 428)
                        .addComponent(locateBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(natureOfemergencyErr1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(pickTheCall1Layout.createSequentialGroup()
                .addGap(344, 344, 344)
                .addComponent(reportAnEmergencyBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pickTheCall1Layout.setVerticalGroup(
            pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCall1Layout.createSequentialGroup()
                .addGroup(pickTheCall1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(locationErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(callerPhoneNumberErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(natureOfemergencyErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pickTheCall1Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(reportAnEmergencyBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(locateBtn1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)))
                .addComponent(descriptionErr1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(132, Short.MAX_VALUE))
        );

        pickTheCall1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {locateBtn1, reportAnEmergencyBtn1});

        jTabbedPane1.addTab("Work Queue", pickTheCall1);

        search911Dept.setBackground(new java.awt.Color(0, 102, 153));
        search911Dept.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        networkTable.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        networkTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Network Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.setViewportView(networkTable);

        findTheDistBtn.setBackground(new java.awt.Color(255, 255, 255));
        findTheDistBtn.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        findTheDistBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767706_search.png"))); // NOI18N
        findTheDistBtn.setText("Find the distance");
        findTheDistBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findTheDistBtnActionPerformed(evt);
            }
        });

        bostonDitsLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bostonDitsLabel.setForeground(new java.awt.Color(255, 255, 255));
        bostonDitsLabel.setText("Distance of location from Boston Dispatch Center:");

        camDistLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        camDistLabel.setForeground(new java.awt.Color(255, 255, 255));
        camDistLabel.setText("Distance of location from Cambridge Dispatch Center:");

        maldenDistLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        maldenDistLabel.setForeground(new java.awt.Color(255, 255, 255));
        maldenDistLabel.setText("Distance of location from Malden Dispatch Center:");

        distTF1.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N

        dist2TF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N

        closestPSAPLabel.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N

        routeCallBtn.setBackground(new java.awt.Color(255, 255, 255));
        routeCallBtn.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        routeCallBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767932_129_ArrowRight.png"))); // NOI18N
        routeCallBtn.setText("Route Request to DC");
        routeCallBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                routeCallBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout search911DeptLayout = new javax.swing.GroupLayout(search911Dept);
        search911Dept.setLayout(search911DeptLayout);
        search911DeptLayout.setHorizontalGroup(
            search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(search911DeptLayout.createSequentialGroup()
                .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(search911DeptLayout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(camDistLabel)
                            .addComponent(bostonDitsLabel)
                            .addComponent(maldenDistLabel))
                        .addGap(34, 34, 34)
                        .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(distTF1)
                            .addComponent(dist2TF)
                            .addComponent(dist3TF)))
                    .addGroup(search911DeptLayout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(table, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(search911DeptLayout.createSequentialGroup()
                            .addGap(219, 219, 219)
                            .addComponent(findTheDistBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(42, 42, 42)
                            .addComponent(routeCallBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, search911DeptLayout.createSequentialGroup()
                            .addGap(85, 85, 85)
                            .addComponent(closestPSAPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 541, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(295, Short.MAX_VALUE))
        );
        search911DeptLayout.setVerticalGroup(
            search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(search911DeptLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(table, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(findTheDistBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(routeCallBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(closestPSAPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bostonDitsLabel)
                    .addComponent(distTF1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dist2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(camDistLabel))
                .addGap(35, 35, 35)
                .addGroup(search911DeptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(maldenDistLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dist3TF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(164, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Search Nearest Dispatch Center", search911Dept);

        manageEmergencySystem.setBackground(new java.awt.Color(0, 102, 153));
        manageEmergencySystem.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        manageEmergencySystem.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N

        managedEmeregencyPanel.setBackground(new java.awt.Color(0, 102, 153));
        managedEmeregencyPanel.setForeground(new java.awt.Color(255, 255, 255));
        managedEmeregencyPanel.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        managedEmeregencyPanel.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N

        createNetworkPanel.setBackground(new java.awt.Color(0, 102, 153));
        createNetworkPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        createNetworkPanel.setForeground(new java.awt.Color(255, 255, 255));

        addNetworkTable.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        addNetworkTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Network name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        networkTab.setViewportView(addNetworkTable);

        networkNameLabel.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        networkNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        networkNameLabel.setText("Enter network name:");

        networkNameTF.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        networkNameTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                networkNameTFFocusGained(evt);
            }
        });

        addNetworkBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addNetworkBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449768059_More.png"))); // NOI18N
        addNetworkBtn.setText("Add network");
        addNetworkBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addNetworkBtnActionPerformed(evt);
            }
        });

        networkNameErr.setBackground(new java.awt.Color(102, 102, 102));
        networkNameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        networkNameErr.setForeground(new java.awt.Color(102, 102, 102));

        createHospitalHeader1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        createHospitalHeader1.setForeground(new java.awt.Color(255, 255, 255));
        createHospitalHeader1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        createHospitalHeader1.setText("          Create Network");

        javax.swing.GroupLayout createNetworkPanelLayout = new javax.swing.GroupLayout(createNetworkPanel);
        createNetworkPanel.setLayout(createNetworkPanelLayout);
        createNetworkPanelLayout.setHorizontalGroup(
            createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createNetworkPanelLayout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(networkTab, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createNetworkPanelLayout.createSequentialGroup()
                        .addComponent(networkNameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addNetworkBtn)
                            .addComponent(networkNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(128, 128, 128)
                .addComponent(networkNameErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(139, 139, 139))
            .addGroup(createNetworkPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(createHospitalHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        createNetworkPanelLayout.setVerticalGroup(
            createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createNetworkPanelLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(createHospitalHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createNetworkPanelLayout.createSequentialGroup()
                        .addGap(118, 118, 118)
                        .addComponent(networkNameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(createNetworkPanelLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(networkTab, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(69, 69, 69)
                        .addGroup(createNetworkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(networkNameLabel)
                            .addComponent(networkNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(addNetworkBtn)
                .addContainerGap(293, Short.MAX_VALUE))
        );

        managedEmeregencyPanel.addTab("Create networks", createNetworkPanel);

        createHospitalPanel.setBackground(new java.awt.Color(0, 102, 153));
        createHospitalPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        hospitalNameTF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        hospitalNameTF.setEnabled(false);
        hospitalNameTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                hospitalNameTFFocusGained(evt);
            }
        });

        hospitalAddressTF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        hospitalAddressTF.setEnabled(false);
        hospitalAddressTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                hospitalAddressTFFocusGained(evt);
            }
        });

        specialityTF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        specialityTF.setEnabled(false);
        specialityTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                specialityTFFocusGained(evt);
            }
        });

        noOfBedsTF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        noOfBedsTF.setEnabled(false);
        noOfBedsTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                noOfBedsTFFocusGained(evt);
            }
        });
        noOfBedsTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noOfBedsTFActionPerformed(evt);
            }
        });

        noOfEmptyBedsTF.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        noOfEmptyBedsTF.setEnabled(false);
        noOfEmptyBedsTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                noOfEmptyBedsTFFocusGained(evt);
            }
        });

        createHospitalHeader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        createHospitalHeader.setForeground(new java.awt.Color(255, 255, 255));
        createHospitalHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        createHospitalHeader.setText("               Create Hospitals");

        addHospitalBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addHospitalBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449768059_More.png"))); // NOI18N
        addHospitalBtn.setText("Add hospital");
        addHospitalBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addHospitalBtnActionPerformed(evt);
            }
        });

        hospitalNameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        hospitalNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        hospitalNameLabel.setText("Hospital Name:");

        hospitalAddressLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        hospitalAddressLabel.setForeground(new java.awt.Color(255, 255, 255));
        hospitalAddressLabel.setText("Hospital Address:");

        specialityLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        specialityLabel.setForeground(new java.awt.Color(255, 255, 255));
        specialityLabel.setText("Speciality:");

        noOfBedsLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        noOfBedsLabel.setForeground(new java.awt.Color(255, 255, 255));
        noOfBedsLabel.setText("Number of beds:");

        numberOfEmptyBeds.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        numberOfEmptyBeds.setForeground(new java.awt.Color(255, 255, 255));
        numberOfEmptyBeds.setText("Number of empty beds:");

        networkLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        networkLabel.setForeground(new java.awt.Color(255, 255, 255));
        networkLabel.setText("Network:");

        netCombo.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        netCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        netCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                netComboActionPerformed(evt);
            }
        });

        hospNameErr.setBackground(new java.awt.Color(102, 102, 102));
        hospNameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        hospNameErr.setForeground(new java.awt.Color(102, 102, 102));

        hospAddressErr.setBackground(new java.awt.Color(102, 102, 102));
        hospAddressErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        hospAddressErr.setForeground(new java.awt.Color(102, 102, 102));

        specilaityErr.setBackground(new java.awt.Color(102, 102, 102));
        specilaityErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        specilaityErr.setForeground(new java.awt.Color(102, 102, 102));

        noOfBedsErr.setBackground(new java.awt.Color(102, 102, 102));
        noOfBedsErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        noOfBedsErr.setForeground(new java.awt.Color(102, 102, 102));

        noOfEmptyBedsErr.setBackground(new java.awt.Color(102, 102, 102));
        noOfEmptyBedsErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        noOfEmptyBedsErr.setForeground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout createHospitalPanelLayout = new javax.swing.GroupLayout(createHospitalPanel);
        createHospitalPanel.setLayout(createHospitalPanelLayout);
        createHospitalPanelLayout.setHorizontalGroup(
            createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createHospitalPanelLayout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addHospitalBtn)
                    .addGroup(createHospitalPanelLayout.createSequentialGroup()
                        .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(noOfBedsLabel)
                            .addComponent(numberOfEmptyBeds)
                            .addComponent(specialityLabel)
                            .addComponent(hospitalAddressLabel)
                            .addComponent(hospitalNameLabel)
                            .addComponent(networkLabel))
                        .addGap(51, 51, 51)
                        .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(specilaityErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(specialityTF)
                            .addComponent(hospAddressErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hospitalNameTF)
                            .addComponent(hospitalAddressTF)
                            .addComponent(hospNameErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(netCombo, 0, 212, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createHospitalPanelLayout.createSequentialGroup()
                                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(noOfEmptyBedsTF)
                                    .addComponent(noOfBedsErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(noOfEmptyBedsErr, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
                            .addComponent(noOfBedsTF))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(createHospitalPanelLayout.createSequentialGroup()
                .addComponent(createHospitalHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 351, Short.MAX_VALUE))
        );

        createHospitalPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {hospitalAddressTF, hospitalNameTF, netCombo, noOfBedsTF, noOfEmptyBedsTF, specialityTF});

        createHospitalPanelLayout.setVerticalGroup(
            createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createHospitalPanelLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(createHospitalHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(netCombo)
                    .addComponent(networkLabel))
                .addGap(37, 37, 37)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hospitalNameLabel)
                    .addComponent(hospitalNameTF))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(hospNameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hospitalAddressLabel)
                    .addComponent(hospitalAddressTF))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hospAddressErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(specialityTF)
                    .addComponent(specialityLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(specilaityErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(noOfBedsLabel)
                    .addComponent(noOfBedsTF))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(noOfBedsErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createHospitalPanelLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(noOfEmptyBedsErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(createHospitalPanelLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(createHospitalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(numberOfEmptyBeds)
                            .addComponent(noOfEmptyBedsTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(76, 76, 76)
                .addComponent(addHospitalBtn)
                .addGap(128, 128, 128))
        );

        managedEmeregencyPanel.addTab("Create Hospitals", createHospitalPanel);

        createEntAdminPanel.setBackground(new java.awt.Color(0, 102, 153));
        createEntAdminPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        createEntAdminPanel.setToolTipText("");
        createEntAdminPanel.setPreferredSize(new java.awt.Dimension(809, 853));

        entAdminTable.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        entAdminTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Enterprise name", "Network", "Username"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(entAdminTable);

        netwrkL.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        netwrkL.setForeground(new java.awt.Color(255, 255, 255));
        netwrkL.setText("Network:");
        netwrkL.setToolTipText("");

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        enterpriseLabel.setForeground(new java.awt.Color(255, 255, 255));
        enterpriseLabel.setText("Enterprise:");
        enterpriseLabel.setToolTipText("");

        entAdminUserL.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        entAdminUserL.setForeground(new java.awt.Color(255, 255, 255));
        entAdminUserL.setText("UserName:");
        entAdminUserL.setToolTipText("");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Password:");
        jLabel14.setToolTipText("");

        username_Admin_TF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                username_Admin_TFFocusGained(evt);
            }
        });

        networkAdminCombo.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        networkAdminCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        networkAdminCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                networkAdminComboActionPerformed(evt);
            }
        });

        entAdminCombo.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        entAdminCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        createAdminButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        createAdminButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449791895_user_male2.png"))); // NOI18N
        createAdminButton.setText("Create admin");
        createAdminButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createAdminButtonActionPerformed(evt);
            }
        });

        passwordAdminTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                passwordAdminTFFocusGained(evt);
            }
        });

        nameAdminErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        nameAdminErr.setForeground(new java.awt.Color(102, 102, 102));

        passwordAdminErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        passwordAdminErr.setForeground(new java.awt.Color(102, 102, 102));

        usernameAdminErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        usernameAdminErr.setForeground(new java.awt.Color(102, 102, 102));

        name_AdminTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                name_AdminTFFocusGained(evt);
            }
        });

        entAdminUserL1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        entAdminUserL1.setForeground(new java.awt.Color(255, 255, 255));
        entAdminUserL1.setText("Admin Name:");
        entAdminUserL1.setToolTipText("");

        javax.swing.GroupLayout createEntAdminPanelLayout = new javax.swing.GroupLayout(createEntAdminPanel);
        createEntAdminPanel.setLayout(createEntAdminPanelLayout);
        createEntAdminPanelLayout.setHorizontalGroup(
            createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jScrollPane2))
                    .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                                .addGap(144, 144, 144)
                                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(entAdminUserL)
                                            .addComponent(jLabel14))
                                        .addGap(67, 67, 67)
                                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(passwordAdminTF, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(username_Admin_TF, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(name_AdminTF, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(entAdminCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(networkAdminCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(createAdminButton)))
                            .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                                .addGap(139, 139, 139)
                                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(entAdminUserL1)
                                            .addComponent(enterpriseLabel))
                                        .addGap(238, 238, 238)
                                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(passwordAdminErr, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(nameAdminErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(usernameAdminErr, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(netwrkL))))
                        .addGap(0, 353, Short.MAX_VALUE)))
                .addContainerGap())
        );

        createEntAdminPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {entAdminUserL, entAdminUserL1, enterpriseLabel, jLabel14, netwrkL});

        createEntAdminPanelLayout.setVerticalGroup(
            createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(nameAdminErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(netwrkL)
                    .addComponent(networkAdminCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(usernameAdminErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createEntAdminPanelLayout.createSequentialGroup()
                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(enterpriseLabel)
                            .addComponent(entAdminCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(entAdminUserL1)
                            .addComponent(name_AdminTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(passwordAdminErr, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(entAdminUserL)
                    .addComponent(username_Admin_TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addGroup(createEntAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordAdminTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(createAdminButton)
                .addGap(60, 60, 60))
        );

        createEntAdminPanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {entAdminUserL, entAdminUserL1, enterpriseLabel, jLabel14, netwrkL});

        managedEmeregencyPanel.addTab("Create enterprise admin", createEntAdminPanel);

        createEnterprisePanel.setBackground(new java.awt.Color(0, 102, 153));
        createEnterprisePanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        enterpriseTable.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        enterpriseTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Enterprise Name", "Network", "Type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(enterpriseTable);

        netLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        netLabel.setForeground(new java.awt.Color(255, 255, 255));
        netLabel.setText("Network:");

        entTypeLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        entTypeLabel.setForeground(new java.awt.Color(255, 255, 255));
        entTypeLabel.setText("Enterprise type:");

        entNameLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        entNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        entNameLabel.setText("Enterprise Name:");

        entNameTf.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                entNameTfFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                entNameTfFocusLost(evt);
            }
        });

        createEntBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        createEntBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449768059_More.png"))); // NOI18N
        createEntBtn.setText("Create Enterprise");
        createEntBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createEntBtnActionPerformed(evt);
            }
        });

        networkCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        entTypeCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        entTypeCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entTypeComboActionPerformed(evt);
            }
        });

        ent911AddressTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ent911AddressTFFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                ent911AddressTFFocusLost(evt);
            }
        });
        ent911AddressTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ent911AddressTFActionPerformed(evt);
            }
        });

        ent911AddressLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        ent911AddressLabel.setForeground(new java.awt.Color(255, 255, 255));
        ent911AddressLabel.setText("911 Enterprise Address:");

        entNameErr.setBackground(new java.awt.Color(102, 102, 102));
        entNameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        entNameErr.setForeground(new java.awt.Color(102, 102, 102));

        entAddressErr.setBackground(new java.awt.Color(102, 102, 102));
        entAddressErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        entAddressErr.setForeground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout createEnterprisePanelLayout = new javax.swing.GroupLayout(createEnterprisePanel);
        createEnterprisePanel.setLayout(createEnterprisePanelLayout);
        createEnterprisePanelLayout.setHorizontalGroup(
            createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                        .addGap(0, 399, Short.MAX_VALUE)
                        .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(entAddressErr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                                    .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(ent911AddressLabel)
                                        .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(createEntBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                                                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(entTypeLabel)
                                                    .addComponent(netLabel)
                                                    .addComponent(entNameLabel))
                                                .addGap(80, 80, 80)
                                                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(entNameTf, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(entTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(networkCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(ent911AddressTF, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                                    .addGap(363, 363, 363)
                                    .addComponent(entNameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        createEnterprisePanelLayout.setVerticalGroup(
            createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createEnterprisePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(98, 98, 98)
                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(netLabel)
                    .addComponent(networkCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(entTypeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entTypeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(entNameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(entNameLabel)
                    .addComponent(entNameTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(createEnterprisePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ent911AddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ent911AddressTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(createEntBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(entAddressErr, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        managedEmeregencyPanel.addTab("Create enterprises", createEnterprisePanel);

        createHospitalAdminPanel.setBackground(new java.awt.Color(0, 102, 153));
        createHospitalAdminPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        networkJLabel.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        networkJLabel.setForeground(new java.awt.Color(255, 255, 255));
        networkJLabel.setText("Network:");

        networkJCombo.setFont(new java.awt.Font(".SF NS Text", 2, 15)); // NOI18N
        networkJCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        networkJCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                networkJComboActionPerformed(evt);
            }
        });

        hospitalTable.setFont(new java.awt.Font(".SF NS Text", 0, 14)); // NOI18N
        hospitalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Hospital Name", "Network"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(hospitalTable);
        if (hospitalTable.getColumnModel().getColumnCount() > 0) {
            hospitalTable.getColumnModel().getColumn(0).setMinWidth(300);
            hospitalTable.getColumnModel().getColumn(0).setPreferredWidth(300);
            hospitalTable.getColumnModel().getColumn(0).setMaxWidth(300);
        }

        nameLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel.setText("Admin Name:");

        userNameLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        userNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        userNameLabel.setText("Username:");

        passwordLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        passwordLabel.setForeground(new java.awt.Color(255, 255, 255));
        passwordLabel.setText("Password:");

        hospitalAdminName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                hospitalAdminNameFocusGained(evt);
            }
        });

        hospitalAdminUsernameTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                hospitalAdminUsernameTFFocusGained(evt);
            }
        });
        hospitalAdminUsernameTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hospitalAdminUsernameTFActionPerformed(evt);
            }
        });

        hospitalAdminPassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                hospitalAdminPasswordFocusGained(evt);
            }
        });
        hospitalAdminPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hospitalAdminPasswordActionPerformed(evt);
            }
        });

        createAdminBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        createAdminBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449791895_user_male2.png"))); // NOI18N
        createAdminBtn.setText("Create Admin");
        createAdminBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createAdminBtnActionPerformed(evt);
            }
        });

        nameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        nameErr.setForeground(new java.awt.Color(102, 102, 102));

        passErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        passErr.setForeground(new java.awt.Color(102, 102, 102));

        userNaemErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        userNaemErr.setForeground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout createHospitalAdminPanelLayout = new javax.swing.GroupLayout(createHospitalAdminPanel);
        createHospitalAdminPanel.setLayout(createHospitalAdminPanelLayout);
        createHospitalAdminPanelLayout.setHorizontalGroup(
            createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(userNameLabel)
                                    .addComponent(nameLabel)
                                    .addComponent(passwordLabel))
                                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                                        .addGap(70, 70, 70)
                                        .addComponent(hospitalAdminPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, createHospitalAdminPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(hospitalAdminName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(hospitalAdminUsernameTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(passErr, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                    .addComponent(userNaemErr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                    .addComponent(nameErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                                .addComponent(createAdminBtn)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
                            .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                                .addComponent(networkJLabel)
                                .addGap(30, 30, 30)
                                .addComponent(networkJCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(25, 25, 25))
        );
        createHospitalAdminPanelLayout.setVerticalGroup(
            createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(networkJLabel)
                    .addComponent(networkJCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                        .addComponent(nameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(userNaemErr, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(passErr, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(306, 306, 306))
                    .addGroup(createHospitalAdminPanelLayout.createSequentialGroup()
                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nameLabel)
                            .addComponent(hospitalAdminName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)
                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(userNameLabel)
                            .addComponent(hospitalAdminUsernameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)
                        .addGroup(createHospitalAdminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(passwordLabel)
                            .addComponent(hospitalAdminPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(93, 93, 93)
                        .addComponent(createAdminBtn)
                        .addGap(108, 108, 108))))
        );

        managedEmeregencyPanel.addTab("Create Hospital Admin", createHospitalAdminPanel);

        javax.swing.GroupLayout manageEmergencySystemLayout = new javax.swing.GroupLayout(manageEmergencySystem);
        manageEmergencySystem.setLayout(manageEmergencySystemLayout);
        manageEmergencySystemLayout.setHorizontalGroup(
            manageEmergencySystemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageEmergencySystemLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(managedEmeregencyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1039, Short.MAX_VALUE))
        );
        manageEmergencySystemLayout.setVerticalGroup(
            manageEmergencySystemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(managedEmeregencyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Manage Network", manageEmergencySystem);

        jPanel6.setBackground(new java.awt.Color(0, 102, 153));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel6.setPreferredSize(new java.awt.Dimension(809, 853));

        timeFrameTable.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        timeFrameTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Emergency location", "T1", "T2", "T3", "T4"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Long.class, java.lang.Long.class, java.lang.Long.class, java.lang.Long.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        timeFrameTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane5.setViewportView(timeFrameTable);
        if (timeFrameTable.getColumnModel().getColumnCount() > 0) {
            timeFrameTable.getColumnModel().getColumn(0).setMinWidth(300);
            timeFrameTable.getColumnModel().getColumn(0).setPreferredWidth(300);
            timeFrameTable.getColumnModel().getColumn(0).setMaxWidth(300);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Emergency time tracking");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1045, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 603, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(151, 151, 151))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jLabel1)
                .addGap(94, 94, 94)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(307, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Generate reports", jPanel6);

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));

        header.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        header.setForeground(new java.awt.Color(255, 255, 255));
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Drone Headquarter's Admin");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(header, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 34, Short.MAX_VALUE)
                .addComponent(header))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void createAdminBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createAdminBtnActionPerformed
        // TODO add your handling code here:
        char[] passwordCharArray = hospitalAdminPassword.getPassword();
        String password = String.valueOf(passwordCharArray).trim();
        String username=hospitalAdminUsernameTF.getText().trim();
        int rowSelected=hospitalTable.getSelectedRow();
        if(rowSelected>=0)
        {
            if(hospitalAdminName.getText().trim().matches("[a-zA-Z0-9 ]+")&&(hospitalAdminUsernameTF.getText().trim().matches("[a-zA-Z0-9]+"))&&(password.matches("[a-zA-Z0-9]+")))
            {
                userNameIsUnique=system.checkIfUserNameIsUnique(username, password, system);

                if(userNameIsUnique==true)
                {
                    Hospital h=(Hospital) hospitalTable.getValueAt(rowSelected,0);
                    HospitalEnterpriseAdmin admin=new HospitalEnterpriseAdmin();
                    admin.setName(hospitalAdminName.getText());
                    h.getEmployeeDirectory().createEmployee(admin);

                    h.getUserAccountDirectory().createUserAccount(username,password,admin,new HospitalEnterpriseAdminRole());
                    JOptionPane.showMessageDialog(this,"The hospital admin is successfully created");
                    hospitalAdminName.setText("");
                    hospitalAdminUsernameTF.setText("");
                    hospitalAdminPassword.setText("");

                    Network network=(Network) networkJCombo.getSelectedItem();
                }
                else
                {
                    userNaemErr.setText("Username already exists");
                }
            }

            else
            {
                if(!hospitalAdminName.getText().matches("[a-zA-Z0-9]+"))
                {
                    nameErr.setText("Enter a valid name");
                }

                if(!hospitalAdminUsernameTF.getText().matches("[a-zA-Z0-9]+"))
                {
                    userNaemErr.setText("Enter a valid username");
                }

                if(!password.matches("[a-zA-Z0-9]+"))
                {
                    passErr.setText("Enter a valid password");
                }
            }

        }

        else
        {
            JOptionPane.showMessageDialog(this,"Please choose a hospital from the table");
        }
    }//GEN-LAST:event_createAdminBtnActionPerformed

    private void hospitalAdminPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hospitalAdminPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hospitalAdminPasswordActionPerformed

    private void hospitalAdminPasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hospitalAdminPasswordFocusGained
        // TODO add your handling code here:
        passErr.setText("");
    }//GEN-LAST:event_hospitalAdminPasswordFocusGained

    private void hospitalAdminUsernameTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hospitalAdminUsernameTFFocusGained
        // TODO add your handling code here:
        userNaemErr.setText("");
    }//GEN-LAST:event_hospitalAdminUsernameTFFocusGained

    private void hospitalAdminNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hospitalAdminNameFocusGained
        // TODO add your handling code here:
        nameErr.setText("");
    }//GEN-LAST:event_hospitalAdminNameFocusGained

    private void networkJComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_networkJComboActionPerformed
        // TODO add your handling code here:
        Network network=(Network) networkJCombo.getSelectedItem();
        if(network!=null)
        {
            populateHospitalTable(network);
        }
    }//GEN-LAST:event_networkJComboActionPerformed

    private void passwordAdminTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordAdminTFFocusGained
        // TODO add your handling code here:
        passwordAdminErr.setText("");
    }//GEN-LAST:event_passwordAdminTFFocusGained

    private void createAdminButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createAdminButtonActionPerformed
        // TODO add your handling code here:
        entUsernameIsUnique=false;
        String username = username_Admin_TF.getText().trim();
        char[] passwordCharArray = passwordAdminTF.getPassword();
        String password = String.valueOf(passwordCharArray).trim();
        if(name_AdminTF.getText().trim().matches("[a-zA-Z0-9 ]+")&&username_Admin_TF.getText().trim().matches("[a-zA-Z0-9]+")&&(password.matches("[a-zA-Z0-9]+")))
        {
            entUsernameIsUnique=system.checkIfUserNameIsUnique(username, password, system);
            if(entUsernameIsUnique==true)
            {
                Enterprise enterprise = (Enterprise) entAdminCombo.getSelectedItem();

                String name = name_AdminTF.getText().trim();
                
                EnterpriseAdmin admin=new EnterpriseAdmin();
                admin.setName(name);
                enterprise.getEmployeeDirectory().createEmployee(admin);
                enterprise.getUserAccountDirectory().createUserAccount(username, password, admin, new Emergency911EnterpriseAdminRole());
                JOptionPane.showMessageDialog(this,"The enterprise admin is successfully created");

                // UserAccount account = enterprise.getUserAccountDirectory().createUserAccount(username, password, employee, new AdminRole());
                populateAdminDetailsTable();
                name_AdminTF.setText("");
                username_Admin_TF.setText("");
                passwordAdminTF.setText("");

                Network network=(Network) networkJCombo.getSelectedItem();
            }
            else
            {
                usernameAdminErr.setText("Username already exists");
            }

        }

        else
        {
            if(!username_Admin_TF.getText().trim().matches("[a-zA-Z0-9]+"))
            {
                usernameAdminErr.setText("Enter a valid username");
            }
            if(!password.matches("[a-zA-Z0-9]+"))
            {
                passwordAdminErr.setText("Enter a valid password");

            }

        }
    }//GEN-LAST:event_createAdminButtonActionPerformed

    private void networkAdminComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_networkAdminComboActionPerformed
        // TODO add your handling code here:
        Network network = (Network) networkAdminCombo.getSelectedItem();
        if (network != null){
            populateEnterpriseAdminComboBox(network);

        }
    }//GEN-LAST:event_networkAdminComboActionPerformed

    private void username_Admin_TFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_username_Admin_TFFocusGained
        // TODO add your handling code here:
        usernameAdminErr.setText("");
    }//GEN-LAST:event_username_Admin_TFFocusGained

    private void netComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_netComboActionPerformed
        // TODO add your handling code here:
        Network network=(Network) netCombo.getSelectedItem();
        if(network!=null)
        {
            hospitalNameTF.setEnabled(true);
            hospitalAddressTF.setEnabled(true);
            specialityTF.setEnabled(true);
            noOfBedsTF.setEnabled(true);
            noOfEmptyBedsTF.setEnabled(true);

        }
    }//GEN-LAST:event_netComboActionPerformed

    private void addHospitalBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addHospitalBtnActionPerformed
        // TODO add your handling code here:

        if(hospitalNameTF.getText().trim().matches("[a-zA-Z0-9 ]+")&&(!hospitalAddressTF.getText().trim().equals(""))&&(specialityTF.getText().trim().matches("[a-zA-Z0-9 ]+"))&&(noOfBedsTF.getText().trim().matches("[0-9]+"))&&(noOfEmptyBedsTF.getText().trim().matches("[0-9]+")))
        {
            for(Network n:system.getNetworkList())
            {
                for(Hospital h:n.getHospitalList())
                {
                    if(h.getHospitalName().equalsIgnoreCase(hospitalNameTF.getText()))
                    {
                        hospitalAlreadyPresent=true;
                        break;
                    }
                }

                if(hospitalAlreadyPresent==true)
                break;

            }

            if(hospitalAlreadyPresent==true)
            {
                hospNameErr.setText("The hospital with same name exists");
            }

            else
            {

                Network network=(Network) netCombo.getSelectedItem();
                if(network!=null)
                {
                    Hospital h=network.addHospital(hospitalNameTF.getText());
                    h.setHospitalName(hospitalNameTF.getText().trim());
                    h.setHospitalLocation(hospitalAddressTF.getText().trim());
                    h.setSpeciality(specialityTF.getText().trim());
                    h.setNumberOfBeds(Integer.parseInt(noOfBedsTF.getText().trim()));
                    h.setNumberOfEmptyBeds(Integer.parseInt(noOfEmptyBedsTF.getText().trim()));
                    JOptionPane.showMessageDialog(this,"The hospital is successfully added to the network");
                    hospitalNameTF.setText("");
                    hospitalAddressTF.setText("");
                    specialityTF.setText("");
                    noOfBedsTF.setText("");
                    noOfEmptyBedsTF.setText("");

                }

            }

        }

        else
        {
            if(!hospitalNameTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
            {
                hospNameErr.setText("Enter a valid hospital name");
            }

            if(hospitalAddressTF.getText().trim().equals(""))
            {
                hospAddressErr.setText("Enter a valid hospital address");

            }

            if(hospitalAddressTF.getText().trim().equals(""))
            {
                hospAddressErr.setText("Enter a valid hospital address");
            }

            if(!specialityTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
            {
                specilaityErr.setText("Enter a valid speciality");
            }

            if(!noOfBedsTF.getText().trim().matches("[0-9]+"))
            {
                noOfBedsErr.setText("Enter valid no of beds");
            }

            if(!noOfEmptyBedsTF.getText().trim().matches("[0-9]+"))
            {
                noOfEmptyBedsErr.setText("Enter valid no of empty beds");
            }
        }
    }//GEN-LAST:event_addHospitalBtnActionPerformed

    private void noOfEmptyBedsTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_noOfEmptyBedsTFFocusGained
        // TODO add your handling code here:
        noOfEmptyBedsErr.setText("");
    }//GEN-LAST:event_noOfEmptyBedsTFFocusGained

    private void noOfBedsTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noOfBedsTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_noOfBedsTFActionPerformed

    private void noOfBedsTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_noOfBedsTFFocusGained
        // TODO add your handling code here:
        noOfBedsErr.setText("");
    }//GEN-LAST:event_noOfBedsTFFocusGained

    private void specialityTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_specialityTFFocusGained
        // TODO add your handling code here:
        specilaityErr.setText("");
    }//GEN-LAST:event_specialityTFFocusGained

    private void hospitalAddressTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hospitalAddressTFFocusGained
        // TODO add your handling code here:
        hospAddressErr.setText("");
    }//GEN-LAST:event_hospitalAddressTFFocusGained

    private void hospitalNameTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_hospitalNameTFFocusGained
        // TODO add your handling code here:
        hospNameErr.setText("");
    }//GEN-LAST:event_hospitalNameTFFocusGained

    private void addNetworkBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addNetworkBtnActionPerformed
        // TODO add your handling code here:
        networkAlreadyPresent=false;
        if(networkNameTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
        {

            for(Network net:system.getNetworkList())
            {
                if(net.getNetworkName().equalsIgnoreCase(networkNameTF.getText()))
                {
                    networkAlreadyPresent=true;
                    break;
                }

            }

            if(networkAlreadyPresent==false)
            {
                if(networkNameErr.getText().equals(""))
                {
                    Network n=system.addNetwork();
                    n.setNetworkName(networkNameTF.getText().trim());
                    JOptionPane.showMessageDialog(this,"The network is successfully created");
                    populateNetworkTable();
                    networkNameTF.setText("");
                    populateCombo();
                }
            }

            else
            {
                networkNameErr.setText("This network already exists");
            }

        }

        else
        {
            networkNameErr.setText("Enter the network name");
        }
    }//GEN-LAST:event_addNetworkBtnActionPerformed

    private void networkNameTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_networkNameTFFocusGained
        // TODO add your handling code here:
        networkNameErr.setText("");
    }//GEN-LAST:event_networkNameTFFocusGained

    private void routeCallBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_routeCallBtnActionPerformed
        // TODO add your handling code here:
        int rowNumber=networkTable.getSelectedRow();
        if(rowNumber>=0)
        {

            Network n= (Network) networkTable.getValueAt(rowNumber,0);
            for(Enterprise ent:n.getEntDirObj().getEnterpriseList())
            {
                if(ent instanceof Emergency911Enterprise)
                {
                    UserAccount userAcc=ent.getUserAccountDirectory().getUserAccountList().get(0);
                    Emergency911DepartmentWorkRequest request=new Emergency911DepartmentWorkRequest();
                    request.setEmployee(ent.getEmployeeDirectory().getEmployeeList().get(0));
                    request.setEmergency(e);
                    e.setEmergencyStatus("Assigned to Dispatch Center");
                    request.setReceiver(userAcc);
                    request.setSender(system.getUserAccountDirectory().getUserAccountList().get(0));
                    userAcc.getWorkQueue().getWorkRequestList().add(request);
                    routeCallBtn.setEnabled(false);

                    request.setEmployee(userAccount.getEmployee());
                    request.setEmergency(e);
                    request.setReceiver(userAccount);
                    request.setSender(userAccount);
                    userAccount.getWorkQueue().getWorkRequestList().add(request);
                    //  populateTimeReportTable();
                    //                     Date d=new Date();
                    //                     e.setPsapAlerted(d);
                    //
                }
            }

            JOptionPane.showMessageDialog(this,"The request has been routed to the nearest DC");
        }

        else

        {
            JOptionPane.showMessageDialog(null, "Please select a row from the table", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_routeCallBtnActionPerformed

    private void findTheDistBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findTheDistBtnActionPerformed

        // TODO add your handling code here:

        try{
            URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+location+"&destinations=360+Huntington+Ave,+Boston,+MA+02115|1350+Massachusetts+Avenue+Cambridge,+MA+02138|77+Salem+St,+Malden,+MA+02148&key=AIzaSyDKx9h8uc94RWeb1pLFBdDNzKQGHL43oKE");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            String line, outputString = "";
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            //System.out.println("outputString >>>> "+outputString);

            String[] distanceStringArray = new String[4];
            try{
                JSONObject obj = new JSONObject(outputString);
                JSONArray rows = obj.getJSONArray("rows");
                //String dist = obj.getJSONObject("rows").getJSONObject("elements").getString("distance");

                for(int i = 0; i<rows.length();i++){
                    JSONArray e=rows.getJSONObject(i).getJSONArray("elements");

                    for(int j = 0 ; j<e.length();j++){
                        String distance=e.getJSONObject(j).getJSONObject("distance").getString("text");
                        distanceStringArray[j]=distance;

                    }

                }

                distTF1.setText(distanceStringArray[0]);
                dist2TF.setText(distanceStringArray[1]);
                dist3TF.setText(distanceStringArray[2]);

            }catch(Exception e){
                System.out.println("The exception occured >> "+e.getMessage());
            }

            DistancePojo capRes = new Gson().fromJson(outputString, DistancePojo.class);
            //  System.out.println("capRes >> "+capRes);

        }

        catch(Exception e)
        {
            System.err.println("The exception is "+e);
        }

        String distance1 =distTF1.getText();
        String dist1="null";
        String dist2="null";
        String dist3="null";
        int p1 = distance1.indexOf(' ');
        if (p1 >= 0)
        {
            dist1 = distance1.substring(0, p1);
        }

        String distance2 =dist2TF.getText();
        int p2 = distance2.indexOf(' ');
        if (p2 >= 0)
        {
            dist2 = distance2.substring(0, p2);
        }

        String distance3 =dist3TF.getText();
        int p3 = distance3.indexOf(' ');
        if (p3 >= 0)
        {
            dist3 = distance3.substring(0, p3);
        }

        float d1=Float.parseFloat(dist1);
        float d2=Float.parseFloat(dist2);
        float d3=Float.parseFloat(dist3);

        float smallest;
        if(d1<d2 && d1<d3)
        {
            smallest = d1;
            closestPSAPLabel.setText("The closest DC is the Boston DC which is at "+smallest+" km distance from the location");
        }
        else if(d2<d3)
        {
            smallest = d2;
            closestPSAPLabel.setText("The closest DC is the Cambridge DC which is at "+smallest+" km distance from the location");
        }
        else
        {
            smallest = d3;
            closestPSAPLabel.setText("The closest DC is the Malden DC which is at "+smallest+" km distance from the location");
        }
    }//GEN-LAST:event_findTheDistBtnActionPerformed

    private void natureOfEmergencyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natureOfEmergencyComboActionPerformed
        // TODO add your handling code here:
        String emrg = (String) natureOfEmergencyCombo.getSelectedItem();
        if (emrg != null){
            populateDescriptionComboBox(emrg);
        }
    }//GEN-LAST:event_natureOfEmergencyComboActionPerformed

    private void locateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtnActionPerformed

    private void reportAnEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtnActionPerformed
        // TODO add your handling code here:

        if(callersPhoneNumberTF.getText().matches("[0-9]+")&& (callersPhoneNumberTF.getText().length()==10))

        {
            callerPhoneNumberErr.setText("");
            findTheDistBtn.setEnabled(true);
            routeCallBtn.setEnabled(true);
            e=emergencyDirectory.createEmergency();
            e.setLocationOfEmergency(locationEmergencyTF.getText());
            e.setCallersPhoneNumber(callersPhoneNumberTF.getText());
            e.setNatureOfEmergency((String) natureOfEmergencyCombo.getSelectedItem());
            e.setDescription((String) descriptionCombo.getSelectedItem());
            e.setPriority(prioritySlider.getValue());
            e.setEmergencyStatus("Reported");
            Date d = new Date();
            e.setReportedTime(d);
            JOptionPane.showMessageDialog(this,"Drone request has been submitted!!");
            locateBtn.setEnabled(true);
            reportAnEmergencyBtn.setEnabled(false);
            callersPhoneNumberTF.setEditable(false);
            location=e.getLocationOfEmergency().replaceAll("\\s","+");
        }

        else
        {

            if(!callersPhoneNumberTF.getText().matches("[0-9]+"))
            {
                callerPhoneNumberErr.setText("Enter only numeric values");
            }

            else
            {
                callerPhoneNumberErr.setText("Enter a 10 digit numeric value");
            }
        }
        
        callersPhoneNumberTF.setText("");
        callersPhoneNumberTF.setEditable(true);
        reportAnEmergencyBtn.setEnabled(true);
        populateEmergencyLocation();
    }//GEN-LAST:event_reportAnEmergencyBtnActionPerformed

    private void reportAnEmergencyBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtn1ActionPerformed
        // TODO add your handling code here:
            int rowSelected = emergencyTable2.getSelectedRow();
//            System.out.print(rowSelected);
            if (rowSelected>=0){
                e = emergencyDirectory.getEmergencyList().get(rowSelected);
                
                if(e.getEmergencyStatus().equalsIgnoreCase("Completed")){
                    JOptionPane.showMessageDialog(this,"Request is already Completed!"); 
                }
                else{
                    JOptionPane.showMessageDialog(this,"Request has been processed!");         
                    findTheDistBtn.setEnabled(true);
                    routeCallBtn.setEnabled(true);
                }
                     
                findTheDistBtn.setEnabled(true);
                routeCallBtn.setEnabled(true);
            }
            else
            {
                JOptionPane.showMessageDialog(this,"Please select a request");  
            }
    }//GEN-LAST:event_reportAnEmergencyBtn1ActionPerformed

    private void locateBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtn1ActionPerformed
        // TODO add your handling code here:
        try
        {
            //
            //        Browser browser = BrowserFactory.create();
            //        JFrame frame = new JFrame("JxBrowser Google Maps");
            //        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
            //        frame.setSize(700, 500);
            //        frame.setLocationRelativeTo(null);
            //        frame.setVisible(true);
            //        browser.loadURL("http://maps.google.com");
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtn1ActionPerformed

    private void locationEmergencyTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locationEmergencyTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_locationEmergencyTFActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
        populateTable2();
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void name_AdminTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_name_AdminTFFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_name_AdminTFFocusGained

    private void ent911AddressTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ent911AddressTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ent911AddressTFActionPerformed

    private void ent911AddressTFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ent911AddressTFFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_ent911AddressTFFocusLost

    private void ent911AddressTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ent911AddressTFFocusGained
        // TODO add your handling code here:
        entAddressErr.setText("");
    }//GEN-LAST:event_ent911AddressTFFocusGained

    private void entTypeComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entTypeComboActionPerformed
        // TODO add your handling code here:
        Enterprise.EnterpriseType type = (Enterprise.EnterpriseType) entTypeCombo.getSelectedItem();
        if(type==EnterpriseType.EMEREGENCY911ENTERPRISE)
        {
            ent911AddressTF.setVisible(true);
            ent911AddressLabel.setVisible(true);
        }
        else
        {
            ent911AddressTF.setVisible(false);
            ent911AddressLabel.setVisible(false);
        }
    }//GEN-LAST:event_entTypeComboActionPerformed

    private void createEntBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createEntBtnActionPerformed
        // TODO add your handling code here:

        enterpriseAlreadyPresent=false;
        Network network = (Network) networkCombo.getSelectedItem();
        Enterprise.EnterpriseType type = (Enterprise.EnterpriseType) entTypeCombo.getSelectedItem();

        if (network == null || type == null) {
            JOptionPane.showMessageDialog(null, "Kindly choose both the options to proceed further");
            return;
        }

        String name = entNameTf.getText().trim();
        if(type==EnterpriseType.EMEREGENCY911ENTERPRISE)
        {
            if(entNameTf.getText().trim().matches("[a-zA-Z0-9 ]+")&&(!ent911AddressTF.getText().trim().equals("")))
            {
                for(Network n:system.getNetworkList())
                {
                    for(Enterprise ent:n.getEntDirObj().getEnterpriseList())
                    {
                        if( ent.getName().equalsIgnoreCase(entNameTf.getText()))
                        {
                            enterpriseAlreadyPresent=true;
                            break;
                        }
                    }
                    if(enterpriseAlreadyPresent==true)
                    entNameErr.setText("This enterprise already exists");
                    break;
                }

                if(enterpriseAlreadyPresent==false)
                {
                    Enterprise enterprise = network.getEntDirObj().createAndAddEnterprise(name, type,ent911AddressTF.getText().trim());
                    JOptionPane.showMessageDialog(this,"The enterprise has been succesfully added to the network");
                    entNameTf.setText("");
                    ent911AddressTF.setText("");
                    populateEntTable();
                    populateAdminNetworkCombo();
                }

            }

            else
            {
                if(!entNameTf.getText().trim().matches("[a-zA-Z0-9 ]+"))
                {
                    entNameErr.setText("Enter a valid enterprise name");
                }

                if(ent911AddressTF.getText().equals(""))
                {
                    entAddressErr.setText("Enter a valid enterprise address");
                }
            }

        }
        else
        {
            if(entNameTf.getText().trim().matches("[a-zA-Z0-9 ]+"))
            {
                for(Network n:system.getNetworkList())
                {
                    for(Enterprise ent:n.getEntDirObj().getEnterpriseList())
                    {
                        if( ent.getName().equalsIgnoreCase(entNameTf.getText()))
                        {
                            enterpriseAlreadyPresent=true;
                            break;
                        }
                    }
                    if(enterpriseAlreadyPresent==true)
                    entNameErr.setText("This enterprise already exists");
                    break;
                }

                if(enterpriseAlreadyPresent==false)
                {
                    Enterprise enterprise = network.getEntDirObj().createAndAddEnterprise(name, type,ent911AddressTF.getText().trim());
                    JOptionPane.showMessageDialog(this,"The enterprise has been succesfully added to the network");
                    entNameTf.setText("");
                    ent911AddressTF.setText("");
                    populateEntTable();
                    populateAdminNetworkCombo();
                }
            }

            else
            {
                if(!entNameTf.getText().matches("[a-zA-Z0-9]+"))
                {
                    entNameErr.setText("Enter a valid enterprise name");
                }
            }

        }
    }//GEN-LAST:event_createEntBtnActionPerformed

    private void entNameTfFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_entNameTfFocusLost
        // TODO add your handling code here:
        entNameErr.setText("");
    }//GEN-LAST:event_entNameTfFocusLost

    private void entNameTfFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_entNameTfFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_entNameTfFocusGained

    private void hospitalAdminUsernameTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hospitalAdminUsernameTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hospitalAdminUsernameTFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addHospitalBtn;
    private javax.swing.JButton addNetworkBtn;
    private javax.swing.JTable addNetworkTable;
    private javax.swing.JLabel bostonDitsLabel;
    private javax.swing.JLabel callerPhoneNumberErr;
    private javax.swing.JLabel callerPhoneNumberErr1;
    private javax.swing.JTextField callersPhoneNumberTF;
    private javax.swing.JLabel camDistLabel;
    private javax.swing.JLabel closestPSAPLabel;
    private javax.swing.JButton createAdminBtn;
    private javax.swing.JButton createAdminButton;
    private javax.swing.JPanel createEntAdminPanel;
    private javax.swing.JButton createEntBtn;
    private javax.swing.JPanel createEnterprisePanel;
    private javax.swing.JPanel createHospitalAdminPanel;
    private javax.swing.JLabel createHospitalHeader;
    private javax.swing.JLabel createHospitalHeader1;
    private javax.swing.JPanel createHospitalPanel;
    private javax.swing.JPanel createNetworkPanel;
    private javax.swing.JLabel description;
    private javax.swing.JComboBox descriptionCombo;
    private javax.swing.JLabel descriptionErr;
    private javax.swing.JLabel descriptionErr1;
    private javax.swing.JTextField dist2TF;
    private javax.swing.JTextField dist3TF;
    private javax.swing.JTextField distTF1;
    private javax.swing.JLabel emergencyLocation;
    private javax.swing.JTable emergencyTable2;
    private javax.swing.JLabel ent911AddressLabel;
    private javax.swing.JTextField ent911AddressTF;
    private javax.swing.JLabel entAddressErr;
    private javax.swing.JComboBox entAdminCombo;
    private javax.swing.JTable entAdminTable;
    private javax.swing.JLabel entAdminUserL;
    private javax.swing.JLabel entAdminUserL1;
    private javax.swing.JLabel entNameErr;
    private javax.swing.JLabel entNameLabel;
    private javax.swing.JTextField entNameTf;
    private javax.swing.JComboBox entTypeCombo;
    private javax.swing.JLabel entTypeLabel;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JTable enterpriseTable;
    private javax.swing.JButton findTheDistBtn;
    private javax.swing.JLabel header;
    private javax.swing.JLabel hospAddressErr;
    private javax.swing.JLabel hospNameErr;
    private javax.swing.JLabel hospitalAddressLabel;
    private javax.swing.JTextField hospitalAddressTF;
    private javax.swing.JTextField hospitalAdminName;
    private javax.swing.JPasswordField hospitalAdminPassword;
    private javax.swing.JTextField hospitalAdminUsernameTF;
    private javax.swing.JLabel hospitalNameLabel;
    private javax.swing.JTextField hospitalNameTF;
    private javax.swing.JTable hospitalTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton locateBtn;
    private javax.swing.JButton locateBtn1;
    private javax.swing.JTextField locationEmergencyTF;
    private javax.swing.JLabel locationErr;
    private javax.swing.JLabel locationErr1;
    private javax.swing.JLabel maldenDistLabel;
    private javax.swing.JPanel manageEmergencySystem;
    private javax.swing.JTabbedPane managedEmeregencyPanel;
    private javax.swing.JLabel nameAdminErr;
    private javax.swing.JLabel nameErr;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField name_AdminTF;
    private javax.swing.JLabel natureOfEmergency;
    private javax.swing.JComboBox natureOfEmergencyCombo;
    private javax.swing.JLabel natureOfemergencyErr;
    private javax.swing.JLabel natureOfemergencyErr1;
    private javax.swing.JComboBox netCombo;
    private javax.swing.JLabel netLabel;
    private javax.swing.JComboBox networkAdminCombo;
    private javax.swing.JComboBox networkCombo;
    private javax.swing.JComboBox networkJCombo;
    private javax.swing.JLabel networkJLabel;
    private javax.swing.JLabel networkLabel;
    private javax.swing.JLabel networkNameErr;
    private javax.swing.JLabel networkNameLabel;
    private javax.swing.JTextField networkNameTF;
    private javax.swing.JScrollPane networkTab;
    private javax.swing.JTable networkTable;
    private javax.swing.JLabel netwrkL;
    private javax.swing.JLabel noOfBedsErr;
    private javax.swing.JLabel noOfBedsLabel;
    private javax.swing.JTextField noOfBedsTF;
    private javax.swing.JLabel noOfEmptyBedsErr;
    private javax.swing.JTextField noOfEmptyBedsTF;
    private javax.swing.JLabel numberOfEmptyBeds;
    private javax.swing.JLabel passErr;
    private javax.swing.JLabel passwordAdminErr;
    private javax.swing.JPasswordField passwordAdminTF;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JLabel phoneNumberOfCaller;
    private javax.swing.JPanel pickTheCall;
    private javax.swing.JPanel pickTheCall1;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JSlider prioritySlider;
    private javax.swing.JButton reportAnEmergencyBtn;
    private javax.swing.JButton reportAnEmergencyBtn1;
    private javax.swing.JButton routeCallBtn;
    private javax.swing.JPanel search911Dept;
    private javax.swing.JLabel specialityLabel;
    private javax.swing.JTextField specialityTF;
    private javax.swing.JLabel specilaityErr;
    private javax.swing.JScrollPane table;
    private javax.swing.JTable timeFrameTable;
    private javax.swing.JLabel userNaemErr;
    private javax.swing.JLabel userNameLabel;
    private javax.swing.JLabel usernameAdminErr;
    private javax.swing.JTextField username_Admin_TF;
    // End of variables declaration//GEN-END:variables


    private void populateTable2() {
        DefaultTableModel model = (DefaultTableModel) emergencyTable2.getModel();
        
        model.setRowCount(0);
        for(Emergency e :emergencyDirectory.getEmergencyList())
        {
//            System.out.println(e.getCallersPhoneNumber());
            Object[] row = new Object[6];
            row[0]=  e.getName();
            row[1]= e.getCallersPhoneNumber();
            row[3]= e.getNatureOfEmergency();
            row[2]= e.getLocationOfEmergency();
            row[4]= e.getPriority();
            row[5]= e.getEmergencyStatus();
            
            model.addRow(row);
        }
        
    }
}
