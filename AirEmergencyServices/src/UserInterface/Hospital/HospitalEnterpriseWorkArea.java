/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Hospital;

import EmergencySystem.Emergency.AccidentEmergency;
import EmergencySystem.Emergency.Description;
import EmergencySystem.Emergency.Emergency;
import EmergencySystem.Emergency.EmergencyDirectory;
import EmergencySystem.Emergency.FireEmergency;
import EmergencySystem.Emergency.MedicalEmergency;
import EmergencySystem.EmergencySystem;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Network.Network;
import Employee.Ambulance;
import Employee.Doctor;
import Employee.Employee;
import Hospital.Hospital;
import Hospital.Organisation.AmbulanceOrganisation;
import Hospital.Organisation.DoctorOrganization;
import Hospital.Organisation.Organisation;
import Hospital.Organisation.Organisation.Type;
import Hospital.Role.AmbulanceRole;
import Hospital.Role.DoctorRole;
import Hospital.UserAccount.UserAccount;
import Hospital.WorkQueue.Emergency911DepartmentWorkRequest;
import Hospital.WorkQueue.WorkRequest;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Font;
import java.util.Date;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Chinmayi Shaligram
 */
public class HospitalEnterpriseWorkArea extends javax.swing.JPanel {

    /**
     * Creates new form HospitalEnterpriseWorkArea
     */
    private JPanel userProcessContainer;
    private UserAccount account;
    private EmergencySystem system; 
    private Network network;
    private EmergencyDirectory emergencyDirectory;
    private Hospital hospital;
    private Emergency e;
    private String location;
    public HospitalEnterpriseWorkArea(JPanel userProcessContainer, UserAccount account, EmergencySystem system, Network network, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.account=account;
        this.system=system;
        emergencyDirectory=system.getEmergencyDirectory();
        this.network=network;
        
        this.hospital= (Hospital)enterprise;
        alertambulanceBtn.setEnabled(false);
        populateOrganisationCombo();
        populateWorkRequestTable();
        populateAmbulanceTable();
        populateOrganisationTable();
        populateEmergencyLocation();
        populateDoctorTable();
        populateNatureOfEmergencyCombo();
        
       locateBtn.setVisible(false);
       organisationCombo.setVisible(false);
       
       JTableHeader tableHeader = emergencyTable.getTableHeader();
       Font headerFont = new Font("Verdana", Font.PLAIN, 16);
       tableHeader.setFont(headerFont);
       
       JTableHeader tableHeader1 = hosptOrganisationTable.getTableHeader();
       tableHeader1.setFont(headerFont);
       
       JTableHeader tableHeader2 = ambulanceTable.getTableHeader();
       tableHeader2.setFont(headerFont);
        
       JTableHeader tableHeader3 = doctorTable.getTableHeader();
       tableHeader3.setFont(headerFont);
        
    }
    
    public void populateEmergencyLocation()
    {
       
        Random rand = new Random(); 
        int index=rand.nextInt(5);
         
         locationEmergencyTF.setText(system.getDirectory().getEmergencyAddressLocationList().get(index).getAddress());
    }
    
        public void populateNatureOfEmergencyCombo()
    {
         natureOfEmergencyCombo.removeAllItems();
        for (Emergency.EmergencyType type : Emergency.EmergencyType.values())
        {
                String value=type.getValue();
                natureOfEmergencyCombo.addItem(value);
        }
  
    }
    
    public void populateAmbulanceTable()
    {
        DefaultTableModel model = (DefaultTableModel) ambulanceTable.getModel();
        model.setRowCount(0);
        for(Network n:system.getNetworkList())
        {
            for(Hospital h:n.getHospitalList())
            {
               for(UserAccount ua:h.getUserAccountDirectory().getUserAccountList())
               {
                   if(ua==account)
                   {
                       for(Organisation org:h.getOrganizationDirectory().getOrganisationList())
                       {
                           if(org instanceof AmbulanceOrganisation)
                           {
                               for(Employee e:org.getEmployeeDirectory().getEmployeeList())
                               {
                                   if(((Ambulance)e).getAvailability().equalsIgnoreCase("Available"))
                                   {
                                     Object[] row = new Object[2];
                                     row[0]= ((Ambulance)e);
                                     row[1] = ((Ambulance)e).getAvailability();
                                     model.addRow(row);
                                   }
                                     
                               }
                           }
                       }
                   }
               }
            }
        }
        {
           
        }
        
    }
    
    
    
        public void populateDescriptionComboBox(String emrg)
    {
        descriptionCombo.removeAllItems();
        if(emrg.equalsIgnoreCase("Accident Emergency"))
        {
            AccidentEmergency accEmerg=new AccidentEmergency();
            for(Description d:accEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
        else if(emrg.equalsIgnoreCase("Medical Emergency"))
        {
            MedicalEmergency medEmerg=new MedicalEmergency();
          
            for(Description d:medEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Fire Emergency"))
        {
            FireEmergency fireEmerg=new FireEmergency();
            System.err.println("I am going in the fire emer");
            for(Description d:fireEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
    }
    
   public void populateWorkRequestTable()
   {
       DefaultTableModel model = (DefaultTableModel) emergencyTable.getModel();
        
        model.setRowCount(0);
        
        for (WorkRequest workRequest:account.getWorkQueue().getWorkRequestList()){
            Object[] row = new Object[5];
            if(((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getPriority()>=7)
            {
            row[2] = "Critical";
           
            }
            
            else if(((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getPriority()<7 &&((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getPriority()>4)
            {
                row[2]="Moderate";
            }
            
            else
            {
                 row[2]="Trivial";
            }
            row[0]= ((Emergency911DepartmentWorkRequest) workRequest);
            row[1]=((Emergency911DepartmentWorkRequest) workRequest).getEmergency();
            row[3] = ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getNatureOfEmergency();
            row[4]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getEmergencyStatus();
            model.addRow(row);
        }
   }
    
    public Hospital getHospital()
    {
        Hospital hospital=null;
        for(Hospital h:network.getHospitalList())
        {
            for(UserAccount ua:h.getUserAccountDirectory().getUserAccountList())
            {
                if(ua==account)
                {
                    hospital=h;
                }
            }
        }
        return hospital;
    }
    public void populateOrganisationCombo()
      {
           organisationCombo.removeAllItems();
        for (Type type : Organisation.Type.values())
        {
            if( (type.getValue().equals(Type.AMBULANCE.getValue()))|| (type.getValue().equals(Type.DOCTOR.getValue())))
                organisationCombo.addItem(type);
        }
      }
    
    public void populateOrganisationTable()
    {
    DefaultTableModel model = (DefaultTableModel) hosptOrganisationTable.getModel();
        
        model.setRowCount(0);
        
        for (Organisation organisation : hospital.getOrganizationDirectory().getOrganisationList())
        {
            Object[] row = new Object[1];
            row[0] = organisation;

            model.addRow(row);
        }
        if(hosptOrganisationTable.getRowCount()>=2)
        {
            addOrgBtn.setVisible(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jOptionPane1 = new javax.swing.JOptionPane();
        header = new javax.swing.JLabel();
        enterpriseWorkArea = new javax.swing.JTabbedPane();
        requestDrone = new javax.swing.JPanel();
        pickTheCall = new javax.swing.JPanel();
        emergencyLocation = new javax.swing.JLabel();
        natureOfEmergency = new javax.swing.JLabel();
        description = new javax.swing.JLabel();
        locationEmergencyTF = new javax.swing.JTextField();
        locationErr = new javax.swing.JLabel();
        callerPhoneNumberErr = new javax.swing.JLabel();
        natureOfemergencyErr = new javax.swing.JLabel();
        descriptionErr = new javax.swing.JLabel();
        reportAnEmergencyBtn = new javax.swing.JButton();
        locateBtn = new javax.swing.JButton();
        priorityLabel = new javax.swing.JLabel();
        prioritySlider = new javax.swing.JSlider();
        natureOfEmergencyCombo = new javax.swing.JComboBox();
        descriptionCombo = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        callersPhoneNumberTF = new javax.swing.JTextField();
        manageOrgPanel = new javax.swing.JPanel();
        organisationLabel = new javax.swing.JLabel();
        organisationCombo = new javax.swing.JComboBox();
        addOrgBtn = new javax.swing.JButton();
        organisationTable = new javax.swing.JScrollPane();
        hosptOrganisationTable = new javax.swing.JTable();
        nameLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        nameTF = new javax.swing.JTextField();
        usernameTF = new javax.swing.JTextField();
        createEmployee = new javax.swing.JButton();
        passwordTF = new javax.swing.JPasswordField();
        nameErr = new javax.swing.JLabel();
        userNameErr = new javax.swing.JLabel();
        passwordErr = new javax.swing.JLabel();
        availabilityLabel = new javax.swing.JLabel();
        availabilityTF = new javax.swing.JTextField();
        availErr = new javax.swing.JLabel();
        specialityLabel = new javax.swing.JLabel();
        specialityTF = new javax.swing.JTextField();
        specialityErr = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        alertAmbulance = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        ambulanceTable = new javax.swing.JTable();
        alertambulanceBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        doctorTable = new javax.swing.JTable();
        workQueue = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        emergencyTable = new javax.swing.JTable();
        processEmergencyBtn = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        doctor1 = new javax.swing.JLabel();
        hospital1 = new javax.swing.JLabel();
        ambulance = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        Doctor = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();

        setBackground(new java.awt.Color(0, 102, 153));
        setForeground(new java.awt.Color(255, 255, 255));

        header.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        header.setForeground(new java.awt.Color(255, 255, 255));
        header.setText("Hospital Enterprise Admin Work Area");

        enterpriseWorkArea.setBackground(new java.awt.Color(255, 153, 51));
        enterpriseWorkArea.setForeground(new java.awt.Color(255, 255, 255));
        enterpriseWorkArea.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        enterpriseWorkArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                enterpriseWorkAreaMouseClicked(evt);
            }
        });

        requestDrone.setBackground(new java.awt.Color(0, 102, 153));

        pickTheCall.setBackground(new java.awt.Color(0, 102, 153));

        emergencyLocation.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        emergencyLocation.setForeground(new java.awt.Color(255, 255, 255));
        emergencyLocation.setText("Location of the incident:");

        natureOfEmergency.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        natureOfEmergency.setForeground(new java.awt.Color(255, 255, 255));
        natureOfEmergency.setText("Nature of the incident: ");

        description.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        description.setForeground(new java.awt.Color(255, 255, 255));
        description.setText("Description:");

        locationEmergencyTF.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationEmergencyTF.setEnabled(false);
        locationEmergencyTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locationEmergencyTFActionPerformed(evt);
            }
        });

        callerPhoneNumberErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        callerPhoneNumberErr.setForeground(new java.awt.Color(102, 102, 102));

        reportAnEmergencyBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        reportAnEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn.setText("Submit request");
        reportAnEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtnActionPerformed(evt);
            }
        });

        locateBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn.setText("Locate the incident location");
        locateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtnActionPerformed(evt);
            }
        });

        priorityLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        priorityLabel.setForeground(new java.awt.Color(255, 255, 255));
        priorityLabel.setText("Priority:");

        prioritySlider.setMajorTickSpacing(1);
        prioritySlider.setMaximum(10);
        prioritySlider.setMinorTickSpacing(1);
        prioritySlider.setPaintLabels(true);
        prioritySlider.setPaintTicks(true);
        prioritySlider.setValue(0);

        natureOfEmergencyCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Medical emergency ", " ", " " }));
        natureOfEmergencyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natureOfEmergencyComboActionPerformed(evt);
            }
        });

        descriptionCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Heart attack", "Asthama attack" }));
        descriptionCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descriptionComboActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Contact Number:");

        callersPhoneNumberTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                callersPhoneNumberTFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pickTheCallLayout = new javax.swing.GroupLayout(pickTheCall);
        pickTheCall.setLayout(pickTheCallLayout);
        pickTheCallLayout.setHorizontalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(108, 108, 108)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(natureOfEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emergencyLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(locateBtn))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(104, 104, 104))))
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(154, 154, 154)
                .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pickTheCallLayout.setVerticalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emergencyLocation)
                    .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(natureOfEmergency)
                        .addGap(25, 25, 25)))
                .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(105, 105, 105)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(locateBtn))))
                .addGap(671, 671, 671))
        );

        javax.swing.GroupLayout requestDroneLayout = new javax.swing.GroupLayout(requestDrone);
        requestDrone.setLayout(requestDroneLayout);
        requestDroneLayout.setHorizontalGroup(
            requestDroneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, requestDroneLayout.createSequentialGroup()
                .addContainerGap(123, Short.MAX_VALUE)
                .addComponent(pickTheCall, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        requestDroneLayout.setVerticalGroup(
            requestDroneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(requestDroneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pickTheCall, javax.swing.GroupLayout.PREFERRED_SIZE, 1076, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        enterpriseWorkArea.addTab("Request Drone", requestDrone);

        manageOrgPanel.setBackground(new java.awt.Color(0, 102, 153));
        manageOrgPanel.setForeground(new java.awt.Color(255, 255, 255));

        organisationLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        organisationLabel.setForeground(new java.awt.Color(255, 255, 255));
        organisationLabel.setText("Organisation");

        organisationCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organisationCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organisationComboActionPerformed(evt);
            }
        });

        addOrgBtn.setBackground(new java.awt.Color(204, 204, 204));
        addOrgBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addOrgBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Hospital/images/1449768059_More.png"))); // NOI18N
        addOrgBtn.setText("Add Organisation");
        addOrgBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addOrgBtnActionPerformed(evt);
            }
        });

        hosptOrganisationTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Organisation"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        organisationTable.setViewportView(hosptOrganisationTable);

        nameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel.setText("Name:");

        usernameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        usernameLabel.setForeground(new java.awt.Color(255, 255, 255));
        usernameLabel.setText("Username:");

        passwordLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        passwordLabel.setForeground(new java.awt.Color(255, 255, 255));
        passwordLabel.setText("Password:");

        nameTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nameTFFocusGained(evt);
            }
        });

        usernameTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                usernameTFFocusGained(evt);
            }
        });

        createEmployee.setBackground(new java.awt.Color(204, 204, 204));
        createEmployee.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        createEmployee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Hospital/images/1449791895_user_male2.png"))); // NOI18N
        createEmployee.setText("Create employee");
        createEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createEmployeeActionPerformed(evt);
            }
        });

        passwordTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                passwordTFFocusGained(evt);
            }
        });

        nameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        nameErr.setForeground(new java.awt.Color(102, 102, 102));

        userNameErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        userNameErr.setForeground(new java.awt.Color(102, 102, 102));

        passwordErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        passwordErr.setForeground(new java.awt.Color(102, 102, 102));

        availabilityLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        availabilityLabel.setForeground(new java.awt.Color(255, 255, 255));
        availabilityLabel.setText("Availability:");

        availabilityTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                availabilityTFFocusGained(evt);
            }
        });

        availErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        availErr.setForeground(new java.awt.Color(102, 102, 102));

        specialityLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        specialityLabel.setForeground(new java.awt.Color(255, 255, 255));
        specialityLabel.setText("Speciality:");

        specialityTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                specialityTFFocusGained(evt);
            }
        });

        specialityErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        specialityErr.setForeground(new java.awt.Color(102, 102, 102));

        jButton1.setBackground(new java.awt.Color(204, 204, 204));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Select the organisation");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout manageOrgPanelLayout = new javax.swing.GroupLayout(manageOrgPanel);
        manageOrgPanel.setLayout(manageOrgPanelLayout);
        manageOrgPanelLayout.setHorizontalGroup(
            manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, manageOrgPanelLayout.createSequentialGroup()
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(574, 574, 574)
                        .addComponent(nameErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, manageOrgPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(userNameErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(passwordErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(availErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(specialityErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, manageOrgPanelLayout.createSequentialGroup()
                        .addGap(252, 252, 252)
                        .addComponent(organisationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79)
                        .addComponent(organisationCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(183, 183, 183))
            .addGroup(manageOrgPanelLayout.createSequentialGroup()
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameLabel)
                            .addComponent(usernameLabel)
                            .addComponent(passwordLabel))
                        .addGap(55, 55, 55)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(manageOrgPanelLayout.createSequentialGroup()
                                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(usernameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(passwordTF, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(122, 122, 122)
                                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(availabilityLabel)
                                    .addComponent(specialityLabel))
                                .addGap(81, 81, 81)
                                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(specialityTF, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(availabilityTF, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(351, 351, 351)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(manageOrgPanelLayout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(addOrgBtn))))
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(organisationTable, javax.swing.GroupLayout.PREFERRED_SIZE, 684, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(358, 358, 358)
                        .addComponent(createEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(167, Short.MAX_VALUE))
        );
        manageOrgPanelLayout.setVerticalGroup(
            manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageOrgPanelLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(organisationCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(organisationLabel))
                .addGap(18, 18, 18)
                .addComponent(addOrgBtn)
                .addGap(35, 35, 35)
                .addComponent(organisationTable, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(userNameErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(passwordErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(availErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, manageOrgPanelLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nameLabel))
                        .addGap(44, 44, 44)
                        .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(usernameLabel)
                            .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(usernameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(availabilityLabel)
                                .addComponent(availabilityTF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(45, 45, 45))
                    .addGroup(manageOrgPanelLayout.createSequentialGroup()
                        .addGap(108, 108, 108)
                        .addComponent(specialityErr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)))
                .addGroup(manageOrgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(specialityTF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(specialityLabel)
                    .addComponent(passwordTF, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordLabel))
                .addGap(38, 38, 38)
                .addComponent(createEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
        );

        enterpriseWorkArea.addTab("Manage Organisations", manageOrgPanel);

        alertAmbulance.setBackground(new java.awt.Color(0, 102, 153));
        alertAmbulance.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        ambulanceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ambulance Name", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(ambulanceTable);

        alertambulanceBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        alertambulanceBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Hospital/images/1449793544_ambulance.png"))); // NOI18N
        alertambulanceBtn.setText("Alert ambulance");
        alertambulanceBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alertambulanceBtnActionPerformed(evt);
            }
        });

        doctorTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Doctor Name", "Speciality", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(doctorTable);

        javax.swing.GroupLayout alertAmbulanceLayout = new javax.swing.GroupLayout(alertAmbulance);
        alertAmbulance.setLayout(alertAmbulanceLayout);
        alertAmbulanceLayout.setHorizontalGroup(
            alertAmbulanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(alertAmbulanceLayout.createSequentialGroup()
                .addGroup(alertAmbulanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(alertAmbulanceLayout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(alertAmbulanceLayout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(alertAmbulanceLayout.createSequentialGroup()
                        .addGap(358, 358, 358)
                        .addComponent(alertambulanceBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(267, Short.MAX_VALUE))
        );
        alertAmbulanceLayout.setVerticalGroup(
            alertAmbulanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(alertAmbulanceLayout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81)
                .addComponent(alertambulanceBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(223, Short.MAX_VALUE))
        );

        enterpriseWorkArea.addTab("Show Organisations", alertAmbulance);

        workQueue.setBackground(new java.awt.Color(0, 102, 153));
        workQueue.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        emergencyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Location of emergency", "Crticality", "Nature of emergency", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(emergencyTable);
        if (emergencyTable.getColumnModel().getColumnCount() > 0) {
            emergencyTable.getColumnModel().getColumn(1).setMinWidth(250);
            emergencyTable.getColumnModel().getColumn(1).setPreferredWidth(250);
            emergencyTable.getColumnModel().getColumn(1).setMaxWidth(250);
        }

        processEmergencyBtn.setBackground(new java.awt.Color(204, 204, 204));
        processEmergencyBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        processEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Hospital/images/1449792327_process.png"))); // NOI18N
        processEmergencyBtn.setText("Process the emergency");
        processEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processEmergencyBtnActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Images/right-arrow.png"))); // NOI18N

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Images/arrow.png"))); // NOI18N

        doctor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Images/doctor.png"))); // NOI18N

        hospital1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Images/hospital.png"))); // NOI18N

        ambulance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Images/ambulance.png"))); // NOI18N

        jTextField1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(0, 102, 153));
        jTextField1.setText("DOCTOR");
        jTextField1.setBorder(null);

        Doctor.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        Doctor.setForeground(new java.awt.Color(0, 102, 153));
        Doctor.setText("HOSPITAL");
        Doctor.setBorder(null);

        jTextField3.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jTextField3.setForeground(new java.awt.Color(0, 102, 153));
        jTextField3.setText("AMBULANCE");
        jTextField3.setBorder(null);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(115, 115, 115)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(doctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jLabel7)
                        .addGap(101, 101, 101)
                        .addComponent(hospital1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(ambulance)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(196, 196, 196)
                        .addComponent(Doctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hospital1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Doctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(ambulance))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jLabel7))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout workQueueLayout = new javax.swing.GroupLayout(workQueue);
        workQueue.setLayout(workQueueLayout);
        workQueueLayout.setHorizontalGroup(
            workQueueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workQueueLayout.createSequentialGroup()
                .addGroup(workQueueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(workQueueLayout.createSequentialGroup()
                        .addGap(383, 383, 383)
                        .addComponent(processEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(workQueueLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(workQueueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 873, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(101, Short.MAX_VALUE))
        );
        workQueueLayout.setVerticalGroup(
            workQueueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workQueueLayout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(processEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(87, 87, 87)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(139, Short.MAX_VALUE))
        );

        enterpriseWorkArea.addTab("Work queue", workQueue);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(233, 233, 233)
                .addComponent(header)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(enterpriseWorkArea, javax.swing.GroupLayout.PREFERRED_SIZE, 1048, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(enterpriseWorkArea, javax.swing.GroupLayout.PREFERRED_SIZE, 891, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addOrgBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addOrgBtnActionPerformed
        // TODO add your handling code here:
        Type type = (Type) organisationCombo.getSelectedItem();
        hospital.getOrganizationDirectory().createOrganisation(type);
        populateOrganisationTable();
    }//GEN-LAST:event_addOrgBtnActionPerformed

    private void createEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createEmployeeActionPerformed
        // TODO add your handling code here:
        boolean userNameIsUnique;
//        specialityErr.setVisible(false);
//        specialityLabel.setVisible(false);
//        specialityTF.setVisible(false);
        String username = usernameTF.getText().trim();
         char[] passwordCharArray = passwordTF.getPassword();
        String password = String.valueOf(passwordCharArray).trim(); 
        
       
        
        
        
//        if(nameTF.getText().trim().matches("[a-zA-Z0-9 ]+")&&username.matches("[a-zA-Z0-9]+")&&password.matches("[a-zA-Z0-9]+")&&availabilityTF.getText().trim().matches("[a-zA-Z0-9]+"))
//        {
//            userNameIsUnique=system.checkIfUserNameIsUnique(username, password);
//            if(userNameIsUnique==true)
//            {
                int rowNumber=hosptOrganisationTable.getSelectedRow();
                if(rowNumber>=0)
                {
                    Organisation org=(Organisation) hosptOrganisationTable.getValueAt(rowNumber,0);
                    if(org instanceof DoctorOrganization)
                    {
                        
                    
                    if(nameTF.getText().trim().matches("[a-zA-Z0-9 ]+")&&username.matches("[a-zA-Z0-9]+")&&password.matches("[a-zA-Z0-9]+")&&availabilityTF.getText().trim().matches("[a-zA-Z0-9]+")&&specialityTF.getText().trim().matches("[a-zA-Z0-9]+"))
                    {
                   
                    userNameIsUnique=system.checkIfUserNameIsUnique(username, password, system);
                    if(userNameIsUnique==true)
                    {
                    Doctor d=new Doctor();
                    d.setName(nameTF.getText().trim());
                    d.setDoctorsAvailablityStatus(true);
                    d.setDoctorsSpeciality(specialityTF.getText().trim());
                    org.getEmployeeDirectory().createEmployee(d);
                    org.getUserAccountDirectory().createUserAccount(username, password, d,new DoctorRole());  
                    JOptionPane.showMessageDialog(this,"The doctor employee is created successfully");
                    nameTF.setText("");
                    usernameTF.setText("");
                    passwordTF.setText("");
                    availabilityTF.setText("");
                    specialityTF.setText("");
                    }
                    
                    else
                    {
                    userNameErr.setText("This username already exists"); 
                    }
                    }
                    
                    else
                    {
                        if(!nameTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
                           {
                            nameErr.setText("Enter a valid name");
                           }
            
                        if(!username.matches("[a-zA-Z0-9]+"))
                            {
                            userNameErr.setText("Enter a valid username");
                            }
            
                        if(!password.matches("[a-zA-Z0-9]+"))
                            {
                            passwordErr.setText("Enter a valid password");
                        
                            }
                        
                        if(!availabilityTF.getText().trim().matches("[a-zA-Z0-9]+"))
                            {
                            availErr.setText("Enter valid availability");
                            }
                        
                        if(!specialityTF.getText().trim().matches("[a-zA-Z0-9]+"))
                            {
                            specialityErr.setText("Enter a valid speciality");
                            }
                            
                        
                        
                    }
                    
                   
                    }
                     else if(org instanceof AmbulanceOrganisation)
                    {
                        
                        
                        if(nameTF.getText().trim().matches("[a-zA-Z0-9 ]+")&&username.matches("[a-zA-Z0-9]+")&&password.matches("[a-zA-Z0-9]+")&&availabilityTF.getText().trim().matches("[a-zA-Z0-9]+"))
                        {
                         userNameIsUnique=system.checkIfUserNameIsUnique(username, password, system);
                        if(userNameIsUnique==true)
                        {
                        Ambulance a=new Ambulance();
                        a.setName(nameTF.getText().trim());
                        a.setAvailability(availabilityTF.getText().trim());
                        org.getEmployeeDirectory().createEmployee(a);
                        org.getUserAccountDirectory().createUserAccount(username, password, a,new AmbulanceRole());
                        JOptionPane.showMessageDialog(this,"The ambulance employee is created successfully");
                        nameTF.setText("");
                        usernameTF.setText("");
                        passwordTF.setText("");
                        availabilityTF.setText("");
                        
                        }
                        
                        else
                        {
                            userNameErr.setText("This username already exists"); 
                        }
                         
                        }
                        
                        else
                        {
                        if(!nameTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
                           {
                            nameErr.setText("Enter a valid name");
                           }
            
                        if(!username.matches("[a-zA-Z0-9]+"))
                            {
                            userNameErr.setText("Enter a valid username");
                            }
            
                        if(!password.matches("[a-zA-Z0-9]+"))
                            {
                            passwordErr.setText("Enter a valid password");
                        
                            }
                        
                        if(!availabilityTF.getText().trim().matches("[a-zA-Z0-9]+"))
                            {
                            availErr.setText("Enter valid availability");
                            }
                        }
                            
        
                    }
        }
        
        else
        {
            JOptionPane.showMessageDialog(this,"Select an organisation from the table");
        }
          
          //  }
            
//            else
//            {
//               userNameErr.setText("This username already exists"); 
//            }
    //    }
        
//        else
//        {
//            if(!nameTF.getText().trim().matches("[a-zA-Z0-9 ]+"))
//            {
//            nameErr.setText("Enter a valid name");
//            }
//            
//            if(!username.matches("[a-zA-Z0-9]+"))
//            {
//                userNameErr.setText("Enter a valid username");
//            }
//            
//            if(!password.matches("[a-zA-Z0-9]+"))
//            {
//                passwordErr.setText("Enter a valid password");
//                        
//            }
      //  }
        
        
        
        
    }//GEN-LAST:event_createEmployeeActionPerformed

    private void alertambulanceBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alertambulanceBtnActionPerformed
        // TODO add your handling code here:
        int rowSelected=ambulanceTable.getSelectedRow();
        int select=emergencyTable.getSelectedRow();
        WorkRequest request=null;
        if(select>=0)
        {
            request = (WorkRequest) emergencyTable.getValueAt(select, 0);
           
        }
        
        else
        {
        JOptionPane.showMessageDialog(this,"Select an incident from the incident table");
        }
        if(rowSelected>=0)
        {
             Emergency emer=(Emergency) emergencyTable.getValueAt(select, 1);
            Ambulance a=(Ambulance) ambulanceTable.getValueAt(rowSelected,0);
           
                for(Organisation org:hospital.getOrganizationDirectory().getOrganisationList())
                {
                    if(org instanceof AmbulanceOrganisation)
                    {
                        for(UserAccount user:org.getUserAccountDirectory().getUserAccountList())
                        {
                            if((user.getEmployee()==a)&&(!emer.getEmergencyStatus().equalsIgnoreCase("Assigned to ambulance")))
                            {
                          
                                request.setSender(account);
                                request.setReceiver(user);
                                emer.setEmergencyStatus("Assigned to ambulance");
                                user.getWorkQueue().getWorkRequestList().add(request);
                                ((Ambulance)user.getEmployee()).setAvailability("Not available");
                                 JOptionPane.showMessageDialog(this,"The ambulance "+a.getName()+ " has been alerted about the incident");
                                 populateAmbulanceTable();
                                 Date d=new Date();
                                 emer.setAmbulanceDispatched(d);
                                 emer.setTotalTimeToDispatchAmbulance((emer.getAmbulanceDispatched().getTime()-emer.getHospitalAlerted().getTime())/ 1000 % 60);
                                System.err.println("The time from hospital alerted to ambulance dipatched"+emer.getTotalTimeToDispatchAmbulance());
                                 break;
                            }
                            
                            else
                            {
                                if(emer.getEmergencyStatus().equalsIgnoreCase("Assigned to ambulance"))
                                {
                                JOptionPane.showMessageDialog(this, "For this incident the ambulance has already been alerted");
                                break;
                                }
                                
                            }
                        }
                    }
                }
            
        }
        
        else{
        JOptionPane.showMessageDialog(this,"Choose a ambulance from the table");
        }
    }//GEN-LAST:event_alertambulanceBtnActionPerformed

    private void processEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processEmergencyBtnActionPerformed
        // TODO add your handling code here:
        int rowSelected=emergencyTable.getSelectedRow();
        if(rowSelected>=0)
        {
            e=(Emergency) emergencyTable.getValueAt(rowSelected, 1);
            JOptionPane.showMessageDialog(this, "Alert the ambulance");
             alertambulanceBtn.setEnabled(true);
            
        }
        
        else
        {
            JOptionPane.showMessageDialog(this,"Choose the incident from the table");
            
        }
    }//GEN-LAST:event_processEmergencyBtnActionPerformed

    private void nameTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nameTFFocusGained
        // TODO add your handling code here:
        nameErr.setText("");
    }//GEN-LAST:event_nameTFFocusGained

    private void usernameTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usernameTFFocusGained
        // TODO add your handling code here:
        userNameErr.setText("");
    }//GEN-LAST:event_usernameTFFocusGained

    private void passwordTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordTFFocusGained
        // TODO add your handling code here:
        passwordErr.setText("");
    }//GEN-LAST:event_passwordTFFocusGained

    private void organisationComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organisationComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_organisationComboActionPerformed

    private void availabilityTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_availabilityTFFocusGained
        // TODO add your handling code here:
        availErr.setText("");
    }//GEN-LAST:event_availabilityTFFocusGained

    private void specialityTFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_specialityTFFocusGained
        // TODO add your handling code here:
        specialityErr.setText("");
    }//GEN-LAST:event_specialityTFFocusGained

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
        int rowSelected=hosptOrganisationTable.getSelectedRow();
        if(rowSelected>=0)
        {
                    hosptOrganisationTable.addMouseListener(new java.awt.event.MouseAdapter() {
                        @Override
                          public void mouseClicked(java.awt.event.MouseEvent evt) 
                          {
                          int row = hosptOrganisationTable.rowAtPoint(evt.getPoint());
                          int col = hosptOrganisationTable.columnAtPoint(evt.getPoint());
                          if (row >= 0 && col >= 0)
                            {
                               specialityErr.setVisible(true);
                               specialityLabel.setVisible(true);
                               specialityTF.setVisible(true);
                               nameTF.setEditable(true);
                               usernameTF.setEditable(true);
                               passwordTF.setEditable(true);
                               availabilityTF.setEditable(true);
                               specialityTF.setEditable(true);
                               

                            }
                          if(row >= 1 && col >= 0)
                          {
                              specialityErr.setVisible(false);
                              specialityLabel.setVisible(false);
                              specialityTF.setVisible(false);
                              nameTF.setEditable(true);
                               usernameTF.setEditable(true);
                               passwordTF.setEditable(true);
                               availabilityTF.setEditable(true);
                               
                              
                          }
                          }
                    });         
        }
        
        else
        {
            JOptionPane.showMessageDialog(this, "Choose an organisation from the table");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void natureOfEmergencyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natureOfEmergencyComboActionPerformed
        // TODO add your handling code here:
        String emrg = (String) natureOfEmergencyCombo.getSelectedItem();
        if (emrg != null){
            populateDescriptionComboBox(emrg);
        }
    }//GEN-LAST:event_natureOfEmergencyComboActionPerformed

    private void locateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            //
            //        Browser browser = BrowserFactory.create();
            //        JFrame frame = new JFrame("JxBrowser Google Maps");
            //        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
            //        frame.setSize(700, 500);
            //        frame.setLocationRelativeTo(null);
            //        frame.setVisible(true);
            //        browser.loadURL("http://maps.google.com");
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Incident Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtnActionPerformed

    private void reportAnEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtnActionPerformed
        // TODO add your handling code here:

        if(callersPhoneNumberTF.getText().matches("[0-9]+")&& (callersPhoneNumberTF.getText().length()==10))

        {
              callerPhoneNumberErr.setText("");
//            findTheDistBtn.setEnabled(true);
//            routeCallBtn.setEnabled(true);
            e=emergencyDirectory.createEmergency();
            e.setLocationOfEmergency(locationEmergencyTF.getText());
            e.setCallersPhoneNumber(callersPhoneNumberTF.getText());
    
            e.setName(account.getEmployee().getName());
            e.setNatureOfEmergency((String) natureOfEmergencyCombo.getSelectedItem());
            e.setDescription((String) descriptionCombo.getSelectedItem());
            e.setPriority(prioritySlider.getValue());
            e.setEmergencyStatus("Reported");
           
            Date d = new Date();
            e.setReportedTime(d);
            JOptionPane.showMessageDialog(this,"Incident has been reported!");
            locateBtn.setEnabled(true);
            reportAnEmergencyBtn.setEnabled(false);
//            callersPhoneNumberTF.setEditable(false);
            location=e.getLocationOfEmergency().replaceAll("\\s","+");
            System.out.println(emergencyDirectory+"test"+e.getEmergencyStatus()+"TEST");
            }

            else
            {

            if(!callersPhoneNumberTF.getText().matches("[0-9]+"))
            {
                callerPhoneNumberErr.setText("Enter only numeric values");
            }

            else
            {
                callerPhoneNumberErr.setText("Enter a 10 digit numeric value");
            }
            }
    }//GEN-LAST:event_reportAnEmergencyBtnActionPerformed

    private void locationEmergencyTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locationEmergencyTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_locationEmergencyTFActionPerformed

    private void callersPhoneNumberTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_callersPhoneNumberTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_callersPhoneNumberTFActionPerformed

    private void enterpriseWorkAreaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_enterpriseWorkAreaMouseClicked
        // TODO add your handling code here:
        populateAmbulanceTable();
        populateDoctorTable();
        
    }//GEN-LAST:event_enterpriseWorkAreaMouseClicked

    private void descriptionComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descriptionComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_descriptionComboActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Doctor;
    private javax.swing.JButton addOrgBtn;
    private javax.swing.JPanel alertAmbulance;
    private javax.swing.JButton alertambulanceBtn;
    private javax.swing.JLabel ambulance;
    private javax.swing.JTable ambulanceTable;
    private javax.swing.JLabel availErr;
    private javax.swing.JLabel availabilityLabel;
    private javax.swing.JTextField availabilityTF;
    private javax.swing.JLabel callerPhoneNumberErr;
    private javax.swing.JTextField callersPhoneNumberTF;
    private javax.swing.JButton createEmployee;
    private javax.swing.JLabel description;
    private javax.swing.JComboBox descriptionCombo;
    private javax.swing.JLabel descriptionErr;
    private javax.swing.JLabel doctor1;
    private javax.swing.JTable doctorTable;
    private javax.swing.JLabel emergencyLocation;
    private javax.swing.JTable emergencyTable;
    private javax.swing.JTabbedPane enterpriseWorkArea;
    private javax.swing.JLabel header;
    private javax.swing.JLabel hospital1;
    private javax.swing.JTable hosptOrganisationTable;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JOptionPane jOptionPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JButton locateBtn;
    private javax.swing.JTextField locationEmergencyTF;
    private javax.swing.JLabel locationErr;
    private javax.swing.JPanel manageOrgPanel;
    private javax.swing.JLabel nameErr;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField nameTF;
    private javax.swing.JLabel natureOfEmergency;
    private javax.swing.JComboBox natureOfEmergencyCombo;
    private javax.swing.JLabel natureOfemergencyErr;
    private javax.swing.JComboBox organisationCombo;
    private javax.swing.JLabel organisationLabel;
    private javax.swing.JScrollPane organisationTable;
    private javax.swing.JLabel passwordErr;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JPasswordField passwordTF;
    private javax.swing.JPanel pickTheCall;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JSlider prioritySlider;
    private javax.swing.JButton processEmergencyBtn;
    private javax.swing.JButton reportAnEmergencyBtn;
    private javax.swing.JPanel requestDrone;
    private javax.swing.JLabel specialityErr;
    private javax.swing.JLabel specialityLabel;
    private javax.swing.JTextField specialityTF;
    private javax.swing.JLabel userNameErr;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JTextField usernameTF;
    private javax.swing.JPanel workQueue;
    // End of variables declaration//GEN-END:variables

    private void populateDoctorTable() {
        DefaultTableModel model = (DefaultTableModel) doctorTable.getModel();
        model.setRowCount(0);
        for(Network n:system.getNetworkList())
        {
            for(Hospital h:n.getHospitalList())
            {
               for(UserAccount ua:h.getUserAccountDirectory().getUserAccountList())
               {
                   if(ua==account)
                   {
                       for(Organisation org:h.getOrganizationDirectory().getOrganisationList())
                       {
                           if(org instanceof DoctorOrganization)
                           {
                               for(Employee e:org.getEmployeeDirectory().getEmployeeList())
                               {
                                     Object[] row = new Object[3];
                                     row[0]= ((Doctor)e).getName();
                                     row[2] = ((Doctor)e).isDoctorsAvailablityStatus();
                                     row[1] = ((Doctor)e).getDoctorsSpeciality();
                                     model.addRow(row);
                                   }
                                     
                               }
                           }
                       }
                   }
               }
            }
        }
    }
