/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SWAT;

import EmergencySystem.Emergency.AccidentEmergency;
import EmergencySystem.Emergency.Description;
import EmergencySystem.Emergency.Emergency;
import EmergencySystem.Emergency.EmergencyDirectory;
import EmergencySystem.Emergency.FireEmergency;
import EmergencySystem.Emergency.MedicalEmergency;
import EmergencySystem.Emergency.TacticalEmergency;
import EmergencySystem.EmergencySystem;
import EmergencySystem.Enterprise.Enterprise;
import EmergencySystem.Network.Network;
import Hospital.UserAccount.UserAccount;
import Hospital.WorkQueue.Emergency911DepartmentWorkRequest;
import Hospital.WorkQueue.WorkRequest;
import Person.Person;
import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_core;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import static java.time.Clock.system;
import java.util.Date;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Aditya
 */
public class SWATAdminWorkArea extends javax.swing.JPanel {

    /**
     * Creates new form SWATAdminWorkArea
     */
    
    
    JPanel userProcessContainer;
    UserAccount account;
    EmergencySystem system;
    private EmergencyDirectory emergencyDirectory;
    Network network;
    Enterprise enterprise;
    private Emergency e;
    private String location;
    private boolean manageFields=false;
    
    public SWATAdminWorkArea(JPanel userProcessContainer, UserAccount account, EmergencySystem system, Network network, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.account=account;
        this.system=system;
        this.network=network;
        this.enterprise=enterprise;
        this.emergencyDirectory = system.getEmergencyDirectory();
        populateEmergencyTable();;
        locateBtn.setVisible(false);
        populateNatureOfEmergencyCombo();
        populateEmergencyLocation();
        
        JTableHeader tableHeader = emergencyTable.getTableHeader();
        Font headerFont = new Font("Verdana", Font.PLAIN, 16);
        tableHeader.setFont(headerFont);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pickTheCall = new javax.swing.JPanel();
        emergencyLocation = new javax.swing.JLabel();
        natureOfEmergency = new javax.swing.JLabel();
        description = new javax.swing.JLabel();
        locationEmergencyTF = new javax.swing.JTextField();
        locationErr = new javax.swing.JLabel();
        callerPhoneNumberErr = new javax.swing.JLabel();
        natureOfemergencyErr = new javax.swing.JLabel();
        descriptionErr = new javax.swing.JLabel();
        priorityLabel = new javax.swing.JLabel();
        prioritySlider = new javax.swing.JSlider();
        natureOfEmergencyCombo = new javax.swing.JComboBox();
        descriptionCombo = new javax.swing.JComboBox();
        callersPhoneNumberTF = new javax.swing.JTextField();
        phoneNumberOfCaller = new javax.swing.JLabel();
        reportAnEmergencyBtn = new javax.swing.JButton();
        locateBtn = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        emergencyTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        retrieveCapturedPhotageBtn = new javax.swing.JButton();
        retrieveCapturedVideoBtn = new javax.swing.JButton();
        completeRequestBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 102, 153));
        setPreferredSize(new java.awt.Dimension(764, 649));

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));
        jPanel1.setPreferredSize(new java.awt.Dimension(809, 853));
        jPanel1.setRequestFocusEnabled(false);

        jTabbedPane1.setBackground(new java.awt.Color(255, 153, 51));
        jTabbedPane1.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        pickTheCall.setBackground(new java.awt.Color(0, 102, 153));

        emergencyLocation.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        emergencyLocation.setForeground(new java.awt.Color(255, 255, 255));
        emergencyLocation.setText("Location of the emergency:");

        natureOfEmergency.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        natureOfEmergency.setForeground(new java.awt.Color(255, 255, 255));
        natureOfEmergency.setText("Nature of the emergency: ");

        description.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        description.setForeground(new java.awt.Color(255, 255, 255));
        description.setText("Description:");

        locationEmergencyTF.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        locationEmergencyTF.setEnabled(false);
        locationEmergencyTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locationEmergencyTFActionPerformed(evt);
            }
        });

        callerPhoneNumberErr.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        callerPhoneNumberErr.setForeground(new java.awt.Color(255, 255, 255));

        priorityLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        priorityLabel.setForeground(new java.awt.Color(255, 255, 255));
        priorityLabel.setText("Priority:");

        prioritySlider.setForeground(new java.awt.Color(255, 255, 255));
        prioritySlider.setMajorTickSpacing(1);
        prioritySlider.setMaximum(10);
        prioritySlider.setMinorTickSpacing(1);
        prioritySlider.setPaintLabels(true);
        prioritySlider.setPaintTicks(true);
        prioritySlider.setValue(0);

        natureOfEmergencyCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tactical Emergency", " " }));
        natureOfEmergencyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natureOfEmergencyComboActionPerformed(evt);
            }
        });

        descriptionCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Terrorist attack", "War" }));

        phoneNumberOfCaller.setBackground(new java.awt.Color(0, 51, 153));
        phoneNumberOfCaller.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        phoneNumberOfCaller.setForeground(new java.awt.Color(255, 255, 255));
        phoneNumberOfCaller.setText("Phone number :");

        reportAnEmergencyBtn.setBackground(new java.awt.Color(255, 255, 255));
        reportAnEmergencyBtn.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        reportAnEmergencyBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767337_09.png"))); // NOI18N
        reportAnEmergencyBtn.setText("Submit request");
        reportAnEmergencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportAnEmergencyBtnActionPerformed(evt);
            }
        });

        locateBtn.setBackground(new java.awt.Color(255, 255, 255));
        locateBtn.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        locateBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/EmergencyManagementSystem/images/1449767491_map_pin_fill.png"))); // NOI18N
        locateBtn.setText("Locate the emergency location");
        locateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locateBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pickTheCallLayout = new javax.swing.GroupLayout(pickTheCall);
        pickTheCall.setLayout(pickTheCallLayout);
        pickTheCallLayout.setHorizontalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(186, 186, 186)
                .addComponent(reportAnEmergencyBtn)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(phoneNumberOfCaller, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emergencyLocation)
                            .addComponent(natureOfEmergency, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(132, 132, 132))
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                                .addGap(42, 42, 42)
                                                .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pickTheCallLayout.createSequentialGroup()
                                        .addGap(0, 22, Short.MAX_VALUE)
                                        .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(pickTheCallLayout.createSequentialGroup()
                                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGap(14, 14, 14)
                                        .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGap(59, 59, 59)
                                        .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGap(62, 62, 62)
                                        .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pickTheCallLayout.createSequentialGroup()
                                        .addGap(59, 59, 59)
                                        .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(pickTheCallLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(locateBtn)))
                .addGap(81, 81, 81))
        );
        pickTheCallLayout.setVerticalGroup(
            pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pickTheCallLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emergencyLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(locationEmergencyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(locationErr, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(natureOfEmergency)
                    .addComponent(natureOfEmergencyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(natureOfemergencyErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(descriptionCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(descriptionErr, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(phoneNumberOfCaller)
                    .addComponent(callersPhoneNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(callerPhoneNumberErr, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(pickTheCallLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(priorityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prioritySlider, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addComponent(reportAnEmergencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(locateBtn)
                .addGap(22, 22, 22))
        );

        jTabbedPane1.addTab("Request Drone", pickTheCall);

        jPanel2.setBackground(new java.awt.Color(0, 102, 153));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        emergencyTable.setFont(new java.awt.Font(".SF NS Text", 0, 15)); // NOI18N
        emergencyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Phone number", "Location of emergency", "Description", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(emergencyTable);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Incident List");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(300, 300, 300)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 791, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel2)
                .addGap(56, 56, 56)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(411, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Work Queue", jPanel2);

        jPanel3.setBackground(new java.awt.Color(0, 102, 153));

        retrieveCapturedPhotageBtn.setBackground(new java.awt.Color(255, 255, 255));
        retrieveCapturedPhotageBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        retrieveCapturedPhotageBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449766076_finance-10.png"))); // NOI18N
        retrieveCapturedPhotageBtn.setText("Retrieve captured photage");
        retrieveCapturedPhotageBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retrieveCapturedPhotageBtnActionPerformed(evt);
            }
        });

        retrieveCapturedVideoBtn.setBackground(new java.awt.Color(255, 255, 255));
        retrieveCapturedVideoBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        retrieveCapturedVideoBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449766076_finance-10.png"))); // NOI18N
        retrieveCapturedVideoBtn.setText("Retrieve captured video");
        retrieveCapturedVideoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retrieveCapturedVideoBtnActionPerformed(evt);
            }
        });

        completeRequestBtn.setBackground(new java.awt.Color(255, 255, 255));
        completeRequestBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        completeRequestBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Police/Images/1449766076_finance-10.png"))); // NOI18N
        completeRequestBtn.setText("Complete Request");
        completeRequestBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                completeRequestBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(220, 220, 220)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(retrieveCapturedPhotageBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(retrieveCapturedVideoBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(completeRequestBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(332, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {retrieveCapturedPhotageBtn, retrieveCapturedVideoBtn});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addComponent(retrieveCapturedPhotageBtn)
                .addGap(48, 48, 48)
                .addComponent(retrieveCapturedVideoBtn)
                .addGap(45, 45, 45)
                .addComponent(completeRequestBtn)
                .addContainerGap(402, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {retrieveCapturedPhotageBtn, retrieveCapturedVideoBtn});

        jTabbedPane1.addTab("Complete Request", jPanel3);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("SWAT Admin Work Area");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 803, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel1)
                .addGap(31, 31, 31)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 809, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 853, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

       
    private void retrieveCapturedPhotageBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retrieveCapturedPhotageBtnActionPerformed
        // TODO add your handling code here:
        int row=emergencyTable.getSelectedRow();
        Emergency emergency=null;
        if(row>=0)
        {
            emergency=(Emergency) emergencyTable.getValueAt(row, 2);
            String imageUrl=emergency.getCapturedPhotageURL();
            opencv_core.IplImage image= cvLoadImage(imageUrl);

                    final CanvasFrame canvas =new CanvasFrame("Captured Footage");
                    canvas.showImage(image);
                    canvas.setCanvasSize(500, 300);
                    //canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
                }

                else
                {
                    JOptionPane.showMessageDialog(this,"Choose an incident from the table");
                }
    }//GEN-LAST:event_retrieveCapturedPhotageBtnActionPerformed

        public void populateEmergencyTable()
    {
        DefaultTableModel model = (DefaultTableModel) emergencyTable.getModel();
        
        model.setRowCount(0);
        
        for (WorkRequest workRequest:account.getWorkQueue().getWorkRequestList()){
            Object[] row = new Object[5];
            row[0]= ((Emergency911DepartmentWorkRequest) workRequest);
            row[1]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getCallersPhoneNumber();
            row[2]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency();
            row[3]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getDescription();
            row[4]= ((Emergency911DepartmentWorkRequest) workRequest).getEmergency().getEmergencyStatus();
            model.addRow(row);
        }
    }
    
    
    
    
    
    private void locationEmergencyTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locationEmergencyTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_locationEmergencyTFActionPerformed

    private void reportAnEmergencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportAnEmergencyBtnActionPerformed
        // TODO add your handling code here:
                if(callersPhoneNumberTF.getText().matches("[0-9]+")&& (callersPhoneNumberTF.getText().length()==10))

        {
            callerPhoneNumberErr.setText("");
            e=emergencyDirectory.createEmergency();
            e.setLocationOfEmergency(locationEmergencyTF.getText());
            e.setCallersPhoneNumber(callersPhoneNumberTF.getText());
            e.setNatureOfEmergency((String) natureOfEmergencyCombo.getSelectedItem());
            e.setDescription((String) descriptionCombo.getSelectedItem());
            e.setPriority(prioritySlider.getValue());
            e.setEmergencyStatus("Reported");
            Date d = new Date();
            e.setReportedTime(d);
            JOptionPane.showMessageDialog(this,"Drone request has been submitted!");
            locateBtn.setEnabled(true);
            reportAnEmergencyBtn.setEnabled(false);
            callersPhoneNumberTF.setEditable(false);
            location=e.getLocationOfEmergency().replaceAll("\\s","+");
        }

        else
        {

            if(!callersPhoneNumberTF.getText().matches("[0-9]+"))
            {
                callerPhoneNumberErr.setText("Enter only numeric values");
            }

            else
            {
                callerPhoneNumberErr.setText("Enter a 10 digit numeric value");
            }
        }
    }//GEN-LAST:event_reportAnEmergencyBtnActionPerformed

    private void locateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locateBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            //
            //        Browser browser = BrowserFactory.create();
            //        JFrame frame = new JFrame("JxBrowser Google Maps");
            //        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            //        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
            //        frame.setSize(700, 500);
            //        frame.setLocationRelativeTo(null);
            //        frame.setVisible(true);
            //        browser.loadURL("http://maps.google.com");
            Browser browser = new Browser();
            BrowserView browserView = new BrowserView(browser);
            JFrame frame = new JFrame("Emergency Location");

            frame.add(browserView, BorderLayout.CENTER);
            frame.setSize(700, 500);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            browser.loadURL("http://maps.google.com/?q="+location);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_locateBtnActionPerformed

    private void natureOfEmergencyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natureOfEmergencyComboActionPerformed
        // TODO add your handling code here:
        String emrg = (String) natureOfEmergencyCombo.getSelectedItem();
        System.out.println(emrg);
        if (emrg != null){
            populateDescriptionComboBox(emrg);
        }
    }//GEN-LAST:event_natureOfEmergencyComboActionPerformed

    private void retrieveCapturedVideoBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retrieveCapturedVideoBtnActionPerformed
        // TODO add your handling code here:
                // TODO add your handling code here:
        int row=emergencyTable.getSelectedRow();
        Emergency emergency=null;
        if(row>=0)
        {
            emergency=(Emergency) emergencyTable.getValueAt(row, 2);
            String imageUrl=emergency.getCapturedVideoURL();
            opencv_core.IplImage image= cvLoadImage(imageUrl);

                    final CanvasFrame canvas =new CanvasFrame("Captured Video");
                    canvas.showImage(image);
                    canvas.setCanvasSize(500, 300);
                    //canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
                }

                else
                {
                    JOptionPane.showMessageDialog(this,"Choose an request from the table");
                }
    }//GEN-LAST:event_retrieveCapturedVideoBtnActionPerformed

    private void completeRequestBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_completeRequestBtnActionPerformed
        // TODO add your handling code here:
        int row=emergencyTable.getSelectedRow();
        Emergency emergency=null;
//        if(row>=0)
//        {
            emergency=(Emergency) emergencyTable.getValueAt(row, 2);
            emergency.setEmergencyStatus("Completed");
            JOptionPane.showMessageDialog(this,"Request Completed");
//        }
    }//GEN-LAST:event_completeRequestBtnActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
        populateEmergencyTable();
    }//GEN-LAST:event_jTabbedPane1MouseClicked
  
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel callerPhoneNumberErr;
    private javax.swing.JTextField callersPhoneNumberTF;
    private javax.swing.JButton completeRequestBtn;
    private javax.swing.JLabel description;
    private javax.swing.JComboBox descriptionCombo;
    private javax.swing.JLabel descriptionErr;
    private javax.swing.JLabel emergencyLocation;
    private javax.swing.JTable emergencyTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton locateBtn;
    private javax.swing.JTextField locationEmergencyTF;
    private javax.swing.JLabel locationErr;
    private javax.swing.JLabel natureOfEmergency;
    private javax.swing.JComboBox natureOfEmergencyCombo;
    private javax.swing.JLabel natureOfemergencyErr;
    private javax.swing.JLabel phoneNumberOfCaller;
    private javax.swing.JPanel pickTheCall;
    private javax.swing.JLabel priorityLabel;
    private javax.swing.JSlider prioritySlider;
    private javax.swing.JButton reportAnEmergencyBtn;
    private javax.swing.JButton retrieveCapturedPhotageBtn;
    private javax.swing.JButton retrieveCapturedVideoBtn;
    // End of variables declaration//GEN-END:variables

    public void populateEmergencyLocation()
    {
       
        Random rand = new Random(); 
        int index=rand.nextInt(5);
         
         locationEmergencyTF.setText(system.getDirectory().getEmergencyAddressLocationList().get(index).getAddress());
    }
    
    public void populateNatureOfEmergencyCombo()
    {
         natureOfEmergencyCombo.removeAllItems();
        for (Emergency.EmergencyType type : Emergency.EmergencyType.values())
        {
                String value=type.getValue();
                natureOfEmergencyCombo.addItem(value);
        }
  
    }
    
    public void populateDescriptionComboBox(String emrg)
    
    {
        descriptionCombo.removeAllItems();
        if(emrg.equalsIgnoreCase("Accident Emergency"))
        {
            AccidentEmergency accEmerg=new AccidentEmergency();
            for(Description d:accEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
        else if(emrg.equalsIgnoreCase("Medical Emergency"))
        {
            MedicalEmergency medEmerg=new MedicalEmergency();
          
            for(Description d:medEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
        
         else if(emrg.equalsIgnoreCase("Fire Emergency"))
        {
            FireEmergency fireEmerg=new FireEmergency();
            System.err.println("I am going in the fire emer");
            for(Description d:fireEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
                
        else if(emrg.equalsIgnoreCase("Tactical Emergency"))
        {
            TacticalEmergency tactEmerg=new TacticalEmergency();
     
            for(Description d:tactEmerg.getDescriptionList())
            {
                descriptionCombo.addItem(d.getName());
            }
        }
    }
}
